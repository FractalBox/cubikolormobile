﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Chrono : MonoBehaviour {
    public GameObject chrono;
    //[HideInInspector]
    public float centiemes = 0; // Le temps que l'on impose
    //[HideInInspector]
    public float timeInCentiSec; // Le temps qui s'écoule effectivement
    [HideInInspector]
    public bool shouldChronoFlow = true;

	// Use this for initialization
	void Start () {
        centiemes = GameObject.Find("LevelID").GetComponent<SelectedLevel>().centiemesForChrono;
        timeInCentiSec = centiemes;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateChrono();
    }

    // Afficher chrono s'il le faut
    // Mettre à jour le temps qui s'écoule jusqu'à (éventuellement) 00'00"00 (ie défaite)
    public void UpdateChrono()
    {
        // Il y a un chrono si dans les infos sur les contraintes du niveau le chiffre en 0 et 1, et celui en 3 et 4 sont non nulles (secondes et centièmes)
        chrono.SetActive(centiemes != 0); // Affichage

        // Mn s cs
        int mn = Mathf.FloorToInt(timeInCentiSec / 6000);
        int s = Mathf.FloorToInt((timeInCentiSec / 6000 - mn) * 60);
        int cs = Mathf.FloorToInt(((timeInCentiSec / 6000 - mn) * 60 - s) * 100);

        if (timeInCentiSec >= 0) chrono.GetComponentInChildren<Text>().text = string.Format("{0:00}'{1:00}\"{2:00}", mn, s, cs);
        else chrono.GetComponentInChildren<Text>().text = "00'00\"00";

        //Décompte cs par cs
        if (shouldChronoFlow) timeInCentiSec -= Time.deltaTime * 100;
    }
}
