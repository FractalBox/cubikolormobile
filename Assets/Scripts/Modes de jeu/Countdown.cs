﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Countdown : MonoBehaviour {
    public GameObject compteurCoups;
    //[HideInInspector]
    public int nbrMaxCoups = 0;

	// Use this for initialization
	void Start () {
        nbrMaxCoups = GameObject.Find("LevelID").GetComponent<SelectedLevel>().maxCoupsForCountdown;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateComtpeurCoups();
    }
    
    // Gérer affichage du compteur de coups +
    // Mettre à jour le compteur de Coups
    public void UpdateComtpeurCoups()
    {
        compteurCoups.SetActive(nbrMaxCoups != 0); // Affichage

        if (nbrMaxCoups - GetComponent<MovePlayer>().nbPJMoves >= 0) compteurCoups.GetComponentInChildren<Text>().text = (nbrMaxCoups - GetComponent<MovePlayer>().nbPJMoves).ToString();
        else compteurCoups.GetComponentInChildren<Text>().text = "0";
    }
}
