﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum Modes { Classic, Hardcore };

public class GameMode : MonoBehaviour {

    public Modes gameMode;

	// Use this for initialization
	void Start () {
        // Récupérer données du menu de sélection du niveau
        gameMode = GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    // Afficher au bon format le game mode pour la victory score window
    public string DisplayGameMode()
    {
        if (gameMode == Modes.Classic) return "C  l  a  s  s  i  k";
        else if(gameMode == Modes.Hardcore) return "H  a  r  d  k  o  r  e";
        else return "";

    }
}
