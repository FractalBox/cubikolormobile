﻿using System;
//using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class SaveLastLevelData
{
    // Dernier niveau joué
    public int lettrePhase1; // Zone du niveau
    public int chiffrePhase2; // Index du niveau
    public int levelIndex; // Le véritable ID du niveau
    public Modes gameMode;
    // Ses contraintes
    public float centiemesForChrono;
    public int maxCoupsForCountdown;

    // Etat de jeu du niveau (inutile)
    //public Vector3 positionPJ;
    //public int countUndos;
    //public int nbPJMoves;
    //public float tempsEcoule; // Le temps qui s'est écoulé depuis le début du niveau (à remettre à 0 au Restart/Retry) CE TEMPS EST EN SECONDES
    //// Données stockées pour le Undo
    //public List<SwipeDirection> PJFlipDirections;
    //public List<float> cameraRotations; // Les rotations de caméra au moment des flips pour que le flip back se fasse selon l'orientation de la caméra correspondante et non selon l'actuelle qui peut être différente
    //public List<Vector3> PJPositions;
    //public List<Quaternion> PJRotations;
}