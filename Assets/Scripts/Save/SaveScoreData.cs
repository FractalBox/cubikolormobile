﻿using System;

[Serializable]
public class SaveScoreData {

    public float[] levelChronoClassik;
    public float[] levelNbrCoupsClassik;

    public float[] levelChronoHardkore;
    public float[] levelNbrCoupsHardkore;

    // pour chaque niveau, 2 modes => 2 tableaux différents pour chaque attribut

    // Constructeur
    public SaveScoreData(int nbrLevels)
    {
        levelChronoClassik = new float[nbrLevels];
        levelNbrCoupsClassik = new float[nbrLevels];

        levelChronoHardkore = new float[nbrLevels];
        levelNbrCoupsHardkore = new float[nbrLevels];
    }
}
