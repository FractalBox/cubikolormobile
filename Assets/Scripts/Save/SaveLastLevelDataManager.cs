﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class SaveLastLevelDataManager : MonoBehaviour {

    void Awake()
    {
        // Pour iOS :
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Sauvegarde automatique du niveau en cours de jeu
    private void OnApplicationPause(bool pause)
    {
        if (pause) Save();
        else Load();
    }

    // Saves data dans un fichier
    public void Save()
    {
        if (GameObject.Find("LevelID") != null)
        {
            SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();
            
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/cubikolorLastLevelSave.data");
            // Lieu de sauvegarde sur ce PC : C:\Users\Remi\AppData\LocalLow\Fractal Box\Cubikolor Mobile\cubikolorLastSave.data
            // Cubikolor Mobile étant le Nom du produit dans Player Settings

            SaveLastLevelData data = new SaveLastLevelData();
            data.lettrePhase1 = levelInfo.lettrePhase1;
            data.chiffrePhase2 = levelInfo.chiffrePhase2;
            data.levelIndex = levelInfo.levelIndex;
            data.gameMode = levelInfo.gameMode;
            data.centiemesForChrono = levelInfo.centiemesForChrono;
            data.maxCoupsForCountdown = levelInfo.maxCoupsForCountdown;
            //data.positionPJ = GameObject.Find("PJ").transform.position;
            //data.countUndos = GameObject.Find("Main Camera").GetComponent<UndoMove>().countUndos;
            //data.nbPJMoves = GameObject.Find("Main Camera").GetComponent<MovePlayer>().nbPJMoves;
            //data.tempsEcoule = GameObject.Find("Main Camera").GetComponent<Temps>().tempsEcoule;
            //data.PJFlipDirections = GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJFlipDirections;
            //data.cameraRotations = GameObject.Find("Main Camera").GetComponent<MovePlayer>().cameraRotations;
            //data.PJPositions = GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJPositions;
            //data.PJRotations = GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJRotations;

            bf.Serialize(file, data);
            file.Close();
        }
    }

    // Récupère saved data 
    // Pour le click sur Continuer qui ouvre le dernier niveau joué (pour l'instant le niveau est restart pas repris en cours de jeu)
    public void Load()
    {
        if (GameObject.Find("LevelID") != null)
        {
            PersistentLastLevelData lastLevelInfo = GameObject.Find("SaveLastLevelDataManager").GetComponent<PersistentLastLevelData>(); // Stockage de ces données sauvegardée

            if (File.Exists(Application.persistentDataPath + "/cubikolorLastLevelSave.data"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/cubikolorLastLevelSave.data", FileMode.Open);

                SaveLastLevelData data = (SaveLastLevelData)bf.Deserialize(file);
                file.Close();

                lastLevelInfo.lettrePhase1 = data.lettrePhase1;
                lastLevelInfo.chiffrePhase2 = data.chiffrePhase2;
                lastLevelInfo.levelIndex = data.levelIndex;
                lastLevelInfo.gameMode = data.gameMode;
                lastLevelInfo.centiemesForChrono = data.centiemesForChrono;
                lastLevelInfo.maxCoupsForCountdown = data.maxCoupsForCountdown;

                //if (GameObject.Find("PJ") != null) // Load en cours de jeu
                //{
                //    GameObject.Find("PJ").transform.position = data.positionPJ;
                //    GameObject.Find("Main Camera").GetComponent<UndoMove>().countUndos = data.countUndos;
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().nbPJMoves = data.nbPJMoves;
                //    GameObject.Find("Main Camera").GetComponent<Temps>().tempsEcoule = data.tempsEcoule;
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJFlipDirections = data.PJFlipDirections;
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().cameraRotations = data.cameraRotations;
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJPositions = data.PJPositions;
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJRotations = data.PJRotations;
                //}
                //else // Load au start (scene_menu)
                //{
                //    GameObject.Find("PJ").transform.position = Vector3.zero;
                //    GameObject.Find("Main Camera").GetComponent<UndoMove>().countUndos = 0;
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().nbPJMoves = 0;
                //    GameObject.Find("Main Camera").GetComponent<Temps>().tempsEcoule = 0;
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJFlipDirections = new List<SwipeDirection>();
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().cameraRotations = new List<float>();
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJPositions = new List<Vector3>();
                //    GameObject.Find("Main Camera").GetComponent<MovePlayer>().PJRotations = new List<Quaternion>();
                //}
            }
        }
    }
}
