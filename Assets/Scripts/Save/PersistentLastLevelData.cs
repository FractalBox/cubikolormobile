﻿using UnityEngine;
using System.Collections.Generic;

public class PersistentLastLevelData : Singleton<PersistentLastLevelData>
{
    [Header("Infos")]
    public int lettrePhase1; // Zone du niveau
    public int chiffrePhase2; // Index du niveau
    public int levelIndex; // Le véritable ID du niveau
    public Modes gameMode;
    // Ses contraintes
    [Space(10)]
    [Header("Contraintes")]
    public float centiemesForChrono;
    public int maxCoupsForCountdown;

    // Etat de jeu du niveau
    //[Space(10)]
    //[Header("Etat du jeu")]
    //public Vector3 positionPJ;
    //public int countUndos = 0;
    //public int nbPJMoves = 0;
    //public float tempsEcoule = 0; // Le temps qui s'est écoulé depuis le début du niveau (à remettre à 0 au Restart/Retry) CE TEMPS EST EN SECONDES
    //// Données stockées pour le Undo
    //public List<SwipeDirection> PJFlipDirections;
    //public List<float> cameraRotations; // Les rotations de caméra au moment des flips pour que le flip back se fasse selon l'orientation de la caméra correspondante et non selon l'actuelle qui peut être différente
    //public List<Vector3> PJPositions;
    //public List<Quaternion> PJRotations;

    // Use this for initialization
    void Start()
    {
        // Premier niveau
        lettrePhase1 = 1;
        chiffrePhase2 = 1;
        levelIndex = (lettrePhase1 - 1) * 10 + chiffrePhase2 - 1;
        gameMode = Modes.Classic;
        centiemesForChrono = 0;
        maxCoupsForCountdown = 0;

        GameObject.Find("SaveLastLevelDataManager").GetComponent<SaveLastLevelDataManager>().Load();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
