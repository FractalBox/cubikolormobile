﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveScoreDataManager : MonoBehaviour
{

    void Awake()
    {
        // Pour iOS :
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
        }
    }

    // Saves data dans un fichier
    public void Save()
    {
        if (GameObject.Find("LevelID") != null)
        {
            // Récupérer niveau actuel dans LevelID
            SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/cubikolorScoreSave.data");

            SaveScoreData data = new SaveScoreData(levelInfo.levels.Length); // On veut tableaux de taille nbr de niveaux
            // Récupérer scores déjà sauvegardés
            data.levelChronoClassik = GetComponent<PersistentScoreData>().levelChronoClassik;
            data.levelNbrCoupsClassik = GetComponent<PersistentScoreData>().levelNbrCoupsClassik;

            data.levelChronoHardkore = GetComponent<PersistentScoreData>().levelChronoHardkore;
            data.levelNbrCoupsHardkore = GetComponent<PersistentScoreData>().levelNbrCoupsHardkore;

            // Récupérer la fenêtre de victoire de ce niveau
            Victory victoireInfo = GameObject.Find("Main Camera").GetComponent<Victory>();
            // En extraire le temps et le nbrCoups réalisés pour ce niveau i
            // Les placer dans les cases i des deux tableaux de sauvegarde de data selon le mode de jeu du niveau i
            // Ne pas du tout remplacer ce score s'il est moins bon ! 
            // TODO si nbr de coups est meilleur mais pas le temps (ou vice versa), on remplace ou pas du tout ??
            // ( Ici on remplace indépendamment l'un de l'autre )
            if (levelInfo.gameMode == Modes.Classic)
            {
                if (data.levelChronoClassik[levelInfo.levelIndex] == 0 || data.levelChronoClassik[levelInfo.levelIndex] > GameObject.Find("Main Camera").GetComponent<Temps>().tempsEcoule)
                {
                    data.levelChronoClassik[levelInfo.levelIndex] = GameObject.Find("Main Camera").GetComponent<Temps>().tempsEcoule;
                }
                if (data.levelNbrCoupsClassik[levelInfo.levelIndex] == 0 || data.levelNbrCoupsClassik[levelInfo.levelIndex] > GameObject.Find("Main Camera").GetComponent<MovePlayer>().nbPJMoves)
                {
                    data.levelNbrCoupsClassik[levelInfo.levelIndex] = GameObject.Find("Main Camera").GetComponent<MovePlayer>().nbPJMoves;
                }
            }
            else if (levelInfo.gameMode == Modes.Hardcore)
            {
                if (data.levelChronoHardkore[levelInfo.levelIndex] == 0 || data.levelChronoHardkore[levelInfo.levelIndex] > GameObject.Find("Main Camera").GetComponent<Temps>().tempsEcoule)
                {
                    data.levelChronoHardkore[levelInfo.levelIndex] = GameObject.Find("Main Camera").GetComponent<Temps>().tempsEcoule;
                }
                if (data.levelNbrCoupsHardkore[levelInfo.levelIndex] == 0 || data.levelNbrCoupsHardkore[levelInfo.levelIndex] > GameObject.Find("Main Camera").GetComponent<MovePlayer>().nbPJMoves)
                {
                    data.levelNbrCoupsHardkore[levelInfo.levelIndex] = GameObject.Find("Main Camera").GetComponent<MovePlayer>().nbPJMoves;
                }
            }

            bf.Serialize(file, data);
            file.Close();
        }
    }

    // Récupère saved data
    public void Load()
    {
        if (GameObject.Find("LevelID") != null)
        {
            SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();

            if (File.Exists(Application.persistentDataPath + "/cubikolorScoreSave.data"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/cubikolorScoreSave.data", FileMode.Open);

                SaveScoreData data = (SaveScoreData)bf.Deserialize(file);
                file.Close();

                // DATA                
                // Donner ces tableaux au script qui gère l'affichage du meilleur score et des scores du niveau en cours de sélection
                PersistentScoreData lastLevelInfo = GetComponent<PersistentScoreData>();
                lastLevelInfo.levelChronoClassik = data.levelChronoClassik;
                lastLevelInfo.levelNbrCoupsClassik = data.levelNbrCoupsClassik;

                lastLevelInfo.levelChronoHardkore = data.levelChronoHardkore;
                lastLevelInfo.levelNbrCoupsHardkore = data.levelNbrCoupsHardkore;                
            }
        }
    }
}
