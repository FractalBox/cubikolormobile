﻿using UnityEngine;
using System.Collections;

public class PersistentScoreData : Singleton<PersistentScoreData> {

    public float[] levelChronoClassik;
    public float[] levelNbrCoupsClassik;

    public float[] levelChronoHardkore;
    public float[] levelNbrCoupsHardkore;

    // Use this for initialization
    void Start () {
        // Création des tableaux pour storage des scores des niveaux
        int nbrLevels = GameObject.Find("LevelID").GetComponent<SelectedLevel>().levels.Length;

        levelChronoClassik = new float[nbrLevels];
        levelNbrCoupsClassik = new float[nbrLevels];

        levelChronoHardkore = new float[nbrLevels];
        levelNbrCoupsHardkore = new float[nbrLevels];

        GameObject.Find("SaveScoreDataManager").GetComponent<SaveScoreDataManager>().Load();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
