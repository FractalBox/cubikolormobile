﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class DoubleClic : MonoBehaviour {
    //private bool oneClick = false;
    private float timerForDoubleClick;
    public float delayBetweenTaps = 0.01f; //this is how long in seconds to allow for a double click
    [Space(10)]
    [HideInInspector]
    public bool hasDoubleClicked = false;
    [HideInInspector]
    public int doubleClicCount = 0;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // DetectDoubleClic()
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void DetectDoubleClic()
    {
        // VERSION SOURIS
        //if (Input.GetMouseButtonDown(0))
        //{
        //    if (!EventSystem.current.IsPointerOverGameObject())
        //    {
        //        if (!oneClick) // first click no previous clicks
        //        {
        //            oneClick = true;
        //            timerForDoubleClick = Time.time; // save the current time

        //            // do one click things
        //        }
        //        else
        //        {
        //            //do double click things
        //            if (!GetComponent<CameraController>().camIsSwiping)
        //            {
        //                doubleClicCount++;
        //                hasDoubleClicked = true;
        //            }

        //            oneClick = false; // found a double click, now reset
        //        }
        //    }
        //}
        //if (oneClick)
        //{
        //    // if the time now is delayBetweenTaps seconds more than when the first click started 
        //    if (Time.time - timerForDoubleClick > delayBetweenTaps)
        //    {
        //        //basically if thats true its been too long and we want to reset so the next click is simply a single click and not a double click.
        //        oneClick = false;
        //        hasDoubleClicked = false;
        //    }
        //}

        // VERSION TOUCH
        //int nbTouches = Input.touchCount;
        //if (nbTouches > 0)
        //{
        //    for (int i = 0; i < nbTouches; i++)
        //    {
        //        Touch touch = Input.GetTouch(i); // Pour le touch i

        //        if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
        //        {
        //            if (touch.phase == TouchPhase.Began) // Doigt posé sur écran
        //            {
        //                if (!oneClick) // first click no previous clicks
        //                {
        //                    oneClick = true;
        //                    timerForDoubleClick = Time.time; // save the current time

        //                    // do one click things
        //                }
        //                else
        //                {
        //                    //do double click things
        //                    if (!GetComponent<CameraController>().camIsSwiping)
        //                    {
        //                        doubleClicCount++;
        //                        hasDoubleClicked = true;
        //                    }

        //                    oneClick = false; // found a double click, now reset
        //                }
        //            }
        //        }
        //        if (oneClick)
        //        {
        //            // if the time now is delayBetweenTaps seconds more than when the first click started 
        //            if (Time.time - timerForDoubleClick > delayBetweenTaps)
        //            {
        //                //basically if thats true its been too long and we want to reset so the next click is simply a single click and not a double click.
        //                oneClick = false;
        //                hasDoubleClicked = false;
        //            }
        //        }
        //    }        
        //}
    }
}
