﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public enum SwipeDirection { None, Up, Down, Left, Right };

public class SwipeInput : MonoBehaviour
{

    private GameObject PJ;

    [HideInInspector]
    public bool startSwipeOnPJ;
    [HideInInspector]
    public bool startSwipeCamera;

    private Vector3 startSwipePos; // début du swipe
    private Vector3 endSwipePos; // fin du swipe
    [HideInInspector]
    public Vector3 currentSwipePos; // position du swipe actuel
    [HideInInspector]
    public SwipeDirection swipeDirection; // La direction du swipe actuel

    // Directions partant du centre de la face supérieure (de normale y) du cube et allant vers le coin indiqué dans leurs noms
    //private Vector3 centreScreen;
    //private Vector3 versCoinUpLeft; 
    //private Vector3 versCoinUpRight;
    //private Vector3 versCoinBotLeft;
    //private Vector3 versCoinBotRight;

    // Directions partance des coins de la face supérieure du cube allant vers les milieux des arrêtes de l'écran
    private Vector3 centreScreen;
    private Vector3 coinVersUp;
    private Vector3 coinVersRight;
    private Vector3 coinVersLeft;
    private Vector3 coinVersDown;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Use this for initialization
    void Start()
    {
        PJ = GameObject.Find("PJ");
    }

    // Update is called once per frame
    void Update()
    {
        RecupererSwipeDirections();
        SwipeOnPJOrCam();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Récupérer les directions référence pour le swipe
    public void RecupererSwipeDirections()
    {
        // VERSION 1 : face supérieure du cube séparée en 4 zones selon les coins de cette face donc par 2 diagonales se croisant au centre de cette face
        // On récupère le centre et les coins de la face supérieure du cube
        //Vector3 centreFaceSup = PJ.transform.position + PJ.transform.localScale.y / 2 * Vector3.up; // Le centre n'est pas transféré dans repère écran ICI
        //Vector3 coinUpLeft = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(PJ.transform.localScale.x / 2, 0, PJ.transform.localScale.z / 2));
        //Vector3 coinUpRight = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(PJ.transform.localScale.x / 2, 0, -PJ.transform.localScale.z / 2));
        //Vector3 coinBotLeft = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(-PJ.transform.localScale.x / 2, 0, PJ.transform.localScale.z / 2));
        //Vector3 coinBotRight = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(-PJ.transform.localScale.x / 2, 0, -PJ.transform.localScale.z / 2));
        //centreFaceSup = Camera.main.WorldToScreenPoint(PJ.transform.position + PJ.transform.localScale.y / 2 * Vector3.up); // ICI il l'est dans repère écran

        //Tracés parallèles à la normale vers l'extérieur de la face supérieure partant de chaque coin et du centre
        //Debug.DrawLine(centreFaceSup, centreFaceSup + Vector3.up, Color.red);
        //Debug.DrawLine(coinUpLeft, coinUpLeft + Vector3.up, Color.black);
        //Debug.DrawLine(coinUpRight, coinUpRight + Vector3.up, Color.blue);
        //Debug.DrawLine(coinBotLeft, coinBotLeft + Vector3.up, Color.green);
        //Debug.DrawLine(coinBotRight, coinBotRight + Vector3.up, Color.magenta);

        //Tracés suivant diagonales vers l'extérieur de la face supérieure partant de chaque coin et du centre
        //Debug.DrawLine(coinUpLeft, coinUpLeft + PJ.transform.localScale.x / 2 * new Vector3(1, 0, 1), Color.black);
        //Debug.DrawLine(coinUpRight, coinUpRight + PJ.transform.localScale.x / 2 * new Vector3(1, 0, -1), Color.blue);
        //Debug.DrawLine(coinBotLeft, coinBotLeft + PJ.transform.localScale.x / 2 * new Vector3(-1, 0, 1), Color.green);
        //Debug.DrawLine(coinBotRight, coinBotRight + PJ.transform.localScale.x / 2 * new Vector3(-1, 0, -1), Color.magenta);

        // Tracés des des diagonales dans le repère écran
        //Debug.DrawLine(Camera.main.WorldToScreenPoint(coinUpLeft), Camera.main.WorldToScreenPoint(coinUpLeft + PJ.transform.localScale.x / 2 * new Vector3(1, 0, 1)), Color.black);
        //Debug.DrawLine(Camera.main.WorldToScreenPoint(coinUpRight), Camera.main.WorldToScreenPoint(coinUpRight + PJ.transform.localScale.x / 2 * new Vector3(1, 0, -1)), Color.blue);
        //Debug.DrawLine(Camera.main.WorldToScreenPoint(coinBotLeft), Camera.main.WorldToScreenPoint(coinBotLeft + PJ.transform.localScale.x / 2 * new Vector3(-1, 0, 1)), Color.green);
        //Debug.DrawLine(Camera.main.WorldToScreenPoint(coinBotRight), Camera.main.WorldToScreenPoint(coinBotRight + PJ.transform.localScale.x / 2 * new Vector3(-1, 0, -1)), Color.magenta);

        //On stocke les directions calculées, On les transfert dans le repère écran, puis On les normalise
        //versCoinUpLeft = Camera.main.WorldToScreenPoint(coinUpLeft + PJ.transform.localScale.x / 2 * new Vector3(1, 0, 1));
        //versCoinUpRight = Camera.main.WorldToScreenPoint(coinUpRight + PJ.transform.localScale.x / 2 * new Vector3(1, 0, -1));
        //versCoinBotLeft = Camera.main.WorldToScreenPoint(coinBotLeft + PJ.transform.localScale.x / 2 * new Vector3(-1, 0, 1));
        //versCoinBotRight = Camera.main.WorldToScreenPoint(coinBotRight + PJ.transform.localScale.x / 2 * new Vector3(-1, 0, -1));

        //Debug.DrawLine(coinUpLeft, centreFaceSup, Color.black);
        //Debug.DrawLine(coinUpRight, centreFaceSup, Color.blue);
        //Debug.DrawLine(coinBotLeft, centreFaceSup, Color.green);
        //Debug.DrawLine(coinBotRight, centreFaceSup, Color.magenta);

        //versCoinUpLeft = coinUpLeft - centreFaceSup;
        //versCoinUpRight = coinUpRight - centreFaceSup;
        //versCoinBotLeft = coinBotLeft - centreFaceSup;
        //versCoinBotRight = coinBotRight - centreFaceSup;

        //versCoinUpLeft.Normalize();
        //versCoinUpRight.Normalize();
        //versCoinBotLeft.Normalize();
        //versCoinBotRight.Normalize();


        // VERSION 2 : écran divisé en 4 zones par 2 diagonales se croisant au centre de l'écran
        //centreScreen = new Vector3(Screen.width / 2, Screen.height / 2, 0); // Centre de l'écran de jeu
        //Vector3 coinUpLeftScreen = centreScreen + new Vector3(-Screen.width / 2, Screen.height / 2, 0);
        //Vector3 coinUpRightScreen = centreScreen + new Vector3(Screen.width / 2, Screen.height / 2, 0);
        //Vector3 coinBotLeftScreen = centreScreen + new Vector3(-Screen.width / 2, -Screen.height / 2, 0);
        //Vector3 coinBotRightScreen = centreScreen + new Vector3(Screen.width / 2, -Screen.height / 2, 0);

        //// Tracés des diagonales su écran se croisant au centre
        //Debug.DrawLine(coinUpLeftScreen, centreScreen, Color.black);
        //Debug.DrawLine(coinUpRightScreen, centreScreen, Color.blue);
        //Debug.DrawLine(coinBotLeftScreen, centreScreen, Color.green);
        //Debug.DrawLine(coinBotRightScreen, centreScreen, Color.magenta);

        //// On stocke les directions calculées puis normalisées
        //versCoinUpLeft = coinUpLeftScreen - centreScreen;
        //versCoinUpRight = coinUpRightScreen - centreScreen;
        //versCoinBotLeft = coinBotLeftScreen - centreScreen;
        //versCoinBotRight = coinBotRightScreen - centreScreen;

        //versCoinUpLeft.Normalize();
        //versCoinUpRight.Normalize();
        //versCoinBotLeft.Normalize();
        //versCoinBotRight.Normalize();


        // VERSION 3 : 4 zones divisées par les directions partant des coins de la face supérieure du cube et allant vers les coins de l'écran
        // On récupère le centre et les coins de la face supérieure du cube
        // Et, On transfert ces coordonnées dans le repère de l'écran
        //Vector3 centreFaceSup = PJ.transform.position + PJ.transform.localScale.y / 2 * Vector3.up; // Le centre n'est pas transféré dans repère écran ICI
        //Vector3 coinUpLeft = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(PJ.transform.localScale.x / 2, 0, PJ.transform.localScale.z / 2));
        //Vector3 coinUpRight = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(PJ.transform.localScale.x / 2, 0, -PJ.transform.localScale.z / 2));
        //Vector3 coinBotLeft = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(-PJ.transform.localScale.x / 2, 0, PJ.transform.localScale.z / 2));
        //Vector3 coinBotRight = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(-PJ.transform.localScale.x / 2, 0, -PJ.transform.localScale.z / 2));

        //// On récupère coins de l'écran
        //centreScreen = new Vector3(Screen.width / 2, Screen.height / 2, 0); // Centre de l'écran de jeu
        //Vector3 coinUpLeftScreen = centreScreen + new Vector3(-Screen.width / 2, Screen.height / 2, 0);
        //Vector3 coinUpRightScreen = centreScreen + new Vector3(Screen.width / 2, Screen.height / 2, 0);
        //Vector3 coinBotLeftScreen = centreScreen + new Vector3(-Screen.width / 2, -Screen.height / 2, 0);
        //Vector3 coinBotRightScreen = centreScreen + new Vector3(Screen.width / 2, -Screen.height / 2, 0);

        //// Tracés des directions coins cube vers coins écran
        ////Debug.DrawLine(coinUpLeftScreen, coinUpLeft, Color.black);
        ////Debug.DrawLine(coinUpRightScreen, coinUpRight, Color.blue);
        ////Debug.DrawLine(coinBotLeftScreen, coinBotLeft, Color.green);
        ////Debug.DrawLine(coinBotRightScreen, coinBotRight, Color.magenta);

        // On récupère les directions Normalisées partant de ces coins et allant vers les coins de l'écran
        //versCoinUpLeft = coinUpLeftScreen - coinUpLeft;
        //versCoinUpRight = coinUpRightScreen - coinUpRight;
        //versCoinBotLeft = coinBotLeftScreen - coinBotLeft;
        //versCoinBotRight = coinBotRightScreen - coinBotRight;

        //versCoinUpLeft.Normalize();
        //versCoinUpRight.Normalize();
        //versCoinBotLeft.Normalize();
        //versCoinBotRight.Normalize();

        // VERSION 4 : 4 zones divisées selon les coins de la face supérieure du cube et les milieux des 4 arrêtes de l'écran
        // On récupère le centre et les coins de la face supérieure du cube
        // Et, On transfert ces coordonnées dans le repère de l'écran
        Vector3 centreFaceSup = PJ.transform.position + PJ.transform.localScale.y / 2 * Vector3.up; // Le centre n'est pas transféré dans repère écran ICI
        Vector3 coinUp = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(PJ.transform.localScale.x / 2, 0, PJ.transform.localScale.z / 2));
        Vector3 coinRight = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(PJ.transform.localScale.x / 2, 0, -PJ.transform.localScale.z / 2));
        Vector3 coinLeft = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(-PJ.transform.localScale.x / 2, 0, PJ.transform.localScale.z / 2));
        Vector3 coinDown = Camera.main.WorldToScreenPoint(centreFaceSup + new Vector3(-PJ.transform.localScale.x / 2, 0, -PJ.transform.localScale.z / 2));

        // On récupère les milieux des arrêtes de l'écran
        centreScreen = new Vector3(Screen.width / 2, Screen.height / 2, 0); // Centre de l'écran de jeu
        Vector3 milieuUp = centreScreen + new Vector3(0, Screen.height / 2, 0);
        Vector3 milieuDown = centreScreen + new Vector3(0, -Screen.height / 2, 0);
        Vector3 milieuLeft = centreScreen + new Vector3(-Screen.width / 2, 0, 0);
        Vector3 milieuRight = centreScreen + new Vector3(Screen.width / 2, 0, 0);

        // Tracés des directions coins cube vers coins écran
        //Debug.DrawLine(milieuUp, coinUp, Color.black);
        //Debug.DrawLine(milieuRight, coinRight, Color.blue);
        //Debug.DrawLine(milieuLeft, coinLeft, Color.green);
        //Debug.DrawLine(milieuDown, coinDown, Color.magenta);

        // On récupère les directions Normalisées partant de ces coins et allant vers les milieux de l'écran
        coinVersUp = milieuUp - coinUp;
        coinVersRight = milieuRight - coinRight;
        coinVersLeft = milieuLeft - coinLeft;
        coinVersDown = milieuDown - coinDown;

        coinVersUp.Normalize();
        coinVersRight.Normalize();
        coinVersLeft.Normalize();
        coinVersDown.Normalize();
    }

    // Détecte s'il y a un swipe partant du PJ
    public void SwipeOnPJOrCam()
    {
        // VERSION SOURIS
        #if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0)) // Clic gauche == Touch (1 finger)
        {
            if (!EventSystem.current.IsPointerOverGameObject()) // TODO adapter à Android
            {
                startSwipePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z); // Récupérer position du début du swipe

                // Est-ce que celui-ci débute sur le PJ
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit sphereHitInfo;
                if (Physics.Raycast(ray, out sphereHitInfo))
                {
                    if (!GetComponent<MovePlayer>().PJMoving && !GetComponent<CameraController>().camIsSwiping && !GetComponent<BlocEvents>().mirror) // Pour empêcher de superposer les effets de swipes
                    {
                        if (sphereHitInfo.collider.tag == "PJ") // Si clic sur/près de PJ 
                        {
                            startSwipeOnPJ = true;
                            startSwipeCamera = false;
                        }
                        else // Swipe ne commence pas sur PJ donc il concerne la caméra
                        {
                            startSwipeOnPJ = false;
                            startSwipeCamera = true;
                        }
                    }
                }
                else // Swipe commence dans le vide (ou sur qq chose qui n'a pas de collider) donc il concerne la caméra
                {
                    startSwipeOnPJ = false;
                    startSwipeCamera = true;
                }
            }
        }

        if (Input.GetMouseButtonUp(0)) // S'il y a eu un Swipe il est terminé
        {
            if (!EventSystem.current.IsPointerOverGameObject()) // TODO adapter à Android
            {
                endSwipePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z); // Récupérer position de fin du swipe

                currentSwipePos = endSwipePos - startSwipePos; // Récupérer la différence entre début et fin == swipe actuel
                currentSwipePos.Normalize(); // Que l'on normalise

                if (startSwipeOnPJ) // Si le swipe a commencé sur le PJ
                {
                    // Récuperer direction du swipe
                    if (currentSwipePos.y > 0 && currentSwipePos.x < 0)
                    {
                        swipeDirection = SwipeDirection.Up;
                        GetComponent<MovePlayer>().flipUp = true;
                    }
                    //swipe down
                    else if (currentSwipePos.y < 0 && currentSwipePos.x > 0)
                    {
                        swipeDirection = SwipeDirection.Down;
                        GetComponent<MovePlayer>().flipDown = true;
                    }
                    //swipe left
                    else if (currentSwipePos.y < 0 && currentSwipePos.x < 0)
                    {
                        swipeDirection = SwipeDirection.Left;
                        GetComponent<MovePlayer>().flipLeft = true;
                    }
                    //swipe right
                    else if (currentSwipePos.y > 0 && currentSwipePos.x > 0)
                    {
                        swipeDirection = SwipeDirection.Right;
                        GetComponent<MovePlayer>().flipRight = true;
                    }
                    startSwipeOnPJ = false;
                }

                else if (startSwipeCamera) // Sinon le swipe a commencé sur la caméra
                {
                    if (currentSwipePos.x < 0)
                    {
                        swipeDirection = SwipeDirection.Left;
                        GetComponent<CameraController>().swipeCamToLeft = true;
                    }
                    else if (currentSwipePos.x > 0)
                    {
                        swipeDirection = SwipeDirection.Right;
                        GetComponent<CameraController>().swipeCamToRight = true;
                    }
                    startSwipeCamera = false;
                }
            }
        }
        #endif

        // VERSION TOUCH
        int nbTouches = Input.touchCount;

        // ~~~~~~~~~~~~~~~~~~ A SUPPRIMER PEUT ETRE ~~~~~~~~~~~~~~~~~~
        //if (nbTouches > 0) // Si au moins un touch
        //{
        //    for (int i = 0; i < nbTouches; i++)
        //    {
        //        Touch touch = Input.GetTouch(i); // Pour le touch i
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        if (nbTouches == 1)
        {
            Touch touch = Input.GetTouch(0);

            if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId)) // Si touch i pas sur de l'UI
            {
                if (touch.phase == TouchPhase.Began) // Doigt posé sur écran
                {
                    startSwipePos = touch.position; // Récupérer position du début du swipe

                    // Est-ce que celui-ci débute sur le PJ
                    Ray ray = Camera.main.ScreenPointToRay(startSwipePos);
                    RaycastHit sphereHitInfo;
                    if (Physics.Raycast(ray, out sphereHitInfo))
                    {
                        if (!GetComponent<MovePlayer>().PJMoving && !GetComponent<CameraController>().camIsSwiping && !GetComponent<BlocEvents>().mirror) // Pour empêcher de superposer les effets de swipes
                        {
                            if (sphereHitInfo.collider.tag == "PJ") // Si clic sur/près de PJ 
                            {
                                startSwipeOnPJ = true;
                                startSwipeCamera = false;
                            }
                            else // Swipe ne commence pas sur PJ donc il concerne la caméra
                            {
                                startSwipeOnPJ = false;
                                startSwipeCamera = true;
                            }
                        }
                    }
                    else // Swipe commence dans le vide (ou sur qq chose qui n'a pas de collider) donc il concerne la caméra
                    {
                        startSwipeOnPJ = false;
                        startSwipeCamera = true;
                    }
                }

                if (touch.phase == TouchPhase.Ended) // Doigt quitte écran
                {
                    endSwipePos = touch.position; // Récupérer position de fin du swipe

                    currentSwipePos = endSwipePos - startSwipePos; // Récupérer la différence entre début et fin == swipe actuel
                    currentSwipePos.Normalize(); // Que l'on normalise

                    if (startSwipeOnPJ) // Si le swipe a commencé sur le PJ
                    {
                        // Récuperer direction du swipe
                        if (currentSwipePos.y > 0 && currentSwipePos.x < 0)
                        {
                            swipeDirection = SwipeDirection.Up;
                            GetComponent<MovePlayer>().flipUp = true;
                        }
                        //swipe down
                        else if (currentSwipePos.y < 0 && currentSwipePos.x > 0)
                        {
                            swipeDirection = SwipeDirection.Down;
                            GetComponent<MovePlayer>().flipDown = true;
                        }
                        //swipe left
                        else if (currentSwipePos.y < 0 && currentSwipePos.x < 0)
                        {
                            swipeDirection = SwipeDirection.Left;
                            GetComponent<MovePlayer>().flipLeft = true;
                        }
                        //swipe right
                        else if (currentSwipePos.y > 0 && currentSwipePos.x > 0)
                        {
                            swipeDirection = SwipeDirection.Right;
                            GetComponent<MovePlayer>().flipRight = true;
                        }
                        startSwipeOnPJ = false;
                    }

                    else if (startSwipeCamera) // Sinon le swipe a commencé sur la caméra
                    {
                        if (currentSwipePos.x < 0)
                        {
                            swipeDirection = SwipeDirection.Left;
                            GetComponent<CameraController>().swipeCamToLeft = true;
                        }
                        else if (currentSwipePos.x > 0)
                        {
                            swipeDirection = SwipeDirection.Right;
                            GetComponent<CameraController>().swipeCamToRight = true;
                        }
                        startSwipeCamera = false;
                    }
                }
            }
        }
    }
}
