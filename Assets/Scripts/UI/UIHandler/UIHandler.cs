﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class UIHandler : Singleton<UIHandler> {
    [HideInInspector]
    public bool shouldShowIntroScreen = true;
    [HideInInspector]
    public GameObject introScreen;
    [HideInInspector]
    public GameObject menuGlobal;
    [HideInInspector]
    public GameObject phase1;
    [HideInInspector]
    public GameObject phase2;
    [HideInInspector]
    public GameObject options;
    [HideInInspector]
    public GameObject credits;

    // Gère l'affichage de l'intro screen
    public void DisplayIntroScreen()
    {
        if (shouldShowIntroScreen && GameObject.Find("Intro Screen") != null)
        {
            introScreen = GameObject.Find("Intro Screen");
            menuGlobal = GameObject.Find("Menu Global");

            Vector3 targetOnScreen = GameObject.Find("On Screen").transform.position;
            Vector3 targetOffScreen = GameObject.Find("Off Screen").transform.position;

            menuGlobal.GetComponent<RectTransform>().transform.position = targetOffScreen;

            introScreen.GetComponent<RectTransform>().transform.position = targetOnScreen;
        }
    }

    // Responsive UI
    public void AjustUISize()
    {
        introScreen = GameObject.Find("Intro Screen");
        menuGlobal = GameObject.Find("Menu Global");
        options = GameObject.Find("Options");
        credits = GameObject.Find("Crédits");
        phase1 = GameObject.Find("Phase 1");
        phase2 = GameObject.Find("Phase 2");

        introScreen.GetComponent<RectTransform>().sizeDelta = new Vector2(GameObject.Find("Canvas").GetComponent<RectTransform>().rect.width, GameObject.Find("Canvas").GetComponent<RectTransform>().rect.height);
        menuGlobal.GetComponent<RectTransform>().sizeDelta = new Vector2(GameObject.Find("Canvas").GetComponent<RectTransform>().rect.width, GameObject.Find("Canvas").GetComponent<RectTransform>().rect.height);
        options.GetComponent<RectTransform>().sizeDelta = new Vector2(GameObject.Find("Canvas").GetComponent<RectTransform>().rect.width, GameObject.Find("Canvas").GetComponent<RectTransform>().rect.height);
        credits.GetComponent<RectTransform>().sizeDelta = new Vector2(GameObject.Find("Canvas").GetComponent<RectTransform>().rect.width, GameObject.Find("Canvas").GetComponent<RectTransform>().rect.height);
        phase1.GetComponent<RectTransform>().sizeDelta = new Vector2(GameObject.Find("Canvas").GetComponent<RectTransform>().rect.width, GameObject.Find("Canvas").GetComponent<RectTransform>().rect.height);
        phase2.GetComponent<RectTransform>().sizeDelta = new Vector2(GameObject.Find("Canvas").GetComponent<RectTransform>().rect.width, GameObject.Find("Canvas").GetComponent<RectTransform>().rect.height);
    }

    // A utiliser pour déterminer si touch se fait sur UI (un EventSystem element)
    public bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
