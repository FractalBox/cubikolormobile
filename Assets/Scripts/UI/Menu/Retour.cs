﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class Retour : MonoBehaviour, IPointerClickHandler {
    [Space(10)]
    public GameObject menuActuel;
    [Space(10)]
    public GameObject menuPrecedent;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // Clic sur Retour == retour de menuActuel à menuPrecedent
    public void ClicRetour()
    {
        Vector3 targetOnScreen = GameObject.Find("On Screen").transform.position;
        Vector3 targetOffScreen = GameObject.Find("Off Screen").transform.position;

        menuActuel.GetComponent<RectTransform>().transform.position = targetOffScreen;

        menuPrecedent.GetComponent<RectTransform>().transform.position = targetOnScreen;
        
        // TODO Remettre Selector s'il existe sur la lettre/le chiffre du dernier niveau jouer OU sur le dernier sélectionné (déjà le cas) ?? 

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ClicRetour();
    }
}
