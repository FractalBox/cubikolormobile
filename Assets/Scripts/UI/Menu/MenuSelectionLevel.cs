﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class MenuSelectionLevel : MonoBehaviour/*, IPointerClickHandler*/
{

    public GameObject selectorZone;
    public GameObject selectorLevel;
    public GameObject selectorMode;
    [Space(10)]
    public GameObject phase1;
    [Space(10)]
    public GameObject phase2;
    public GameObject titrePhase2;

    [HideInInspector]
    public int countTaps = 0;

    // Use this for initialization
    void Awake()
    {
        GameObject.Find("UIHandler").GetComponent<UIHandler>().DisplayIntroScreen();
        GameObject.Find("UIHandler").GetComponent<UIHandler>().AjustUISize();
    }

    // Update is called once per frame
    void Update()
    {

    }


    // Pour phase 1
    // Premier tap highlight la lettre (fait dans éditeur) + place le selector + update les stats niveau
    public void FirstTap(GameObject levelButton, GameObject selector)
    {
        // Placer Selector sur la bonne lettre
        selector.transform.position = levelButton.transform.position;

        // TODO update stats niveau + Meilleur score  

    }

    // TODO update les stats totales

    // Charger Phase 2 avec le bon Titre (Niveaux: "Lettre"-?)
    // Deuxième tap confirme la sélection et charge la phase 2
    public void SecondTap(GameObject levelButton)
    {
        // Afficher le nouveau menu
        Vector3 targetOnScreen = GameObject.Find("On Screen").transform.position;
        Vector3 targetOffScreen = GameObject.Find("Off Screen").transform.position;

        phase1.GetComponent<RectTransform>().transform.position = targetOffScreen;

        phase2.GetComponent<RectTransform>().transform.position = targetOnScreen;

        // Update le titre de la phase 2
        titrePhase2.GetComponent<Text>().text = "Niveaux: " + levelButton.GetComponentInChildren<Text>().text + "-?";
    }


    // Pour phase 2
    // Un tap pour selectionner le niveau (selector correspondant), on peut ensuite choisir un mode de jeu puis cliquer sur Play + affiche les stats de ce niveau
    // Mode de jeu == clic sur son icône (selector correspondant)
    // Clic sur joueur charge la scène avec le niveau et le mode de jeu choisis et les eventuelles contraintes de ce niveau (TODO aller les chercher, where??)
    // Si aucune stats et contraintes on affiche "-"
    public void LevelClic()
    {
        // On ne peut plus cliquer sur les niveaux (accurate for console, but for android/iOs ??)
        //for (int i = 1; i <= 10; i++)
        //{
        //    phase2.transform.FindChild(i + " Button").GetComponent<Button>().interactable = false;
        //}

        // Boutons du selectGameMode sont interactables
        //for (int i = 0; i < selectGameMode.GetComponentsInChildren<Button>().Length; i++)
        //{
        //    selectGameMode.GetComponentsInChildren<Button>()[i].interactable = true;
        //}
    }


    // Transformer lettre en chiffre de 1 à 15 (A à O) pour calculer le level index dans CreateTerrain
    public int LettreToInt(string lettre)
    {
        if (lettre == "A")
        {
            return 1;
        }
        else if (lettre == "B")
        {
            return 2;
        }
        else if (lettre == "C")
        {
            return 3;
        }
        else if (lettre == "D")
        {
            return 4;
        }
        else if (lettre == "E")
        {
            return 5;
        }
        else if (lettre == "F")
        {
            return 6;
        }
        else if (lettre == "G")
        {
            return 7;
        }
        else if (lettre == "H")
        {
            return 8;
        }
        else if (lettre == "I")
        {
            return 9;
        }
        else if (lettre == "J")
        {
            return 10;
        }
        else if (lettre == "K")
        {
            return 11;
        }
        else if (lettre == "L")
        {
            return 12;
        }
        else if (lettre == "M")
        {
            return 13;
        }
        else if (lettre == "N")
        {
            return 14;
        }
        else if (lettre == "O")
        {
            return 15;
        }
        else return -1;
    }


    // VERSION SOURIS
    // NB attacher ce script à chaque bouton
    // Pour détecter un double clique/simple clique
    //public void OnPointerClick(PointerEventData eventData)
    //{
    //    countTaps = eventData.clickCount;

    //    // Pour les lettres (zones)
    //    if (gameObject.name.Contains("Zone"))
    //    {
    //        if (countTaps == 1)
    //        {
    //            FirstTap();

    //            // Récupérer lettre sélectionnée
    //            GameObject.Find("LevelID").GetComponent<SelectedLevel>().lettrePhase1 = LettreToInt(gameObject.GetComponentInChildren<Text>().text);

    //        }

    //        else if (countTaps == 2)
    //        {
    //            SecondTap();

    //            // Calculer les contraintes éventuelles
    //            GameObject.Find("LevelID").GetComponent<SelectedLevel>().GetConstraints();
    //        }
    //    }

    //    // Pour les chiffres (levels)
    //    else if (gameObject.name.Contains("Level"))
    //    {
    //        FirstTap();

    //        // Récupérer chiffre sélectionné
    //        GameObject.Find("LevelID").GetComponent<SelectedLevel>().chiffrePhase2 = int.Parse(gameObject.GetComponentInChildren<Text>().text);

    //        // Calculer les contraintes éventuelles
    //        GameObject.Find("LevelID").GetComponent<SelectedLevel>().GetConstraints();
    //    }

    //    else // Mode de jeu Classik ou Hardkore
    //    {
    //        FirstTap();

    //        // Récupérer le mode de jeu sélectionné
    //        if (gameObject.GetComponentInChildren<Text>().text == "Classik") GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode = Modes.Classic;
    //        else if (gameObject.GetComponentInChildren<Text>().text == "Hardkore") GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode = Modes.Hardcore;

    //        // Calculer les contraintes éventuelles
    //        GameObject.Find("LevelID").GetComponent<SelectedLevel>().GetConstraints();
    //    }
    //}

    // VERSION IPHONE
    // J'ai mis un EventTrigger avec OnPointerDown qui appelle TapsOnUI()
    // NB attacher ce script une seule fois à la caméra
    // Pour détecter un double clique/simple clique
    public void TapsOnUI()
    {
        float marge = 0.5f;
        // Pour les lettres(zones)
        if (EventSystem.current.currentSelectedGameObject.name.Contains("Zone"))
        {
            if (Vector3.Distance(selectorZone.transform.position, EventSystem.current.currentSelectedGameObject.transform.position) >= - marge && Vector3.Distance(selectorZone.transform.position, EventSystem.current.currentSelectedGameObject.transform.position) <= marge)
            {
                SecondTap(EventSystem.current.currentSelectedGameObject);

                // Calculer les contraintes éventuelles
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().GetConstraints();
            }
            else
            {
                FirstTap(EventSystem.current.currentSelectedGameObject, selectorZone);

                // Récupérer lettre sélectionnée
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().lettrePhase1 = LettreToInt(EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text);
            }
        }

        // Pour les chiffres (levels)
        else if (EventSystem.current.currentSelectedGameObject.name.Contains("Level"))
        {
            FirstTap(EventSystem.current.currentSelectedGameObject, selectorLevel);

            // Récupérer chiffre sélectionné
            GameObject.Find("LevelID").GetComponent<SelectedLevel>().chiffrePhase2 = int.Parse(EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text);

            // Calculer les contraintes éventuelles
            GameObject.Find("LevelID").GetComponent<SelectedLevel>().GetConstraints();
        }

        // Mode de jeu Classik ou Hardkore
        else if(EventSystem.current.currentSelectedGameObject.name.Contains("Klassic") || EventSystem.current.currentSelectedGameObject.name.Contains("Hardkore"))
        {
            FirstTap(EventSystem.current.currentSelectedGameObject, selectorMode);

            // Récupérer le mode de jeu sélectionné
            if (EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text == "Classik") GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode = Modes.Classic;
            else if (EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text == "Hardkore") GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode = Modes.Hardcore;

            // Calculer les contraintes éventuelles
            GameObject.Find("LevelID").GetComponent<SelectedLevel>().GetConstraints();
        }
    }
}
