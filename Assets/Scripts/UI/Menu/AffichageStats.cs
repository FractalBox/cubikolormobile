﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AffichageStats : MonoBehaviour
{
    // Stats d'une zone de (10) niveaux
    [Space(10)]
    [Header("Phase 1")]
    public Text sumTimeStatParLettrePhase1;
    public Text sumMedalsParLettrePhase1; // TODO sumMédaille phase 1 pour une zone (somme des médailles des 10 niveaux)
    // Stats globales de la zone
    public Text sumTimeStatGlobalPhase1;
    public Text sumMedalsGlobalPhase1; // TODO
    
    [Space(10)]
    [Header("Phase 2")]
    public Text timeStatPhase2;
    public Text moveStatPhase2;
    public Image medalPhase2; // TODO médaille phase 2

    [Space(10)]
    [Header("Medals")]
    // TODO Image n'est pas le bon type ici !
    public Image medalOr;
    public Image medalArgent;
    public Image medalBronze;
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AfficherStatsPhase1();
        AfficherStatsPhase2();
    }

    // Affichage du score pour la phase 1
    public void AfficherStatsPhase1()
    {
        PersistentScoreData scoreData = GameObject.Find("SaveScoreDataManager").GetComponent<PersistentScoreData>();
        SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();

        // Récupérer lettre
        // Parcourir 10 niveaux de la zone de cette lettre
        // Faire la somme des temps enregistrés de chaque niveau de cette zone
        float time = 0;
        if (scoreData.levelChronoClassik.Length != 0)
        {
            for (int i = 1; i <= 10; i++)
            {
                time += scoreData.levelChronoClassik[(levelInfo.lettrePhase1 - 1) * 10 + i - 1]; // Quel mode ? Classik pour l'instant
            }
        }
        sumTimeStatParLettrePhase1.text = DisplayTime(time);

        // Somme globale
        float globalTime = 0;
        if (scoreData.levelChronoClassik.Length != 0)
        {
            for (int i = 0; i < levelInfo.levels.Length; i++)
            {
                globalTime += scoreData.levelChronoClassik[i];
            }
        }
        sumTimeStatGlobalPhase1.text = DisplayTime(globalTime);

        // TODO médailles 
    }

    // Affichage du score pour la phase 2
    public void AfficherStatsPhase2()
    {
        PersistentScoreData scoreData = GameObject.Find("SaveScoreDataManager").GetComponent<PersistentScoreData>();
        SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();

        if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode == Modes.Classic)
        {
            if (scoreData.levelChronoClassik.Length != 0) timeStatPhase2.text = "Temps\n" + DisplayTime(scoreData.levelChronoClassik[levelInfo.levelIndex]);
            else timeStatPhase2.text = "Temps\n" + DisplayTime(0);
            if (scoreData.levelNbrCoupsClassik.Length != 0) moveStatPhase2.text = scoreData.levelNbrCoupsClassik[levelInfo.levelIndex].ToString("Coups\n000");
            else moveStatPhase2.text = "Coups\n000";
        }
        else if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode == Modes.Hardcore)
        {
            if (scoreData.levelChronoHardkore.Length != 0) timeStatPhase2.text = "Temps\n" + DisplayTime(scoreData.levelChronoHardkore[levelInfo.levelIndex]);
            else timeStatPhase2.text = "Temps\n" + DisplayTime(0);
            if (scoreData.levelNbrCoupsHardkore.Length != 0) moveStatPhase2.text = scoreData.levelNbrCoupsHardkore[levelInfo.levelIndex].ToString("Coups\n000");
            else moveStatPhase2.text = "Coups\n000";
        }
    }

    // Afficher un instant de temps au bon format
    public string DisplayTime(float time)
    {
        int mn = Mathf.FloorToInt(time / 6000);
        int s = Mathf.FloorToInt((time / 6000 - mn) * 60);
        int cs = Mathf.FloorToInt(((time / 6000 - mn) * 60 - s) * 100);

        return string.Format("{0:00}'{1:00}\"{2:00}", mn, s, cs);
    }
}
