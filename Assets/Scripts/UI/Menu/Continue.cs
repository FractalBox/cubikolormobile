﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Continue : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    
    // Gère le clic sur "Continuer"
    public void OnClicContinuer()
    {       
        // Pour sauter l'intro screen en repassant au Menu
        GameObject UIHandler = GameObject.Find("UIHandler");
        DontDestroyOnLoad(UIHandler);
        UIHandler.GetComponent<UIHandler>().shouldShowIntroScreen = false;

        // Remettre les bonnes données dans LevelID qui doivent être celles du dernier niveau joué
        SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();
        PersistentLastLevelData lastLevelData = GameObject.Find("SaveLastLevelDataManager").GetComponent<PersistentLastLevelData>();
        levelInfo.lettrePhase1 = lastLevelData.lettrePhase1;
        levelInfo.chiffrePhase2 = lastLevelData.chiffrePhase2;
        levelInfo.levelIndex = lastLevelData.levelIndex;
        levelInfo.centiemesForChrono = lastLevelData.centiemesForChrono;
        levelInfo.maxCoupsForCountdown = lastLevelData.maxCoupsForCountdown;

        // Sauvegarde de scores
        DontDestroyOnLoad(GameObject.Find("LevelID"));
        DontDestroyOnLoad(GameObject.Find("SaveScoreDataManager"));
        DontDestroyOnLoad(GameObject.Find("SaveLastLevelDataManager"));

        SceneManager.LoadScene("scene_jeu");
    }
}
