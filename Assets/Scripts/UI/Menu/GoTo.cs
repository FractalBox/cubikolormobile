﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class GoTo : MonoBehaviour/*, IPointerClickHandler*/ {

    [Space(10)]
    public GameObject menuActuel;
    [Space(10)]
    public GameObject menuSuivant;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    // Clic sur Bouton qui ouvre nouvelle fenêtre de menu
    public void ClicGoTo()
    {
        Vector3 targetOnScreen = GameObject.Find("On Screen").transform.position;
        Vector3 targetOffScreen = GameObject.Find("Off Screen").transform.position;

        menuActuel.GetComponent<RectTransform>().transform.position = targetOffScreen;

        menuSuivant.GetComponent<RectTransform>().transform.position = targetOnScreen;

        // TODO Remettre Selector s'il existe sur la lettre/le chiffre du dernier niveau jouer OU sur le dernier sélectionné (déjà le cas) ?? 

    }

    // VERSION SOURIS
    //public void OnPointerClick(PointerEventData eventData)
    //{
    //    ClicGoTo();
    //}

    // VERSION IPHONE
    // J'ai mis un EventTrigger avec OnPointerDown qui appelle GoTo()
    //public void TapOn()
    //{
    //    int nbTouches = Input.touchCount;

    //    if (nbTouches > 0) // Si au moins un touch
    //    {
    //        for (int i = 0; i < nbTouches; i++)
    //        {
    //            Touch touch = Input.GetTouch(i); // Pour le touch i

    //            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId)) // Si touch i est sur de l'UI
    //            {
    //                if (touch.phase == TouchPhase.Began) // Doigt posé sur écran
    //                {
    //                    if (EventSystem.current.currentSelectedGameObject.name.Contains("Continuer"))
    //                    {
    //                        ClicGoTo();
    //                    }
    //                }
    //            }
    //        }            
    //    }
    //}    
}
