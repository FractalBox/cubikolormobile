﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class SelectedLevel : Singleton<SelectedLevel> {

    [Header("Fichier Levels")]
    public TextAsset levelsFile;
    [HideInInspector]
    public string[] levels; // Infos des niveaux séparés par "_" dans le fichier (150 levels en tout, numérpté de 0 à 149)
    [HideInInspector]
    public string[] infosLevelIndex; // Infos du Index-ième niveau séparées par "=" dans le fichier
    [HideInInspector]
    public string[] infosContraintesLevelIndex; // Infos sur les contraintes (chrono et countdown) du indexième level

    [Space(10)]
    public int lettrePhase1 = 1; // Zone du niveau
    public int chiffrePhase2 = 1; // Index du niveau
    public int levelIndex = 1; // Le véritable ID du niveau

    [Space(10)]
    public Modes gameMode = Modes.Classic; // Mode de jeu choisi pour ce niveau
    
    //[HideInInspector]
    public float centiemesForChrono; // A donner au chrono in game
    //[HideInInspector]
    public int maxCoupsForCountdown; // A donner au compteur in game

    
    override public void Awake()
    {
        // Récuperer le text du fichier
        string levelsText = levelsFile.text;

        // Split pour séparer les levels
        levels = levelsText.Split('_'); // Pour récupérer tout de suite les niveaux du jeu pour créer les tableaux de scores notamment
    }

    // Récupérer les contraintes éventuelles du niveau choisi
    // A appeler à chaque clic sur un nouveau numéro de niveau
    public void GetConstraints()
    {
        levelIndex = (lettrePhase1 - 1) * 10 + chiffrePhase2 - 1; // Selon la lettre et le chiffre
                
        // Split pour dégager les infos du indexième niveau des blocs du indexième niveau
        infosLevelIndex = levels[levelIndex].Split('=');

        // Split pour séparer les infos sur les contraintes
        infosContraintesLevelIndex = infosLevelIndex[0].Split('/');
        if(infosContraintesLevelIndex[0].Contains("\n")) infosContraintesLevelIndex[0] = infosContraintesLevelIndex[0].Split('\n')[1]; // On retire ainsi le retour à la ligne pour récupérer le premier chiffre d'info sur les contraintes

        // 3 premiers chiffres sont pour mode Classik
        if (gameMode == Modes.Classic)
        {
            // Chrono
            if (infosContraintesLevelIndex[0] == "0")
            {   
                // Pour le chrono in game
                centiemesForChrono = 0;

                Camera.main.GetComponent<AffichageMenu>().chronoText.GetComponent<Text>().text = "-";
            }
            else
            {
                float centiemes = int.Parse(infosContraintesLevelIndex[1]);
                float secondes = int.Parse(infosContraintesLevelIndex[0]);

                // Pour le chrono in game
                centiemesForChrono = secondes * 100 + centiemes;
                
                // Minutes secondes centièmes
                float time = secondes * 100 + centiemes; // En centièmes
                int mn = Mathf.FloorToInt(time / 6000);
                int s = Mathf.FloorToInt((time / 6000 - mn) * 60);
                int cs = Mathf.FloorToInt(((time / 6000 - mn) * 60 - s) * 100);
                Camera.main.GetComponent<AffichageMenu>().chronoText.GetComponent<Text>().text = string.Format("{0:00}'{1:00}\"{2:00}", mn, s, cs);
            }

            // Max coups
            if (infosContraintesLevelIndex[2] == "0")
            {
                // Pour le compteur in game
                maxCoupsForCountdown = 0;

                Camera.main.GetComponent<AffichageMenu>().countdownText.GetComponent<Text>().text = "-";
            }
            else
            {
                // Pour le compteur in game
                maxCoupsForCountdown = int.Parse(infosContraintesLevelIndex[2]);

                Camera.main.GetComponent<AffichageMenu>().countdownText.GetComponent<Text>().text = string.Format("{000}", int.Parse(infosContraintesLevelIndex[2]));
            }
        }

        // 3 derniers chiffres sont pour mode Hardkore
        if (gameMode == Modes.Hardcore)
        {
            // Chrono
            if (infosContraintesLevelIndex[3] == "0")
            {
                // Pour le chrono in game
                centiemesForChrono = 0;

                Camera.main.GetComponent<AffichageMenu>().chronoText.GetComponent<Text>().text = "-";
            }
            else 
            {
                float centiemes = int.Parse(infosContraintesLevelIndex[4]);
                float secondes = int.Parse(infosContraintesLevelIndex[3]);

                // Pour le chrono in game
                centiemesForChrono = secondes * 100 + centiemes;

                // Minutes secondes centièmes
                float time = secondes * 100 + centiemes; // En centièmes
                int mn = Mathf.FloorToInt(time / 6000);
                int s = Mathf.FloorToInt((time / 6000 - mn) * 60);
                int cs = Mathf.FloorToInt(((time / 6000 - mn) * 60 - s) * 100);
                Camera.main.GetComponent<AffichageMenu>().chronoText.GetComponent<Text>().text = string.Format("{0:00}'{1:00}\"{2:00}", mn, s, cs);
            }

            // Max coups
            if (infosContraintesLevelIndex[5] == "0")
            {
                // Pour le compteur in game
                maxCoupsForCountdown = 0;

                Camera.main.GetComponent<AffichageMenu>().countdownText.GetComponent<Text>().text = "-";
            }
            else
            {
                // Pour le compteur in game
                maxCoupsForCountdown = int.Parse(infosContraintesLevelIndex[5]);

                Camera.main.GetComponent<AffichageMenu>().countdownText.GetComponent<Text>().text = string.Format("{000}", int.Parse(infosContraintesLevelIndex[5]));
            }
        }
    }

    // Gère le clic sur "Jouer"
    public void OnClicJouer()
    {
        DontDestroyOnLoad(gameObject);

        // Pour sauter l'intro screen en repassant au Menu
        GameObject UIHandler = GameObject.Find("UIHandler");
        DontDestroyOnLoad(UIHandler);
        UIHandler.GetComponent<UIHandler>().shouldShowIntroScreen = false;
        
        // Sauvegarde de scores
        DontDestroyOnLoad(GameObject.Find("SaveScoreDataManager"));
        DontDestroyOnLoad(GameObject.Find("SaveLastLevelDataManager"));

        SceneManager.LoadScene("scene_jeu");
    }    
}
