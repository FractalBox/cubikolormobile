﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;

public class GoToMenu : MonoBehaviour/*, IPointerClickHandler*/ {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

    }

    // Gère clic sur bouton menu
    public void OnClicMenu()
    {
        // Remettre les bonnes données dans lastLevel qui doivent être celles du dernier niveau joué
        SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();
        PersistentLastLevelData lastLevelInfo = GameObject.Find("SaveLastLevelDataManager").GetComponent<PersistentLastLevelData>();
        if (GameObject.Find("Main Camera").GetComponent<Victory>().scoreWindow.activeInHierarchy) // En cas de victoire du niveau au moment où on appuie sur Menu
        {
            if (levelInfo.chiffrePhase2 + 1 > 10) // Passage à lettre suivante et donc chiffre devient 1
            {
                levelInfo.lettrePhase1++;
                levelInfo.chiffrePhase2 = 1;
            }
            else
            {
                levelInfo.chiffrePhase2++;
            }
            levelInfo.levelIndex = (levelInfo.lettrePhase1 - 1) * 10 + levelInfo.chiffrePhase2 - 1;
            Camera.main.GetComponent<CreateTerrain>().indexLevel = levelInfo.levelIndex;
            // (Le mode de jeu ne change pas)

            // Recharger les contraintes du niveau suivant
            GameObject.Find("Main Camera").GetComponent<LevelSuivant>().GetLevelConstraints();
            Camera.main.GetComponent<Chrono>().centiemes = levelInfo.centiemesForChrono; // Chrono
            Camera.main.GetComponent<Countdown>().nbrMaxCoups = levelInfo.maxCoupsForCountdown; // Countdown            
        }
        // MàJ des données pour Continuer => niveau suivant si gagné ou le même sinon
        lastLevelInfo.lettrePhase1 = levelInfo.lettrePhase1;
        lastLevelInfo.chiffrePhase2 = levelInfo.chiffrePhase2;
        lastLevelInfo.levelIndex = levelInfo.levelIndex;
        lastLevelInfo.gameMode = levelInfo.gameMode;
        lastLevelInfo.centiemesForChrono = levelInfo.centiemesForChrono;
        lastLevelInfo.maxCoupsForCountdown = levelInfo.maxCoupsForCountdown;

        // Remise à zero du levelID car les selectors se remettent sur A-1 qd on revient dans Menu
        levelInfo.lettrePhase1 = 1;
        levelInfo.chiffrePhase2 = 1;

        // Load les scores pour les afficher dans les menus
        GameObject.Find("SaveScoreDataManager").GetComponent<SaveScoreDataManager>().Load();
        // Load le dernier niveau joué
        GameObject.Find("SaveLastLevelDataManager").GetComponent<SaveLastLevelDataManager>().Load();
        
        SceneManager.LoadScene("scene_menu");
    }

    // VERSION SOURIS
    // Remettre clic event sur le bouton
    //public void OnPointerClick(PointerEventData eventData)
    //{
    //    OnClicMenu();
    //}

    // VERSION IPHONE
    // J'ai mis un EventTrigger avec OnPointerDown qui appelle OnClicMenu()
    //public void TapMenu()
    //{
    //    int nbTouches = Input.touchCount;

    //    if (nbTouches > 0) // Si au moins un touch
    //    {
    //        for (int i = 0; i < nbTouches; i++)
    //        {
    //            Touch touch = Input.GetTouch(i); // Pour le touch i

    //            if (GameObject.Find("UIHandler").GetComponent<UIHandler>().IsPointerOverUIObject() /*EventSystem.current.IsPointerOverGameObject(touch.fingerId)*/) // Si touch i est sur de l'UI
    //            {
    //                if (touch.phase == TouchPhase.Began) // Doigt posé sur écran
    //                {
    //                    Ray ray = Camera.main.ScreenPointToRay(touch.position);
    //                    RaycastHit sphereHitInfo;
    //                    if (Physics.Raycast(ray, out sphereHitInfo))
    //                    {
    //                        // Pour les lettres(zones)
    //                        if (sphereHitInfo.collider.gameObject.name.Contains("Menu"))
    //                        {
    //                            OnClicMenu();
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}
}
