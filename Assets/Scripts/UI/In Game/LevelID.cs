﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelID : MonoBehaviour {

    public GameObject levelID;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        DisplayLevelID();
    }

    // Gère l'affichage du numéro du niveau dans scène_jeu
    public void DisplayLevelID()
    {
        levelID.GetComponentInChildren<Text>().text = IntToLettre(GameObject.Find("LevelID").GetComponent<SelectedLevel>().lettrePhase1) + "-" + GameObject.Find("LevelID").GetComponent<SelectedLevel>().chiffrePhase2;
    }

    // Transformer chiffre en chiffre de A à O (1 à 15)
    public string IntToLettre(int lettre)
    {
        if (lettre == 1)
        {
            return "A";
        }
        else if (lettre == 2)
        {
            return "B";
        }
        else if (lettre == 3)
        {
            return "C";
        }
        else if (lettre == 4)
        {
            return "D";
        }
        else if (lettre == 5)
        {
            return "E";
        }
        else if (lettre == 6)
        {
            return "F";
        }
        else if (lettre == 7)
        {
            return "G";
        }
        else if (lettre == 8)
        {
            return "H";
        }
        else if (lettre == 9)
        {
            return "I";
        }
        else if (lettre == 10)
        {
            return "J";
        }
        else if (lettre == 11)
        {
            return "K";
        }
        else if (lettre == 12)
        {
            return "L";
        }
        else if (lettre == 13)
        {
            return "M";
        }
        else if (lettre == 14)
        {
            return "N";
        }
        else if (lettre == 15)
        {
            return "O";
        }
        else return "";
    }
}
