﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSuivant : MonoBehaviour/*, IPointerClickHandler*/ {
    
    public GameObject scoreWindow; // Bouton "Niveau suivant"

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    // Si clic sur le bouton Suivant pour charger le niveau suivant
    public void OnClicSuivant()
    {
        SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();

        if (levelInfo.chiffrePhase2 + 1 > 10) // Passage à lettre suivante et donc chiffre devient 1
        {
            levelInfo.lettrePhase1++;
            levelInfo.chiffrePhase2 = 1;
        }
        else
        {
            levelInfo.chiffrePhase2++;
        }
        levelInfo.levelIndex = (levelInfo.lettrePhase1 - 1) * 10 + levelInfo.chiffrePhase2 - 1;
        Camera.main.GetComponent<CreateTerrain>().indexLevel = levelInfo.levelIndex;
        // (Le mode de jeu ne change pas)

        // Recharger les contraintes du niveau suivant
        GetLevelConstraints();
        Camera.main.GetComponent<Chrono>().centiemes = levelInfo.centiemesForChrono; // Chrono
        Camera.main.GetComponent<Countdown>().nbrMaxCoups = levelInfo.maxCoupsForCountdown; // Countdown

        Camera.main.GetComponent<Victory>().timeRecordUi.SetActive(false);
        Camera.main.GetComponent<Victory>().moveRecordUi.SetActive(false);
        scoreWindow.SetActive(false);
        Camera.main.GetComponent<Restart>().OnClicRestart();

        // Changer (pour click sur Continuer) les attributs de LastLevelInfo
        PersistentLastLevelData lastLevelInfo = GameObject.Find("SaveLastLevelDataManager").GetComponent<PersistentLastLevelData>();
        lastLevelInfo.lettrePhase1 = levelInfo.lettrePhase1;
        lastLevelInfo.chiffrePhase2 = levelInfo.chiffrePhase2;
        lastLevelInfo.levelIndex = levelInfo.levelIndex;
        lastLevelInfo.gameMode = levelInfo.gameMode;
        lastLevelInfo.centiemesForChrono = levelInfo.centiemesForChrono;
        lastLevelInfo.maxCoupsForCountdown = levelInfo.maxCoupsForCountdown;

        // Sauvegarde du dernier niveau joué
        GameObject.Find("SaveLastLevelDataManager").GetComponent<SaveLastLevelDataManager>().Save();
    }

    // Récupere les contraintes d'un niveau sans affichage particulier à faire
    // Stocke ces valeurs dans champs de LevelID
    public void GetLevelConstraints()
    {
        GameObject.Find("LevelID").GetComponent<SelectedLevel>().levelIndex = (GameObject.Find("LevelID").GetComponent<SelectedLevel>().lettrePhase1 - 1) * 10 + GameObject.Find("LevelID").GetComponent<SelectedLevel>().chiffrePhase2 - 1; // Selon la lettre et le chiffre

        // Récuperer le text du fichier
        string levelsText = GameObject.Find("LevelID").GetComponent<SelectedLevel>().levelsFile.text;

        // Split pour séparer les levels
        GameObject.Find("LevelID").GetComponent<SelectedLevel>().levels = levelsText.Split('_');

        // Split pour dégager les infos du indexième niveau des blocs du indexième niveau
        GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosLevelIndex = GameObject.Find("LevelID").GetComponent<SelectedLevel>().levels[GameObject.Find("LevelID").GetComponent<SelectedLevel>().levelIndex].Split('=');

        // Split pour séparer les infos sur les contraintes
        GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex = GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosLevelIndex[0].Split('/');
        if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[0].Contains("\n")) GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[0] = GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[0].Split('\n')[1]; // On retire ainsi le retour à la ligne pour récupérer le premier chiffre d'info sur les contraintes

        // 3 premiers chiffres sont pour mode Classik
        if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode == Modes.Classic)
        {
            // Chrono
            if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[0] == "0")
            {
                // Pour le chrono in game
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().centiemesForChrono = 0;
            }
            else
            {
                float centiemes = int.Parse(GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[1]);
                float secondes = int.Parse(GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[0]);

                // Pour le chrono in game
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().centiemesForChrono = secondes * 100 + centiemes;
            }

            // Max coups
            if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[2] == "0")
            {
                // Pour le compteur in game
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().maxCoupsForCountdown = 0;
            }
            else
            {
                // Pour le compteur in game
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().maxCoupsForCountdown = int.Parse(GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[2]);
            }
        }

        // 3 derniers chiffres sont pour mode Hardkore
        if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().gameMode == Modes.Hardcore)
        {
            // Chrono
            if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[3] == "0")
            {
                // Pour le chrono in game
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().centiemesForChrono = 0;
            }
            else
            {
                float centiemes = int.Parse(GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[4]);
                float secondes = int.Parse(GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[3]);

                // Pour le chrono in game
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().centiemesForChrono = secondes * 100 + centiemes;
            }

            // Max coups
            if (GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[5] == "0")
            {
                // Pour le compteur in game
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().maxCoupsForCountdown = 0;
            }
            else
            {
                // Pour le compteur in game
                GameObject.Find("LevelID").GetComponent<SelectedLevel>().maxCoupsForCountdown = int.Parse(GameObject.Find("LevelID").GetComponent<SelectedLevel>().infosContraintesLevelIndex[5]);
            }
        }
    }

    // VERSION SOURIS
    //public void OnPointerClick(PointerEventData eventData)
    //{
    //    OnClicSuivant();
    //}

    // VERSION IPHONE
    // J'ai mis un EventTrigger avec OnPointerDown qui appelle OnClicSuivant()
    //public void TapOnSuivant()
    //{
    //    int nbTouches = Input.touchCount;

    //    if (nbTouches > 0) // Si au moins un touch
    //    {
    //        for (int i = 0; i < nbTouches; i++)
    //        {
    //            Touch touch = Input.GetTouch(i); // Pour le touch i

    //            if (GameObject.Find("UIHandler").GetComponent<UIHandler>().IsPointerOverUIObject() /*EventSystem.current.IsPointerOverGameObject(touch.fingerId)*/) // Si touch i est sur de l'UI
    //            {
    //                if (touch.phase == TouchPhase.Began) // Doigt posé sur écran
    //                {
    //                    Ray ray = Camera.main.ScreenPointToRay(touch.position);
    //                    RaycastHit sphereHitInfo;
    //                    if (Physics.Raycast(ray, out sphereHitInfo))
    //                    {
    //                        // Pour les lettres(zones)
    //                        if (sphereHitInfo.collider.gameObject.name.Contains("Suivant"))
    //                        {
    //                            OnClicSuivant();
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}
}
