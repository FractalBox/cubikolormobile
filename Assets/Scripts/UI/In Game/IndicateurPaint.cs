﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IndicateurPaint : MonoBehaviour {

    public GameObject indicateurCoupsPaint;
    private GameObject PJ;
    
	// Use this for initialization
	void Start () {
        PJ = GameObject.Find("PJ");
	}
	
	// Update is called once per frame
	void Update () {
        // Bien placer l'indicateur par rapport au PJ
        indicateurCoupsPaint.GetComponent<RectTransform>().position = RectTransformUtility.WorldToScreenPoint(GameObject.Find("Main Camera").GetComponent<Camera>(), PJ.transform.position + PJ.transform.localScale.y / 2 * Vector3.up);
	}
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Affiche l'indicateur du nbr de coups restant après un paint du PJ
    // Update le nbr de coups affiché à côté du PJ
    // Masque l'indicateur une fois les 10 coups écoulés
    // A appeler une fois le mouvement du PJ fini
    public void UpdateIndicateurCoupsPaint()
    {
        int nbrCoups = GetComponent<BlocEvents>().coupsAvecPaint;

        indicateurCoupsPaint.GetComponentInChildren<Text>().text = nbrCoups.ToString(); // Màj de sa valeur affichée
        indicateurCoupsPaint.SetActive(nbrCoups != 0); // Affichage de l'indicateur
    }
}
