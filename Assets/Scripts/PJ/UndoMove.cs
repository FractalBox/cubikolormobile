﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class UndoMove : MonoBehaviour {

    private GameObject PJ;
    private GameObject pJPivot;

    [HideInInspector]
    public bool doFlipBack = false;
    [HideInInspector]
    public bool effectBack = false;

    // Pour n'autoriser qu'un appel
    private int countCalls = 0;
    private bool oncePiege = false;
    private bool onceMirror = false;
    private bool onceMaJ = false;

    [HideInInspector]
    public bool PJUndoing = false;
    [HideInInspector]
    public int countUndos = 0;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Use this for initialization
    void Start () {
        PJ = GameObject.Find("PJ");
        pJPivot = GameObject.Find("PJPivot");
    }
	
	// Update is called once per frame
	void Update () {
        StartCoroutine("UndoEffect");
        StartCoroutine("FlipBack");
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Arrondissement de valeur à taux près
    private int AroundToInt(float valeur)
    {
        return (int)(Mathf.Round(valeur));
    }

    // VERSION SOURIS
    // Remettre clic event sur le bouton
    // A appeler au clic sur Undo/GoBack
    public void OnClicUndo()
    {
        effectBack = (!GetComponent<BlocEvents>().PJUnderTerrainEffect && !GetComponent<MovePlayer>().PJMoving && GetComponent<MovePlayer>().PJPositions.Count != 0 && GetComponent<MovePlayer>().PJFlipDirections.Count != 0); // Il y a eu des mouvements PJ réalisés donc on peut faire un Undo
        oncePiege = effectBack;
        onceMirror = effectBack;
        onceMaJ = effectBack;
    }

    // VERSION IPHONE
    // J'ai mis un EventTrigger avec OnPointerDown qui appelle OnClicUndo()
    //public void TapUndo()
    //{
    //    int nbTouches = Input.touchCount;

    //    if (nbTouches > 0) // Si au moins un touch
    //    {
    //        for (int i = 0; i < nbTouches; i++)
    //        {
    //            Touch touch = Input.GetTouch(i); // Pour le touch i

    //            if (GameObject.Find("UIHandler").GetComponent<UIHandler>().IsPointerOverUIObject() /*EventSystem.current.IsPointerOverGameObject(touch.fingerId)*/) // Si touch i est sur de l'UI
    //            {
    //                if (touch.phase == TouchPhase.Began) // Doigt posé sur écran
    //                {
    //                    Ray ray = Camera.main.ScreenPointToRay(touch.position);
    //                    RaycastHit sphereHitInfo;
    //                    if (Physics.Raycast(ray, out sphereHitInfo))
    //                    {
    //                        // Pour les lettres(zones)
    //                        if (sphereHitInfo.collider.gameObject.name.Contains("Undo"))
    //                        {
    //                            effectBack = (!GetComponent<BlocEvents>().PJUnderTerrainEffect && !GetComponent<MovePlayer>().PJMoving && GetComponent<MovePlayer>().PJPositions.Count != 0 && GetComponent<MovePlayer>().PJFlipDirections.Count != 0); // Il y a eu des mouvements PJ réalisés donc on peut faire un Undo
    //                            oncePiege = effectBack;
    //                            onceMirror = effectBack;
    //                            onceMaJ = effectBack;
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}

    // Animation flip back du PJ
    // Il compte pour un coup donc nbPJMove ++
    public IEnumerator FlipBack()
    {
        if (doFlipBack && GetComponent<MovePlayer>().PJFlipDirections.Count != 0)
        {
            if (GetComponent<MovePlayer>().PJFlipDirections[GetComponent<MovePlayer>().PJFlipDirections.Count - 1] == SwipeDirection.Left)
            {
                // PJ est entrain de faire un flip donc on ne peut en démarrer un autre
                PJUndoing = true;
                
                // Pivot
                float currentCamRot = GetComponent<MovePlayer>().cameraRotations[GetComponent<MovePlayer>().cameraRotations.Count - 1];
                Vector3 centerEdgeDirection = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(1, -1, 0);
                pJPivot.transform.position = PJ.transform.position + PJ.transform.localScale.x / 2 * centerEdgeDirection;
                pJPivot.transform.rotation = GetComponent<MovePlayer>().initialRotation;                
                
                // Target de rotation pour le Slerp
                Vector3 rotationAxis = Quaternion.AngleAxis(currentCamRot, Vector3.up) * Vector3.forward;
                GetComponent<MovePlayer>().targetRotation = GetComponent<MovePlayer>().GetTargetQuaternion(GetComponent<MovePlayer>().initialRotation, Quaternion.AngleAxis(-GetComponent<MovePlayer>().angleRot, rotationAxis));

                // Attacher PJ au pivot pour qu'il suive la rotation
                PJ.transform.parent = pJPivot.transform;

                // Sortie de cette boucle pour ne faire qu'un seul flip
                GetComponent<MovePlayer>().PJFlipDirections[GetComponent<MovePlayer>().PJFlipDirections.Count - 1] = SwipeDirection.None;
            }

            else if (GetComponent<MovePlayer>().PJFlipDirections[GetComponent<MovePlayer>().PJFlipDirections.Count - 1] == SwipeDirection.Right)
            {
                // PJ est entrain de faire un flip donc on ne peut en démarrer un autre
                PJUndoing = true;

                // Pivot 
                float currentCamRot = GetComponent<MovePlayer>().cameraRotations[GetComponent<MovePlayer>().cameraRotations.Count - 1];
                Vector3 centerEdgeDirection = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(-1, -1, 0);
                pJPivot.transform.position = PJ.transform.position + PJ.transform.localScale.x / 2 * centerEdgeDirection;
                pJPivot.transform.rotation = GetComponent<MovePlayer>().initialRotation;

                // Target de rotation pour le Slerp
                Vector3 rotationAxis = Quaternion.AngleAxis(currentCamRot, Vector3.up) * Vector3.forward;
                GetComponent<MovePlayer>().targetRotation = GetComponent<MovePlayer>().GetTargetQuaternion(GetComponent<MovePlayer>().initialRotation, Quaternion.AngleAxis(GetComponent<MovePlayer>().angleRot, rotationAxis));

                // Attacher PJ au pivot pour qu'il suive la rotation
                PJ.transform.parent = pJPivot.transform;

                // Sortie de cette boucle pour ne faire qu'un seul flip
                GetComponent<MovePlayer>().PJFlipDirections[GetComponent<MovePlayer>().PJFlipDirections.Count - 1] = SwipeDirection.None;
            }
            else if (GetComponent<MovePlayer>().PJFlipDirections[GetComponent<MovePlayer>().PJFlipDirections.Count - 1] == SwipeDirection.Up)
            {
                // PJ est entrain de faire un flip donc on ne peut en démarrer un autre
                PJUndoing = true;

                // Pivot
                float currentCamRot = GetComponent<MovePlayer>().cameraRotations[GetComponent<MovePlayer>().cameraRotations.Count - 1];
                Vector3 centerEdgeDirection = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(0, -1, -1);
                pJPivot.transform.position = PJ.transform.position + PJ.transform.localScale.x / 2 * centerEdgeDirection;
                pJPivot.transform.rotation = GetComponent<MovePlayer>().initialRotation;
                
                // Target de rotation pour le Slerp
                Vector3 rotationAxis = Quaternion.AngleAxis(currentCamRot, Vector3.up) * Vector3.right;
                GetComponent<MovePlayer>().targetRotation = GetComponent<MovePlayer>().GetTargetQuaternion(GetComponent<MovePlayer>().initialRotation, Quaternion.AngleAxis(-GetComponent<MovePlayer>().angleRot, rotationAxis));
                
                // Attacher PJ au pivot pour qu'il suive la rotation
                PJ.transform.parent = pJPivot.transform;

                // Sortie de cette boucle pour ne faire qu'un seul flip
                GetComponent<MovePlayer>().PJFlipDirections[GetComponent<MovePlayer>().PJFlipDirections.Count - 1] = SwipeDirection.None;
            }
            else if (GetComponent<MovePlayer>().PJFlipDirections[GetComponent<MovePlayer>().PJFlipDirections.Count - 1] == SwipeDirection.Down)
            {
                // PJ est entrain de faire un flip donc on ne peut en démarrer un autre
                PJUndoing = true;

                // Pivot
                float currentCamRot = GetComponent<MovePlayer>().cameraRotations[GetComponent<MovePlayer>().cameraRotations.Count - 1];
                Vector3 centerEdgeDirection = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(0, -1, 1);
                pJPivot.transform.position = PJ.transform.position + PJ.transform.localScale.x / 2 * centerEdgeDirection;
                pJPivot.transform.rotation = GetComponent<MovePlayer>().initialRotation;

                // Target de rotation pour le Slerp
                Vector3 rotationAxis = Quaternion.AngleAxis(currentCamRot, Vector3.up) * Vector3.right;
                GetComponent<MovePlayer>().targetRotation = GetComponent<MovePlayer>().GetTargetQuaternion(GetComponent<MovePlayer>().initialRotation, Quaternion.AngleAxis(GetComponent<MovePlayer>().angleRot, rotationAxis));

                // Attacher PJ au pivot pour qu'il suive la rotation
                PJ.transform.parent = pJPivot.transform;

                // Sortie de cette boucle pour ne faire qu'un seul flip
                GetComponent<MovePlayer>().PJFlipDirections[GetComponent<MovePlayer>().PJFlipDirections.Count - 1] = SwipeDirection.None;
            }

            // Mouvement rotation flip du pivot auquel est attaché le PJ
            pJPivot.transform.rotation = Quaternion.Slerp(pJPivot.transform.rotation, GetComponent<MovePlayer>().targetRotation, Time.deltaTime * GetComponent<MovePlayer>().flipSpeed);

            // Condition d'arrêt mouvement flip
            yield return new WaitUntil(() => pJPivot.transform.rotation == GetComponent<MovePlayer>().targetRotation);

            // Tout ce qui appelé ci-dessous n'est appelé qu'une seule fois
            if (countCalls == 0)
            {
                // On efface les mémoires qui concernent le PJ
                GetComponent<MovePlayer>().cameraRotations.RemoveAt(GetComponent<MovePlayer>().cameraRotations.Count - 1);
                GetComponent<MovePlayer>().PJPositions.RemoveAt(GetComponent<MovePlayer>().PJPositions.Count - 1);
                GetComponent<MovePlayer>().PJRotations.RemoveAt(GetComponent<MovePlayer>().PJRotations.Count - 1);
                GetComponent<MovePlayer>().PJFlipDirections.RemoveAt(GetComponent<MovePlayer>().PJFlipDirections.Count - 1);

                // Reset
                PJ.transform.parent = null;
                GetComponent<MovePlayer>().targetRotation = new Quaternion();
                GetComponent<MovePlayer>().initialPosition = PJ.transform.position;
                GetComponent<MovePlayer>().initialRotation = PJ.transform.rotation;
                pJPivot.transform.position = GetComponent<MovePlayer>().initialPosition;
                pJPivot.transform.rotation = GetComponent<MovePlayer>().initialRotation;

                // Ajustement
                PJ.transform.position = new Vector3(AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.y), AroundToInt(PJ.transform.position.z));

                PJUndoing = false;
                doFlipBack = false;

                countUndos++;

                countCalls++;
                yield return new WaitForSeconds(0.1f);
                countCalls = 0;
            }
        }
    }

    // Animation d'effet inverse à déclencher avant le flip back du PJ
    public IEnumerator UndoEffect()
    {
        if (effectBack && !PJUndoing)
        {
            // Récupérer bloc sous le PJ à repositionner comme il était avant le précédent mvt enregistré
            GameObject blocSousPJ = GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)];
            
            // Si le bloc avant le dernier mvt du PJ est un piège
            // Le recréer et le remettre dans les matrices du level
            if (GetComponent<BlocEvents>().typesBloc.Count > 1 && GetComponent<BlocEvents>().typesBloc[GetComponent<BlocEvents>().typesBloc.Count - 2] == "O")
            {
                if (oncePiege) // Si on ne met pas ce bool ça créé X Pieges Recréés mais en repassant dessus, le PJ ne détruit qu'un seul piège et pas les X
                {
                    // Bloc sous le PJ avant son dernier mouvement
                    List<Vector3> posPJ = GetComponent<MovePlayer>().PJPositions; // Positions précédentes du PJ
                    GameObject blocPiegeAvant = (GameObject)Instantiate(GetComponent<CreateTerrain>().cubePiege, new Vector3(AroundToInt(posPJ[posPJ.Count - 1].x), AroundToInt(posPJ[posPJ.Count - 1].y - PJ.transform.localScale.y), AroundToInt(posPJ[posPJ.Count - 1].z)), Quaternion.identity);
                    blocPiegeAvant.name = "Piege recree";
                    blocPiegeAvant.transform.parent = GameObject.Find("Level").transform;

                    GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(posPJ[posPJ.Count - 1].x), AroundToInt(posPJ[posPJ.Count - 1].z)] = blocPiegeAvant;
                    GetComponent<CreateTerrain>().levelMatrix[AroundToInt(posPJ[posPJ.Count - 1].x), AroundToInt(posPJ[posPJ.Count - 1].z)] = "O/O/O/O/" + AroundToInt(posPJ[posPJ.Count - 1].y - PJ.transform.localScale.y).ToString();

                    // Si le bloc avant le dernier mvt du PJ est un verrou
                    if (GetComponent<BlocEvents>().typesBloc[GetComponent<BlocEvents>().typesBloc.Count - 1] == "Z")
                    {
                        blocSousPJ.GetComponent<Renderer>().material = GetComponent<BlocEvents>().blocVerouilleMat;
                        blocSousPJ.name = "End again After Undo";
                    }
                }
                oncePiege = false;
            }

            // Si le bloc sous le PJ est un mirror, il faut remmettre le level comme avant l'effet
            // Donc réinverser les signes des hauteurs des blocs + refaire mvt shockwave
            if (GetComponent<BlocEvents>().typesBloc[GetComponent<BlocEvents>().typesBloc.Count - 1] == "M")
            {
                // On met PJ en enfant du bloc mirror pour qu'il suive la transformation du niveau
                GameObject blocMirror = GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)];
                PJ.transform.parent = blocMirror.transform;

                // On récupère le bloc qui est le plus éloigné de l'axe Y
                GameObject blocMostFar = blocMirror; // Arbitrairement le bloc mirror

                for (int i = 0; i < GetComponent<CreateTerrain>().nbrColonnesLignes; i++)
                {
                    for (int j = 0; j < GetComponent<CreateTerrain>().nbrColonnesLignes; j++)
                    {
                        GameObject bloc = GetComponent<CreateTerrain>().blocsMatrix[i, j];

                        if (bloc != null)
                        {
                            // Déterminer quel est le plus loin de Y
                            if (Mathf.Abs(AroundToInt(bloc.transform.position.y)) > Mathf.Abs(AroundToInt(blocMostFar.transform.position.y))) blocMostFar = bloc;
                        }
                    }
                }

                // Mouvements des blocs SAUF le plus loin de Y
                for (int i = 0; i < GetComponent<CreateTerrain>().nbrColonnesLignes; i++)
                {
                    for (int j = 0; j < GetComponent<CreateTerrain>().nbrColonnesLignes; j++)
                    {
                        GameObject bloc = GetComponent<CreateTerrain>().blocsMatrix[i, j];

                        if (bloc != null)
                        {
                            if (bloc != blocMostFar) StartCoroutine(GetComponent<BlocEvents>().Shockwave(bloc));
                        }
                    }
                }

                // Pour que tous les blocs est le temps de bouger, on fait bouger le plus loin à part en dernier
                string[] infosBlocMostFar = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(blocMostFar.transform.position.x), AroundToInt(blocMostFar.transform.position.z)].Split('/');
                Vector3 targetMostFarPos = new Vector3(AroundToInt(blocMostFar.transform.position.x), -int.Parse(infosBlocMostFar[4]), AroundToInt(blocMostFar.transform.position.z));
                if (blocMostFar.transform.position != targetMostFarPos) blocMostFar.transform.position = Vector3.Lerp(blocMostFar.transform.position, targetMostFarPos, Time.deltaTime * GetComponent<BlocEvents>().maxSpeedBlocs);
                yield return new WaitUntil(() => blocMostFar.transform.position == targetMostFarPos);

                if (blocMostFar.transform.position == targetMostFarPos)
                {
                    // Ajustements de la position de chaque bloc 
                    for (int i = 0; i < GetComponent<CreateTerrain>().nbrColonnesLignes; i++)
                    {
                        for (int j = 0; j < GetComponent<CreateTerrain>().nbrColonnesLignes; j++)
                        {
                            if (GetComponent<CreateTerrain>().blocsMatrix[i, j] != null)
                            {
                                GetComponent<CreateTerrain>().blocsMatrix[i, j].transform.position = new Vector3(AroundToInt(GetComponent<CreateTerrain>().blocsMatrix[i, j].transform.position.x), AroundToInt(GetComponent<CreateTerrain>().blocsMatrix[i, j].transform.position.y), AroundToInt(GetComponent<CreateTerrain>().blocsMatrix[i, j].transform.position.z));
                            }
                        }
                    }
                }

                if (onceMirror)
                {
                    // Si bloc sous PJ est un paint, il faut remettre au PJ son material original et nbcoups à 0 puis le bouger 
                    // Si le bloc avant le dernier mvt est un PaintPJ, il faut qd PJ retombe dessus ré-incrémenter son nbrcoups à 10
                    // Sinon mais si PJ a été paint, il faut ré-incrémenter son nbrcoups qd il tombe sur le bloc d'avant son mvt, sinon rien
                    if (GetComponent<BlocEvents>().coupsAvecPaint != 0 && GetComponent<BlocEvents>().coupsAvecPaint + 1 <= 10) GetComponent<BlocEvents>().coupsAvecPaint++;
                    else if (GetComponent<BlocEvents>().coupsAvecPaint == 10)
                    {
                        GetComponent<BlocEvents>().coupsAvecPaint = 0;
                        PJ.GetComponent<Renderer>().material = GetComponent<MovePlayer>().originalPjMaterial;
                    }
                    GetComponent<IndicateurPaint>().UpdateIndicateurCoupsPaint();

                    // Effacer mémoire des mvts annulés
                    GetComponent<BlocEvents>().positionsBlocAvantEffet.RemoveAt(GetComponent<BlocEvents>().positionsBlocAvantEffet.Count - 1);
                    GetComponent<BlocEvents>().positionsBlocDetruit.RemoveAt(GetComponent<BlocEvents>().positionsBlocDetruit.Count - 1);
                    GetComponent<BlocEvents>().positionsBlocsCrees.RemoveAt(GetComponent<BlocEvents>().positionsBlocsCrees.Count - 1);
                    GetComponent<BlocEvents>().typesBloc.RemoveAt(GetComponent<BlocEvents>().typesBloc.Count - 1);

                    effectBack = false;
                    GetComponent<MovePlayer>().nbPJMoves++;
                    doFlipBack = true; // On peut maintenant commencer le flip back du PJ
                }
                onceMirror = false;
            }

            // Dans le cas sans Mirror
            else
            {
                PJ.transform.parent = blocSousPJ.transform;

                // Si le bloc avant le dernier mvt du PJ est un verrou
                if (GetComponent<BlocEvents>().typesBloc[GetComponent<BlocEvents>().typesBloc.Count - 1] == "Z")
                {
                    blocSousPJ.GetComponent<Renderer>().material = GetComponent<BlocEvents>().blocVerouilleMat;
                    blocSousPJ.name = "End again After Undo";
                }

                // Recréer bloc détruit
                if (GetComponent<BlocEvents>().positionsBlocDetruit[GetComponent<BlocEvents>().positionsBlocDetruit.Count - 1] != new Vector3(-1, -1, -1))
                {
                    GameObject blocRecree = (GameObject)Instantiate(GetComponent<CreateTerrain>().cubeNeutre, GetComponent<BlocEvents>().positionsBlocDetruit[GetComponent<BlocEvents>().positionsBlocDetruit.Count - 1], Quaternion.identity);
                    blocRecree.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);
                    blocRecree.name = "Colonne recréée";
                    blocRecree.transform.parent = blocSousPJ.transform;
                }

                // Target position du bloc avant l'effet
                Vector3 targetPos = GetComponent<BlocEvents>().positionsBlocAvantEffet[GetComponent<BlocEvents>().positionsBlocAvantEffet.Count - 1];

                if (onceMaJ) // Ne faire qu'une fois cette mise à jour
                {
                    // Remettre à jour dans les matrices du niveau
                    int variationHeight = AroundToInt(targetPos.y) - AroundToInt(blocSousPJ.transform.position.y);
                    string[] infosBlocSousPJ = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)].Split('/');
                    GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)] = infosBlocSousPJ[0] + "/" + infosBlocSousPJ[1] + "/" + infosBlocSousPJ[2] + "/" + infosBlocSousPJ[3] + "/" + (int.Parse(infosBlocSousPJ[4]) + variationHeight).ToString();
                    GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)] = blocSousPJ;
                }
                onceMaJ = false;

                // Faire l'effet inverse (si le cube est Neutre il ne se passe rien) grâce aux 3 listes de mémoires de positions des blocsLevel et de ceux éventuellement créés ou détruit
                blocSousPJ.transform.position = Vector3.Lerp(blocSousPJ.transform.position, targetPos, Time.deltaTime * GetComponent<BlocEvents>().speedPJ * 2);

                // Arrêt de l'annulation
                yield return new WaitUntil(() => blocSousPJ.transform.position == targetPos);

                // Tout ici est appelé qu'une seule fois
                if (countCalls == 0)
                {
                    // Libérer PJ
                    PJ.transform.parent = null;

                    // Si bloc sous PJ est un paint, il faut remettre au PJ son material original et nbcoups à 0 puis le bouger 
                    // Si le bloc avant le dernier mvt est un PaintPJ, il faut qd PJ retombe dessus ré-incrémenter son nbrcoups à 10
                    // Sinon mais si PJ a été paint, il faut ré-incrémenter son nbrcoups qd il tombe sur le bloc d'avant son mvt, sinon rien
                    if (GetComponent<BlocEvents>().coupsAvecPaint != 0 && GetComponent<BlocEvents>().coupsAvecPaint + 1 <= 10) GetComponent<BlocEvents>().coupsAvecPaint++;
                    else if (GetComponent<BlocEvents>().coupsAvecPaint == 10)
                    {
                        GetComponent<BlocEvents>().coupsAvecPaint = 0;
                        PJ.GetComponent<Renderer>().material = GetComponent<MovePlayer>().originalPjMaterial;
                    }
                    GetComponent<IndicateurPaint>().UpdateIndicateurCoupsPaint();

                    // Détruire blocs créés
                    List<Vector3> lastListPos = GetComponent<BlocEvents>().positionsBlocsCrees[GetComponent<BlocEvents>().positionsBlocsCrees.Count - 1];

                    for (int j = 0; j < lastListPos.Count; j++)
                    {
                        Vector3 posJ = lastListPos[j];
                        GameObject blocLevelAtPosJ = GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(posJ.x), AroundToInt(posJ.z)];
                        GameObject blocADetruire = blocLevelAtPosJ;

                        if (blocLevelAtPosJ.transform.childCount != 0)
                        {
                            for (int k = 0; k < blocLevelAtPosJ.transform.childCount; k++)
                            {
                                if (blocLevelAtPosJ.transform.GetChild(k).position.y <= blocADetruire.transform.position.y) blocADetruire = blocLevelAtPosJ.transform.GetChild(k).gameObject;
                            }
                        }

                        Destroy(blocADetruire);
                    }

                    // Effacer mémoire des mvts annulés
                    GetComponent<BlocEvents>().positionsBlocAvantEffet.RemoveAt(GetComponent<BlocEvents>().positionsBlocAvantEffet.Count - 1);
                    GetComponent<BlocEvents>().positionsBlocDetruit.RemoveAt(GetComponent<BlocEvents>().positionsBlocDetruit.Count - 1);
                    GetComponent<BlocEvents>().positionsBlocsCrees.RemoveAt(GetComponent<BlocEvents>().positionsBlocsCrees.Count - 1);
                    GetComponent<BlocEvents>().typesBloc.RemoveAt(GetComponent<BlocEvents>().typesBloc.Count - 1);

                    effectBack = false;
                    GetComponent<MovePlayer>().nbPJMoves++;
                    doFlipBack = true; // On peut maintenant commencer le flip back du PJ

                    countCalls++;
                    yield return new WaitForSeconds(0.1f);
                    countCalls = 0;
                }
            }
        }
    }
}