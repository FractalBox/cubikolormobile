﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Collisions : MonoBehaviour
{
    private GameObject PJ;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void Start()
    {
        PJ = GameObject.Find("PJ");
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Arrondissement de valeur à taux près
    private int AroundToInt(float valeur)
    {
        return (int)(Mathf.Round(valeur));
    }

    // Vérifie que le bloc dans la levelMatrix qui correspond à la targetPos vers laquelle se dirige le PJ est dans les limites de mvt possible
    // A savoir, le PJ ne peut être sur un "N" et il ne peut être que sur un bloc de la même hauteur
    // Enfin, le PJ.x et PJ.z doivent être compris entre 0 et nbColonnesLignes
    public bool CheckCollisions(Vector3 targetPos)
    {
        if (0 <= AroundToInt(targetPos.x) && AroundToInt(targetPos.x) <= GetComponent<CreateTerrain>().nbrColonnesLignes - 1
            && 0 <= AroundToInt(targetPos.z) && AroundToInt(targetPos.z) <= GetComponent<CreateTerrain>().nbrColonnesLignes - 1) // Si target est dans les limites du level (ie dans la levelMatrix)
        {
            string blocText = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(targetPos.x), AroundToInt(targetPos.z)]; // Bloc de la targetPos
            string[] blocInfos = blocText.Split('/'); // Infos de ce bloc (4 colors et height)

            if (blocInfos[0] != "N") // Pas un bloc N
            {
                if (int.Parse(blocInfos[4]) + AroundToInt(PJ.transform.localScale.y) == AroundToInt(targetPos.y)) // On check la hauteur
                {
                    return true;
                }
                else return false; //si hauteur bloc est différente de celle de la target (qui est forcément celle du PJ) alors impossible
            }
            else return false; // Bloc N donc impossible
        }
        else return false; // Sinon hors levelMatrix donc impossible
    }
}
