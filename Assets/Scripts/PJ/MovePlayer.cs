using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MovePlayer : MonoBehaviour 
{
    private GameObject PJ;
    private GameObject pJPivot;
    private GameObject camPivot;

    public Material originalPjMaterial;

    [HideInInspector]
    public Vector3 initialPJPosition;

    // Pour le flip back
    [HideInInspector]
    public List<SwipeDirection> PJFlipDirections;
    [HideInInspector]
    public List<float> cameraRotations; // Les rotations de caméra au moment des flips pour que le flip back se fasse selon l'orientation de la caméra correspondante et non selon l'actuelle qui peut être différente
    [HideInInspector]
    public List<Vector3> PJPositions;
    [HideInInspector]
    public List<Quaternion> PJRotations;

    [Space(10)]
    public float flipSpeed = 8f;
    public float angleRot = 90;

    [HideInInspector]
    public Vector3 initialPosition;
    [HideInInspector]
    public Quaternion initialRotation;
    [HideInInspector]
    public Quaternion targetRotation;

    [Space(10)]
    [HideInInspector]
    public bool PJMoving;
    [HideInInspector]
    public bool flipUp = false;
    [HideInInspector]
    public bool flipDown = false;
    [HideInInspector]
    public bool flipLeft = false;
    [HideInInspector]
    public bool flipRight = false;

    private int countCalls = 0; // Pour empêcher double appel à la fonction d'effet de mvt sur PJ dû à un bloc Color

    [HideInInspector]
    public int nbPJMoves = 0;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private void Start()
    {
        PJ = GameObject.Find("PJ");
        pJPivot = GameObject.Find("PJPivot");
        camPivot = GameObject.Find("CamPivot");

        PJ.transform.position = initialPJPosition;

        initialPosition = PJ.transform.position;
        initialRotation = PJ.transform.rotation;

        pJPivot.transform.position = initialPosition;
        pJPivot.transform.rotation = initialRotation;

        GetComponent<BlocEvents>().positionsBlocAvantEffet = new List<Vector3>();
        GetComponent<BlocEvents>().positionsBlocsCrees = new List<List<Vector3>>();
        GetComponent<BlocEvents>().positionsBlocDetruit = new List<Vector3>();
        GetComponent<BlocEvents>().typesBloc = new List<string>();
        
        // Pour enregistrer les données du bloc Start pour le Undo
        GetComponent<BlocEvents>().positionsBlocAvantEffet.Add(new Vector3(AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.y - PJ.transform.localScale.y), AroundToInt(PJ.transform.position.z)));
        GetComponent<BlocEvents>().positionsBlocsCrees.Add(new List<Vector3>());
        GetComponent<BlocEvents>().positionsBlocDetruit.Add(new Vector3(-1, -1, -1));
        GetComponent<BlocEvents>().typesBloc.Add("A"); // Ca sera toujours bloc Start en premier donc A
    }

    private void Update()
    {
        StartCoroutine("MovePJ");
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Arrondissement de valeur à taux près
    private float Around(float valeur, float taux)
    {
        return Mathf.Round(valeur * taux) / taux;
    }

    // Arrondissement de valeur à taux près
    private int AroundToInt(float valeur)
    {
        return (int)(Mathf.Round(valeur));
    }

    // Récupère la nouvelle target de rotation pour le pivot
    public Quaternion GetTargetQuaternion(Quaternion initial, Quaternion rotation)
    {
        return rotation * initial;
    }
    
    public IEnumerator MovePJ()
    {        
        // PJ peut bouger
        if (!GetComponent<BlocEvents>().PJUnderTerrainEffect && !GetComponent<UndoMove>().PJUndoing)
        {
            if (flipLeft)
            {
                // Déterminer vecteur direction du mouvement
                float currentCamRot = camPivot.transform.localRotation.eulerAngles.y;
                Vector3 centerEdgeDirection = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(-1, -1, 0);

                // Target positions pour check les collisions
                Vector3 direction = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(-1, 0, 0);
                Vector3 targetPos = PJ.transform.position + direction;
                
                // Check collisions pour autoriser le mouvement
                if (GetComponent<Collisions>().CheckCollisions(targetPos))
                {
                    // PJ est entrain de faire un flip donc on ne peut en démarrer un autre
                    PJMoving = true;

                    // Pour le Flip back
                    cameraRotations.Add(camPivot.transform.localRotation.eulerAngles.y);
                    PJFlipDirections.Add(GetComponent<SwipeInput>().swipeDirection);
                    PJPositions.Add(PJ.transform.position);
                    PJRotations.Add(PJ.transform.rotation);

                    // Pivot
                    pJPivot.transform.position = PJ.transform.position + PJ.transform.localScale.x / 2 * centerEdgeDirection;
                    pJPivot.transform.rotation = initialRotation;

                    // Target de rotation pour le Slerp
                    Vector3 rotationAxis = Quaternion.AngleAxis(currentCamRot, Vector3.up) * Vector3.forward;
                    targetRotation = GetTargetQuaternion(initialRotation, Quaternion.AngleAxis(angleRot, rotationAxis));

                    // Attacher PJ au pivot pour qu'il suive la rotation
                    PJ.transform.parent = pJPivot.transform;

                    // Si y'a un bloc piège avant mvt du PJ
                    GetComponent<BlocEvents>().EffacerBlocPiege(PJ.transform.position);

                    // Décrémenter le nbr de coups (Paint) possibles au cas où PJ est painté
                    if(GetComponent<BlocEvents>().coupsAvecPaint - 1 >= 0) GetComponent<BlocEvents>().coupsAvecPaint--;

                    // On compte ce nouveau mvt du PJ
                    nbPJMoves++;
                }

                // Sortie de cette boucle pour ne faire qu'une seule rotation
                flipLeft = false;
            }

            else if (flipRight)
            {
                // Déterminer vecteur direction du mouvement
                float currentCamRot = camPivot.transform.localRotation.eulerAngles.y;
                Vector3 centerEdgeDirection = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(1, -1, 0);

                // Target positions pour check les collisions
                Vector3 direction = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(1, 0, 0);
                Vector3 targetPos = PJ.transform.position + direction;

                // Check collisions pour autoriser le mouvement
                if (GetComponent<Collisions>().CheckCollisions(targetPos))
                {
                    // PJ est entrain de faire un flip donc on ne peut en démarrer un autre
                    PJMoving = true;

                    // Pour le Flip back
                    cameraRotations.Add(camPivot.transform.localRotation.eulerAngles.y);
                    PJFlipDirections.Add(GetComponent<SwipeInput>().swipeDirection);
                    PJPositions.Add(PJ.transform.position);
                    PJRotations.Add(PJ.transform.rotation);

                    // Pivot
                    pJPivot.transform.position = PJ.transform.position + PJ.transform.localScale.x / 2 * centerEdgeDirection;
                    pJPivot.transform.rotation = initialRotation;

                    // Target de rotation pour le Slerp
                    Vector3 rotationAxis = Quaternion.AngleAxis(currentCamRot, Vector3.up) * Vector3.forward;
                    targetRotation = GetTargetQuaternion(initialRotation, Quaternion.AngleAxis(-angleRot, rotationAxis));

                    // Attacher PJ au pivot pour qu'il suive la rotation
                    PJ.transform.parent = pJPivot.transform;

                    // Si y'a un bloc piège avant mvt du PJ
                    GetComponent<BlocEvents>().EffacerBlocPiege(PJ.transform.position);

                    // Décrémenter le nbr de coups possibles au cas où PJ est painté
                    if (GetComponent<BlocEvents>().coupsAvecPaint - 1 >= 0) GetComponent<BlocEvents>().coupsAvecPaint--;

                    // On compte ce nouveau mvt du PJ
                    nbPJMoves++;
                }

                // Sortie de cette boucle pour ne faire qu'une seule rotation
                flipRight = false;
            }
            else if (flipUp)
            {
                // Déterminer vecteur direction du mouvement
                float currentCamRot = camPivot.transform.localRotation.eulerAngles.y;
                Vector3 centerEdgeDirection = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(0, -1, 1);

                // Target positions pour check les collisions
                Vector3 direction = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(0, 0, 1);
                Vector3 targetPos = PJ.transform.position + direction;

                // Check collisions pour autoriser le mouvement
                if (GetComponent<Collisions>().CheckCollisions(targetPos))
                {
                    // PJ est entrain de faire un flip donc on ne peut en démarrer un autre
                    PJMoving = true;

                    // Pour le Flip back
                    cameraRotations.Add(camPivot.transform.localRotation.eulerAngles.y);
                    PJFlipDirections.Add(GetComponent<SwipeInput>().swipeDirection);
                    PJPositions.Add(PJ.transform.position);
                    PJRotations.Add(PJ.transform.rotation);

                    // Pivot
                    pJPivot.transform.position = PJ.transform.position + PJ.transform.localScale.x / 2 * centerEdgeDirection;
                    pJPivot.transform.rotation = initialRotation;

                    // Target de rotation pour le Slerp
                    Vector3 rotationAxis = Quaternion.AngleAxis(currentCamRot, Vector3.up) * Vector3.right;
                    targetRotation = GetTargetQuaternion(initialRotation, Quaternion.AngleAxis(angleRot, rotationAxis));

                    // Attacher PJ au pivot pour qu'il suive la rotation
                    PJ.transform.parent = pJPivot.transform;

                    // Si y'a un bloc piège avant mvt du PJ
                    GetComponent<BlocEvents>().EffacerBlocPiege(PJ.transform.position);

                    // Décrémenter le nbr de coups possibles au cas où PJ est painté
                    if (GetComponent<BlocEvents>().coupsAvecPaint - 1 >= 0) GetComponent<BlocEvents>().coupsAvecPaint--;

                    // On compte ce nouveau mvt du PJ
                    nbPJMoves++;
                }
                
                // Sortie de cette boucle pour ne faire qu'une seule rotation
                flipUp = false;
            }
            else if (flipDown)
            {
                // Déterminer vecteur direction du mouvement
                float currentCamRot = camPivot.transform.localRotation.eulerAngles.y;
                Vector3 centerEdgeDirection = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(0, -1, -1);

                // Target positions pour check les collisions
                Vector3 direction = Quaternion.AngleAxis(currentCamRot, Vector3.up) * new Vector3(0, 0, -1);
                Vector3 targetPos = PJ.transform.position + direction;

                // Check collisions pour autoriser le mouvement
                if (GetComponent<Collisions>().CheckCollisions(targetPos))
                {
                    // PJ est entrain de faire un flip donc on ne peut en démarrer un autre
                    PJMoving = true;

                    // Pour le Flip back
                    cameraRotations.Add(camPivot.transform.localRotation.eulerAngles.y);
                    PJFlipDirections.Add(GetComponent<SwipeInput>().swipeDirection);
                    PJPositions.Add(PJ.transform.position);
                    PJRotations.Add(PJ.transform.rotation);

                    // Pivot
                    pJPivot.transform.position = PJ.transform.position + PJ.transform.localScale.x / 2 * centerEdgeDirection;
                    pJPivot.transform.rotation = initialRotation;

                    // Target de rotation pour le Slerp
                    Vector3 rotationAxis = Quaternion.AngleAxis(currentCamRot, Vector3.up) * Vector3.right;
                    targetRotation = GetTargetQuaternion(initialRotation, Quaternion.AngleAxis(-angleRot, rotationAxis));

                    // Attacher PJ au pivot pour qu'il suive la rotation
                    PJ.transform.parent = pJPivot.transform;

                    // Si y'a un bloc piège avant mvt du PJ
                    GetComponent<BlocEvents>().EffacerBlocPiege(PJ.transform.position);

                    // Décrémenter le nbr de coups possibles au cas où PJ est painté
                    if (GetComponent<BlocEvents>().coupsAvecPaint - 1 >= 0) GetComponent<BlocEvents>().coupsAvecPaint--;

                    // On compte ce nouveau mvt du PJ
                    nbPJMoves++;
                }


                // Sortie de cette boucle pour ne faire qu'une seule rotation
                flipDown = false;
            }

            // Mouvement rotation flip du pivot auquel est attaché le PJ
            pJPivot.transform.rotation = Quaternion.Slerp(pJPivot.transform.rotation, targetRotation, Time.deltaTime * flipSpeed);
            
            // Condition d'arrêt mouvement flip
            yield return new WaitUntil(() => pJPivot.transform.rotation == targetRotation);
            
            // Reset
            targetRotation = new Quaternion();
            PJ.transform.parent = null;
            // Ajustement
            PJ.transform.position = new Vector3(AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.y), AroundToInt(PJ.transform.position.z));
            initialPosition = PJ.transform.position;
            initialRotation = PJ.transform.rotation;
            PJMoving = false;

            // Autoriser qu'un seul appel à la fois pour ce qui se trouve dans le if ci-dessous
            if (countCalls == 0 && !GetComponent<UndoMove>().effectBack && !GetComponent<UndoMove>().PJUndoing && !GetComponent<UndoMove>().doFlipBack)
            {
                // Pour déclencher effet d'un bloc Color sur le PJ
                GetComponent<BlocEvents>().EventBlocColor();

                // Indicateur UI du nbr de coups restant après un paint
                // TODO attendre d'afficher 0 avant de masquer l'indicateur et aussi peut être attendre un effet de repose de material ?
                GetComponent<IndicateurPaint>().UpdateIndicateurCoupsPaint();
                // Une fois mvt du PJ terminé on vérifie que, s'il a été painté, et que, s'il a fait 10 coups, il retrouve son material initial
                GetComponent<BlocEvents>().GiveBackPJMaterial(PJ.transform.position);

                // Pour Undo
                GetComponent<BlocEvents>().positionsBlocAvantEffet.Add(new Vector3(AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.y - PJ.transform.localScale.y), AroundToInt(PJ.transform.position.z)));
                GetComponent<BlocEvents>().positionsBlocsCrees.Add(new List<Vector3>());
                GetComponent<BlocEvents>().positionsBlocDetruit.Add(new Vector3(-1, -1, -1));
                // Mise en mémoire du type du bloc
                string[] blocSousPJInfos = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)].Split('/');
                GetComponent<BlocEvents>().typesBloc.Add(blocSousPJInfos[0]);

                countCalls++;
                yield return new WaitForSeconds(0.1f);
                countCalls = 0;
            }
        }
    }
}