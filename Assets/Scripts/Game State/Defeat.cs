﻿using UnityEngine;
using System.Collections.Generic;

public class Defeat : MonoBehaviour {
    public GameObject gameOverUI; // Le empty game over qui contient l'UI
    public bool isDefeat = false;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        IsDefeat();
	}

    // Si condition de défaite est remplie, on affiche l'UI de Game Over
    public void IsDefeat()
    {
        // En mode Classik ou Hardkore ça ne change rien

        if (GetComponent<Countdown>().nbrMaxCoups != 0)
        {
            isDefeat = GetComponent<MovePlayer>().nbPJMoves > GetComponent<Countdown>().nbrMaxCoups; // Ici condition de défaite == nbr max de coups dépassé            
        }

        if (GetComponent<Chrono>().centiemes != 0)
        {
            isDefeat = GetComponent<Chrono>().timeInCentiSec < 0; // Ici condition de défaite == secondes écoulées
        }

        if (GetComponent<Countdown>().nbrMaxCoups != 0 && GetComponent<Chrono>().centiemes != 0)
        {
            // Ici condition de défaite == secondes écoulées || nbr max de coups dépassé
            if (GetComponent<Chrono>().timeInCentiSec < 0) isDefeat = true;
            if (GetComponent<MovePlayer>().nbPJMoves > GetComponent<Countdown>().nbrMaxCoups) isDefeat = true; 
        }

        if (isDefeat)
        {
            // Désactiver tout le reste de l'UI
            for (int i = 0; i < GetComponent<Victory>().UiADesactiver.Count; i++)
            {
                GetComponent<Victory>().UiADesactiver[i].SetActive(false);
            }

            gameOverUI.SetActive(isDefeat);
        }
    }
}
