﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Victory : MonoBehaviour {

    public GameObject scoreWindow; // Fenêtre de victoire

    public GameObject gameModeUi; // Sous la ligne du titre, on affiche le mode de jeu
    [Space(10)]
    public GameObject moveRecordUi; // En cas de meilleur score on affiche "New Record" sous le titre correspondant
    public GameObject timeRecordUi; // En cas de meilleur score on affiche "New Record" sous le titre correspondant
    [Space(10)]
    public GameObject movesStat; // Nbr de mvt
    public GameObject timeStat; // Temps écoulé depuis début du niveau
    public GameObject rewindsStat; // Nbr de undos

    [Space(10)]
    public List<GameObject> UiADesactiver; // Mettre toute l'UI que l'on veut désactiver pendant l'affichage de la fenêtre de victoire

    [Space(10)]
    public Material verrouilleMat;
    public Material deverrouilleMat;
    
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {

    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Si la condition de victoire est remplie, on passe au niveau suivant
    // Appelée qd PJ passe sur bloc verrou
    public void IsVictory()
    {
        if(!GetComponent<Defeat>().isDefeat && GetNbrBlocsVerrous() == 0) // Si Victoire
        {
            // On arrête l'éventuel Chrono si en mode Chrono
            GetComponent<Chrono>().shouldChronoFlow = false;

            // On arrête le temps qui s'écoule depuis le début du niveau
            GetComponent<Temps>().shouldTimeFlow = false;

            scoreWindow.SetActive(true);

            // Désactiver tout le reste de l'UI
            for (int i = 0; i < UiADesactiver.Count; i++)
            {
                UiADesactiver[i].SetActive(false);
            }

            // Afficher record, le nbr de coups, le game mode, le nbr de undos
            gameModeUi.GetComponentInChildren<Text>().text = GetComponent<GameMode>().DisplayGameMode();
            movesStat.GetComponentInChildren<Text>().text = GetComponent<MovePlayer>().nbPJMoves.ToString("000");
            timeStat.GetComponentInChildren<Text>().text = GetComponent<Temps>().DisplayTime(GetComponent<Temps>().tempsEcoule);
            rewindsStat.GetComponentInChildren<Text>().text = GetComponent<UndoMove>().countUndos.ToString("000");

            // TODO rajouter un nbrUndos au nbrCoups ? ou le rajouter juste dans le score ?

            PersistentScoreData scoreDataManager = GameObject.Find("SaveScoreDataManager").GetComponent<PersistentScoreData>();
            SelectedLevel levelInfo = GameObject.Find("LevelID").GetComponent<SelectedLevel>();

            // TODO calculer (et afficher ?) le score => calcul des médailles ?
            // TODO afficher la bonne médaille

            // Afficher "New Record" si meilleur nbr coups et/ou meilleur temps
            // Comparer temps et nbrCoups dans Sauvegarde du niveau
            // Si tempsSave > newTemps alors on affiche NewRecord (idem coups)
            if(levelInfo.gameMode == Modes.Classic)
            {
                if (scoreDataManager.levelChronoClassik[levelInfo.levelIndex] > GetComponent<Temps>().tempsEcoule)
                {
                    timeRecordUi.SetActive(true);
                }
                else timeRecordUi.SetActive(false);
                if (scoreDataManager.levelNbrCoupsClassik[levelInfo.levelIndex] > GetComponent<MovePlayer>().nbPJMoves)
                {
                    moveRecordUi.SetActive(true);
                }
                else moveRecordUi.SetActive(false);
            }
            else if (levelInfo.gameMode == Modes.Hardcore)
            {
                if (scoreDataManager.levelChronoHardkore[levelInfo.levelIndex] > GetComponent<Temps>().tempsEcoule)
                {
                    timeRecordUi.SetActive(true);
                }
                else timeRecordUi.SetActive(false);
                if (scoreDataManager.levelNbrCoupsHardkore[levelInfo.levelIndex] > GetComponent<MovePlayer>().nbPJMoves)
                {
                    moveRecordUi.SetActive(true);
                }
                else moveRecordUi.SetActive(false);
            }

            // Nouvelle Sauvegarde des scores
            GameObject.Find("SaveScoreDataManager").GetComponent<SaveScoreDataManager>().Save();

            // Le niveau accessible par Continuer doit donc être le niveau suivant
            PersistentLastLevelData lastLevelData = GameObject.Find("SaveLastLevelDataManager").GetComponent<PersistentLastLevelData>();
            if (lastLevelData.chiffrePhase2 + 1 > 10) // Passage à lettre suivante et donc chiffre devient 1
            {
                lastLevelData.lettrePhase1 = levelInfo.lettrePhase1 + 1;
                lastLevelData.chiffrePhase2 = 1;
            }
            else
            {
                lastLevelData.chiffrePhase2 = levelInfo.chiffrePhase2 + 1;
            }
            lastLevelData.levelIndex = (lastLevelData.lettrePhase1 - 1) * 10 + lastLevelData.chiffrePhase2 - 1;

            // Recharger les contraintes du niveau suivant
            GetComponent<LevelSuivant>().GetLevelConstraints();
            lastLevelData.centiemesForChrono = GameObject.Find("LevelID").GetComponent<SelectedLevel>().centiemesForChrono; // Chrono
            lastLevelData.maxCoupsForCountdown = GameObject.Find("LevelID").GetComponent<SelectedLevel>().maxCoupsForCountdown; // Countdown
            // Sauvegarde du dernier niveau joué
            GameObject.Find("SaveLastLevelDataManager").GetComponent<SaveLastLevelDataManager>().Save();
        }
    }
    
    // Donne le nbr de tous les blocs verrous du level
    public int GetNbrBlocsVerrous()
    {
        int res = 0;

        for (int i = 0; i < GameObject.Find("Level").transform.childCount; i++)
        {
            if (GameObject.Find("Level").transform.GetChild(i).name.Contains("End") && !GameObject.Find("Level").transform.GetChild(i).name.Contains("Deverouille"))
            {
                res ++;
            }
        }

        return res;
    }

    // Donne le nbr de tous les blocs verrous DEVEROUILLES du level
    public int GetNbrBlocsVerrousDeverouilles()
    {
        int res = 0;

        for (int i = 0; i < GameObject.Find("Level").transform.childCount; i++) // TODO trouver une vrai condition de victoire 
        {
            if (GameObject.Find("Level").transform.GetChild(i).name.Contains("Deverouille"))
            {
                res++;
            }
        }

        return res;        
    }
}
