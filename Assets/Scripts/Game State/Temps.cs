﻿using UnityEngine;
using System.Collections;

public class Temps : MonoBehaviour {

    [HideInInspector]
    public float tempsEcoule = 0; // Le temps qui s'est écoulé depuis le début du niveau (à remettre à 0 au Restart/Retry) CE TEMPS EST EN SECONDES
    [HideInInspector]
    public bool shouldTimeFlow = true;
                                  
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        EcouleTemps();
    }

    // Calcule le temps qui s'écoule selon une condition de jeu
    public void EcouleTemps()
    {
        if(shouldTimeFlow) tempsEcoule += Time.deltaTime * 100; // En cs
    }

    // Afficher un instant de temps au bon format
    public string DisplayTime(float time)
    {                
        int mn = Mathf.FloorToInt(time / 6000);
        int s = Mathf.FloorToInt((time / 6000 - mn) * 60);
        int cs = Mathf.FloorToInt(((time / 6000 - mn) * 60 - s) * 100);

        return string.Format("{0:00}'{1:00}\"{2:00}", mn, s, cs);
    }
}
