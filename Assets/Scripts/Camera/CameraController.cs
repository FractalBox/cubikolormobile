using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    private GameObject PJ;
    private GameObject camPivot;
    
    [Header("Rotation")]
    public float sensibilitySwipeCam = 10f;
    public float angleRotCam = 90f;
    
    private Vector3 initialPosition;
    [HideInInspector]
    public Quaternion initialRotation;
    private Quaternion targetRotation;

    [Space(10)]
    [Header("Zoom")]
    public float sensibilityZoomCam = 5f;
    public float ratioZoom = 1/2f;
    private float dummyFieldOfView;
    private float variationZoom = 0;
    private float fieldOfViewInit;

    [HideInInspector]
    public bool camIsSwiping = false;
    [HideInInspector]
    public bool camIsZooming = false;
    [HideInInspector]
    public bool swipeCamToLeft;
    [HideInInspector]
    public bool swipeCamToRight;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private void Start()
    {
        PJ = GameObject.Find("PJ");
        camPivot = GameObject.Find("CamPivot");

        initialPosition = new Vector3(PJ.transform.position.x, PJ.transform.localScale.y / 2, PJ.transform.position.z);
        camPivot.transform.position = initialPosition;
        initialRotation = camPivot.transform.rotation;

        fieldOfViewInit = Camera.main.fieldOfView;
    }

    private void Update()
    {
        // Pour Camera Follow PJ
        camPivot.transform.position = new Vector3(PJ.transform.position.x, PJ.transform.localScale.y / 2, PJ.transform.position.z); // TODO peut être faire en sorte que caméra suive aussi PJ dans ses translations selon Y 

        StartCoroutine("CamSwipe");
        StartCoroutine("ZoomCam");
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Arrondissement de valeur à taux près
    private float Around(float valeur, float taux)
    {
        return Mathf.Round(valeur * taux) / taux;
    }

    // Récupère la nouvelle target de rotation pour le pivot
    public Quaternion GetTargetQuaternion(Quaternion initial, Quaternion rotation)
    {
        return rotation * initial;
    }
    
    // Animation de swipe de la caméra de angleRot en angleRot
    public IEnumerator CamSwipe()
    {
        if (swipeCamToLeft)
        {
            // Caméra entrain de swipper donc on ne peut relancer une rotation
            camIsSwiping = true;

            // Target de rotation pour le Slerp
            targetRotation = GetTargetQuaternion(initialRotation, Quaternion.AngleAxis(-angleRotCam, Vector3.up));

            swipeCamToLeft = false;
        }
        else if (swipeCamToRight)
        {
            // Caméra entrain de swipper donc on ne peut relancer une rotation
            camIsSwiping = true;

            // Target de rotation pour le Slerp
            targetRotation = GetTargetQuaternion(initialRotation, Quaternion.AngleAxis(angleRotCam, Vector3.up));

            swipeCamToRight = false;
        }

        // Mouvement rotation caméra
        camPivot.transform.rotation = Quaternion.Slerp(camPivot.transform.rotation, targetRotation, Time.deltaTime * sensibilitySwipeCam);

        // Reset
        yield return new WaitUntil(() => camPivot.transform.rotation == targetRotation);
        targetRotation = new Quaternion();
        initialRotation = camPivot.transform.rotation;
        camIsSwiping = false;
    }

    // Animation zoom caméra selon ratioZoom
    public IEnumerator ZoomCam()
    {
        float marge = 0.005f;
        bool dummyChanged = false;

        if (GetComponent<DoubleClic>().hasDoubleClicked)
        {
            camIsZooming = true; 

            if (GetComponent<DoubleClic>().doubleClicCount % 2 != 0) // Zoom cam
            {
                dummyChanged = true;
                dummyFieldOfView = fieldOfViewInit * ratioZoom;
            }
            else // Dezoom cam
            {
                dummyChanged = true;
                dummyFieldOfView = fieldOfViewInit;
            }

            variationZoom = dummyFieldOfView - Camera.main.fieldOfView;
            Camera.main.fieldOfView += variationZoom * Time.deltaTime * sensibilityZoomCam;
        }

        yield return new WaitUntil(() => -marge <= variationZoom && variationZoom <= marge);
        if (dummyChanged) GetComponent<DoubleClic>().hasDoubleClicked = false;
        variationZoom = 0;
        camIsZooming = false;
    }   
}