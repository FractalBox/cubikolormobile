﻿using UnityEngine;
using System.Collections.Generic;

public class CreateTerrain : MonoBehaviour
{
    private GameObject PJ;
    [HideInInspector]
    public GameObject level;

    [Header("Fichier Levels")]
    public TextAsset levelsFile;
    public int indexLevel = 0;
    [Space(10)]
    private string[] levels; // Infos des niveaux séparés par "_" dans le fichier (150 levels en tout)
    private string[] infosLevelIndex; // Infos du Index-ième niveau séparées par "=" dans le fichier
    [HideInInspector]
    public string[] blocsLevelIndex; // Infos sur les blocs du Index-ième niveau séparées par ","  dans le fichier
    [HideInInspector]
    public int nbrColonnesLignes; // Longueur côté du terrain (en incuant les blocs voids)

    [Space(10)]
    [Header("Prefabs Blocs")]
    public GameObject cubeStart;
    public GameObject cubeNeutre;
    public GameObject cube1Color;
    public GameObject cube2Color;
    public GameObject cube4Color;
    public GameObject cubeTutorial;
    public GameObject cubePiege;
    public GameObject cubeMirror;
    public GameObject cubeEnd;

    [Space(10)]
    [Header("Colors & Materials")]
    public List<Color> colors; // Dans l'ordre : 0 = B, 1 = R, 2 = C, 3 = G, 4 = Y, 5 = P.

    [Space(10)]
    public List<Material> paintMaterials; // Même ordre de couleur

    [Space(10)]
    public string[,] levelMatrix; // Matrice des "C/C/C/C/H" des blocs du niveau
    public GameObject[,] blocsMatrix; // Matrice des blocs physiques du niveau

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void Awake()
    {
        PJ = GameObject.Find("PJ");
        level = new GameObject("Level");

        // Récupérer données du menu de sélection du niveau
        indexLevel = GameObject.Find("LevelID").GetComponent<SelectedLevel>().levelIndex;

        GetBlocsFromLevelText();

        nbrColonnesLignes = (int)Mathf.Sqrt(blocsLevelIndex.Length);
        levelMatrix = new string[nbrColonnesLignes, nbrColonnesLignes];
        blocsMatrix = new GameObject[nbrColonnesLignes, nbrColonnesLignes];

        LoadLevelFromLevelText();
    }

    public void Update()
    {

    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Arrondissement de valeur à taux près
    private int AroundToInt(float valeur)
    {
        return (int)(Mathf.Round(valeur));
    }

    // Print la matrice level
    public void PrintMatrix()
    {
        for(int i = 0; i < nbrColonnesLignes; i++)
        {
            for(int j =0; j< nbrColonnesLignes; j++)
            {
                print(levelMatrix[i, j] + " ");
            }
            print("\n");
        }
    }

    // Séparation du text dans levelsFile pour en dégager les informations sur les blocs du niveau 1 (Color/Color/Color/Color/Hauteur, ...)
    public void GetBlocsFromLevelText(/*levelIndex*/)
    {
        // Récuperer le text du fichier
        string levelsText = levelsFile.text;

        // Split pour séparer les levels
        levels = levelsText.Split('_');

        // Split pour dégager les infos du indexième niveau des blocs du indexième niveau
        infosLevelIndex = levels[indexLevel].Split('=');

        // Split pour séparer les blocs
        blocsLevelIndex = infosLevelIndex[1].Split(',');
    }

    // Creation du niveau 1 selon les infos récuperées
    public void LoadLevelFromLevelText()
    {
        // Position d'un bloc (le premier bloc est en (0,0)) sachant que son y est déterminé par le fichier
        int x = 0;
        int z = 0;
        
        // Split chaque string de infosBlocsLevelIndex pour connaître Colors et Height du bloc à instancier
        for (int i = 0; i < blocsLevelIndex.Length; i++)
        {
            // Positionner selon matrice nbrColonneLigne x nbrColonneLigne
            if (i != 0 && i % nbrColonnesLignes == 0)
            {
                z = 0;
                x++;
            }
            else z = i % nbrColonnesLignes;
            
            // Ajouter ce bloc dans la matrice des infos bloc du level
            levelMatrix[x, z] = blocsLevelIndex[i];

            // Bloc instancié
            GameObject bloc = null;

            string[] blocText = blocsLevelIndex[i].Split('/'); // Infos sur le bloc i du niveau 1
            int hauteur = int.Parse(blocText[4]); // Hauteur du bloc i
            int nbColor = 0; // Nbr de couleurs du bloc i
            List<Color> blocColors = new List<Color>(); // Couleur(s) du bloc i

            // Si le bloc est une couleur, il faut savoir combien il y en a et savoir laquelle/lesquelles :
            if (blocText[0] == "R" || blocText[0] == "C" || blocText[0] == "G" || blocText[0] == "Y" || blocText[0] == "P" || blocText[0] == "B")
            {
                // 4 fois la même lettre == 1 couleur
                if (blocText[0] == blocText[1] && blocText[1] == blocText[2] && blocText[2] == blocText[3])
                {
                    nbColor = 1;
                    blocColors.Add(StringToColor(blocText[0]));
                }
                // 2 lettres différente == 2 couleurs
                if (blocText[0] != blocText[1] && blocText[1] != blocText[2] && blocText[2] == blocText[3])
                {
                    nbColor = 2;
                    blocColors.Add(StringToColor(blocText[0]));
                    blocColors.Add(StringToColor(blocText[1]));
                }
                // 3 lettres diff == 3 couleurs
                //if (string.Compare(blocText[0], blocText[1]) + string.Compare(blocText[1], blocText[2]) + string.Compare(blocText[2], blocText[3]) == -2)
                //{
                //    nbColor = 3;
                //    blocColors.Add(StringToColor(blocText[0]));
                //    blocColors.Add(StringToColor(blocText[1]));
                //    blocColors.Add(StringToColor(blocText[2]));
                //}
                // 4 lettres diff == 4 couleurs
                if (blocText[0] != blocText[1] && blocText[1] != blocText[2] && blocText[2] != blocText[3])
                {
                    nbColor = 4;
                    blocColors.Add(StringToColor(blocText[0]));
                    blocColors.Add(StringToColor(blocText[1]));
                    blocColors.Add(StringToColor(blocText[2]));
                    blocColors.Add(StringToColor(blocText[3]));
                }

                if (nbColor == 1)
                {
                    bloc = (GameObject)Instantiate(cube1Color, new Vector3(x, hauteur, z), Quaternion.identity);
                    bloc.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                    bloc.name = "One Color " + i.ToString();
                    bloc.GetComponent<Renderer>().material.SetColor("_Color", blocColors[0]);
                    bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
                }
                else if (nbColor == 2)
                {
                    bloc = (GameObject)Instantiate(cube2Color, new Vector3(x, hauteur, z), Quaternion.identity);
                    bloc.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                    bloc.name = "Two Color " + i.ToString();
                    bloc.GetComponent<Renderer>().material.SetColor("_Color", blocColors[0]);
                    bloc.GetComponent<Renderer>().material.SetColor("_ColorTwo", blocColors[1]);
                    bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
                }
                else if (nbColor == 4)
                {
                    bloc = (GameObject)Instantiate(cube4Color, new Vector3(x, hauteur, z), Quaternion.identity);
                    bloc.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                    bloc.name = "Four Color " + i.ToString();
                    bloc.GetComponent<Renderer>().material.SetColor("_Color", blocColors[0]);
                    bloc.GetComponent<Renderer>().material.SetColor("_ColorTwo", blocColors[1]);
                    bloc.GetComponent<Renderer>().material.SetColor("_ColorThree", blocColors[2]);
                    bloc.GetComponent<Renderer>().material.SetColor("_ColorFour", blocColors[3]);
                    bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
                }
            }

            // Si bloc de départ alors il faut placer le PJ dessus et fixer cette position comme étant la position Initiale
            if (blocText[0] == "A")
            {
                bloc = (GameObject)Instantiate(cubeStart, new Vector3(x, hauteur, z), Quaternion.identity);
                bloc.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                bloc.name = "Start " + i.ToString();
                GetComponent<MovePlayer>().initialPJPosition = new Vector3(bloc.transform.position.x, bloc.transform.position.y + PJ.transform.localScale.y, bloc.transform.position.z); // Positionner PJ sur le cube start
                bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
            }

            // Si bloc de fin (1ère lettre == Z)
            // Il peut y en avoir plusieurs !
            // La deuxième lettre (si celle-ci n'est pas un Z) correspond à une aide pour donner l'ordre
            else if (blocText[0] == "Z")
            {
                bloc = (GameObject)Instantiate(cubeEnd, new Vector3(x, hauteur, z), Quaternion.identity);
                bloc.name = "End " + i.ToString();
                bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
            }

            // Si bloc Neutre
            else if (blocText[0] == "W")
            {
                bloc = (GameObject)Instantiate(cubeNeutre, new Vector3(x, hauteur, z), Quaternion.identity);
                bloc.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                bloc.name = "Neutre " + i.ToString();
                bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
            }

            // Si bloc Tutorial
            else if (blocText[0] == "I")
            {
                bloc = (GameObject)Instantiate(cubeTutorial, new Vector3(x, hauteur, z), Quaternion.identity);
                bloc.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                bloc.name = "Tutoriel " + i.ToString();
                bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level      
            }

            // Si bloc Ghost
            else if (blocText[0] == "T")
            {
                bloc = (GameObject)Instantiate(cubeNeutre, new Vector3(x, hauteur, z), Quaternion.identity);
                bloc.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                bloc.name = "Ghost " + i.ToString();
                bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
            }

            // Si bloc Piege
            else if (blocText[0] == "O")
            {
                bloc = (GameObject)Instantiate(cubePiege, new Vector3(x, hauteur, z), Quaternion.identity);
                bloc.name = "Piege " + i.ToString();
                bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
            }

            // Si bloc Mirror
            else if (blocText[0] == "M")
            {
                bloc = (GameObject)Instantiate(cubeMirror, new Vector3(x, hauteur, z), Quaternion.identity);
                bloc.name = "Mirror " + i.ToString();
                bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
            }

            // Si bloc Paint
            // Il faut récupérer la couleur du paint (K/Color/Color/Color/Hauteur)
            else if (blocText[0] == "K")
            {
                bloc = (GameObject)Instantiate(cubeMirror, new Vector3(x, hauteur, z), Quaternion.identity);
                bloc.name = "Paint " + i.ToString();
                bloc.GetComponent<Renderer>().material = StringToMaterial(blocText[1]);
                bloc.transform.parent = level.transform; // Mettre ce nouveau bloc en enfant du GameObject Level
            }

            // Pour une question visuelle :
            // Compléter par bloc Colonne jusqu'à la hauteur 0, seulement si hauteur est > 0
            // TODO implémenter une logique pour une esthétique de level (pcq pour l'instant c'est plat au niveau 0)
            // Sauf pour les bloc Piege
            if (hauteur > 0 && blocText[0] != "O")
            {
                for (int j = hauteur - 1; j >= 0; j--)
                {
                    GameObject blocColonne = (GameObject)Instantiate(cubeNeutre, new Vector3(x, j, z), Quaternion.identity);
                    blocColonne.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel                    
                    blocColonne.name = "Colonne " + j.ToString();
                    blocColonne.transform.parent = bloc.transform;
                }
            }

            // Ajouter le bloc instancié dans matrice des blocs physiques
            blocsMatrix[x, z] = bloc;
        }
    }

    // Passe de la lettre du fichier à une Color
    public Color StringToColor(string lettre)
    {
        if (lettre == "B")
        {
            return colors[0];
        }
        else if (lettre == "R")
        {
            return colors[1];
        }
        else if (lettre == "C")
        {
            return colors[2];
        }
        else if (lettre == "G")
        {
            return colors[3];
        }
        else if (lettre == "Y")
        {
            return colors[4];
        }
        else if (lettre == "P")
        {
            return colors[5];
        }
        else return Color.white;
    }

    // Passe de la lettre du fichier à un Material
    public Material StringToMaterial(string lettre)
    {
        if (lettre == "B")
        {
            return paintMaterials[0];
        }
        else if (lettre == "R")
        {
            return paintMaterials[1];
        }
        else if (lettre == "C")
        {
            return paintMaterials[2];
        }
        else if (lettre == "G")
        {
            return paintMaterials[3];
        }
        else if (lettre == "Y")
        {
            return paintMaterials[4];
        }
        else if (lettre == "P")
        {
            return paintMaterials[5];
        }
        else return null;
    }    
}