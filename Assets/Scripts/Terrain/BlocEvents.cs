﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BlocEvents : MonoBehaviour
{
    private GameObject PJ;
    private GameObject faceY;
    private GameObject faceC;
    private GameObject faceR;
    private GameObject faceG;
    private GameObject faceP;
    private GameObject faceB;

    // Bools d'évènements des blocs
    private bool monte;
    private bool descend;
    private bool unlock;
    private bool paint;
    [HideInInspector]
    public Color PJColorIfPainted = Color.white; // Blanc si PJ pas paint
    [HideInInspector]
    public int coupsAvecPaint;
    [HideInInspector]
    public bool mirror;
    private bool calledOnce = false;

    public float speedPJ = 8;
    public float minSpeedBlocs = 0.5f;
    public float maxSpeedBlocs = 20;
    [Space(10)]
    [Header("Materials for Verrou")]
    public Material blocVerouilleMat;
    public Material blocDeverouilleMat;

    [HideInInspector]
    public bool PJUnderTerrainEffect;

    private Vector3 targetPos;
    
    // Pour le Back(UndoMove)
    [HideInInspector]
    public List<Vector3> positionsBlocAvantEffet; // Retenir toutes les positions des blocs à l'entrée de PJ dessus
    [HideInInspector]
    public List<List<Vector3>> positionsBlocsCrees; // Il peut en y avoir plusieurs pour la descente donc liste de liste
    [HideInInspector]
    public List<Vector3> positionsBlocDetruit; // Un seul à la fois (si (-1, -1, -1) alors pas de blocs détruits à recréer)
    [HideInInspector]
    public List<string> typesBloc; // Retenir toutes les premières lettres des infos des blocs une fois PJ dessus

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Use this for initialization
    void Start()
    {
        PJ = GameObject.Find("PJ");

        faceY = PJ.transform.Find("faceYELLOW").gameObject;
        faceC = PJ.transform.Find("faceCYAN").gameObject;
        faceR = PJ.transform.Find("faceRED").gameObject;
        faceG = PJ.transform.Find("faceGREEN").gameObject;
        faceP = PJ.transform.Find("facePINK").gameObject;
        faceB = PJ.transform.Find("faceBLUE").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("EffetBlocColorOnPJ");
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Arrondissement de valeur à taux près
    private int AroundToInt(float valeur)
    {
        return (int)(Mathf.Round(valeur));
    }

    // Arrondissement de valeur au nombre de décimales données
    private double AroundToDecimales(float valeur, int nbDecimales)
    {
        return System.Math.Round(valeur, nbDecimales);
    }

    // Renvoie la face posée sur le sol du niveau
    // Comparer foreach face.y PJ avec centreFaceSupérieureBlocSousPJ.position.y     
    // A appeler que si PJ immobile
    public GameObject GetFaceSurSol()
    {
        string blocSousPJ = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)]; // Bloc sous le PJ
        string[] blocInfos = blocSousPJ.Split('/'); // Infos sur ce bloc

        Vector3 centreFaceSupBloc = new Vector3(AroundToInt(PJ.transform.position.x), int.Parse(blocInfos[4]) + (float)AroundToDecimales(PJ.transform.localScale.y / 2, 1), AroundToInt(PJ.transform.position.z)); // Attention ! Scale des blocs du terrain doit être la même que celle du PJ

        if (centreFaceSupBloc.y - AroundToDecimales(faceY.transform.position.y, 1) == 0) return faceY;
        else if (centreFaceSupBloc.y - AroundToDecimales(faceC.transform.position.y, 1) == 0) return faceC;
        else if (centreFaceSupBloc.y - AroundToDecimales(faceR.transform.position.y, 1) == 0) return faceR;
        else if (centreFaceSupBloc.y - AroundToDecimales(faceG.transform.position.y, 1) == 0) return faceG;
        else if (centreFaceSupBloc.y - AroundToDecimales(faceP.transform.position.y, 1) == 0) return faceP;
        else if (centreFaceSupBloc.y - AroundToDecimales(faceB.transform.position.y, 1) == 0) return faceB;
        else
        {
            print("Erreur : aucune face du PJ ne touche le sol !");
            return null;
        }
    }

    // Si bloc sous PJ est un bloc Color
    // Il faut regarder si 1, 2, 4 color(s)
    // Puis si face sur sol est de la même couleur ou non
    // Si oui, PJ monte
    // Sinon, PJ descend
    public void EventBlocColor()
    {
        string blocSousPJ = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)]; // Bloc sous le PJ
        string[] blocInfos = blocSousPJ.Split('/'); // Infos sur ce bloc
        List<Color> blocColors = new List<Color>(); // Couleur(s) du bloc i

        GameObject faceAuSol = GetFaceSurSol();

        // Si le bloc est une couleur, il faut savoir combien il y en a et savoir laquelle/lesquelles :
        if (blocInfos[0] == "R" || blocInfos[0] == "C" || blocInfos[0] == "G" || blocInfos[0] == "Y" || blocInfos[0] == "P" || blocInfos[0] == "B")
        {
            PJUnderTerrainEffect = true; // Pour empêcher d'autres mouvements pendant l'effet

            // 4 fois la même lettre == 1 couleur
            if (blocInfos[0] == blocInfos[1] && blocInfos[1] == blocInfos[2] && blocInfos[2] == blocInfos[3])
            {
                blocColors.Add(GetComponent<CreateTerrain>().StringToColor(blocInfos[0]));
            }
            // 2 lettres différente == 2 couleurs
            if (blocInfos[0] != blocInfos[1] && blocInfos[1] != blocInfos[2] && blocInfos[2] == blocInfos[3])
            {
                blocColors.Add(GetComponent<CreateTerrain>().StringToColor(blocInfos[0]));
                blocColors.Add(GetComponent<CreateTerrain>().StringToColor(blocInfos[1]));
            }
            // 4 lettres diff == 4 couleurs
            if (blocInfos[0] != blocInfos[1] && blocInfos[1] != blocInfos[2] && blocInfos[2] != blocInfos[3])
            {
                blocColors.Add(GetComponent<CreateTerrain>().StringToColor(blocInfos[0]));
                blocColors.Add(GetComponent<CreateTerrain>().StringToColor(blocInfos[1]));
                blocColors.Add(GetComponent<CreateTerrain>().StringToColor(blocInfos[2]));
                blocColors.Add(GetComponent<CreateTerrain>().StringToColor(blocInfos[3]));
            }

            if (PJColorIfPainted == Color.white) // PJ non painted
            {
                if (blocColors.Contains(GetFaceColor(faceAuSol)))
                {
                    monte = true;
                }
                else descend = true;
            }
            else // PJ painted
            {
                if (blocColors.Contains(PJColorIfPainted))
                {
                    monte = true;
                }
                else descend = true;
            }
            
        }

        // Si Bloc Verrou
        else if (blocInfos[0] == "Z") 
        {            
            unlock = true;
        }

        // Si Bloc PaintPJ
        else if(blocInfos[0] == "K")
        {
            paint = true;
        }

        // Si Bloc Mirror
        else if(blocInfos[0] == "M")
        {
            mirror = true;
            calledOnce = mirror;
        }
    }

    // Renvoie la couleur de la face donnée en argument
    public Color GetFaceColor(GameObject face)
    {
        if (face.name == "faceBLUE") return GetComponent<CreateTerrain>().colors[0];
        else if (face.name == "faceRED") return GetComponent<CreateTerrain>().colors[1];
        else if (face.name == "faceCYAN") return GetComponent<CreateTerrain>().colors[2];
        else if (face.name == "faceGREEN") return GetComponent<CreateTerrain>().colors[3];
        else if (face.name == "faceYELLOW") return GetComponent<CreateTerrain>().colors[4];
        else if (face.name == "facePINK") return GetComponent<CreateTerrain>().colors[5];
        else
        {
            print("Erreur : pas de couleur sur cette face !");
            return Color.white;
        }
    }

    // Monte le PJ et cube sous PJ
    // Créer cube Colonne à y == 0
    // OU
    // Descend PJ et cube sous PJ
    // Détruire le cube colonne à y == 0 s'il existe
    public IEnumerator EffetBlocColorOnPJ()
    {
        if (PJUnderTerrainEffect)
        {
            // Différentiation
            bool PJMonte = false;
            bool PJDescend = false;

            GameObject blocSousPJ = null;
            string[] infosBlocSousPJ = new string[4]; // Pour mettre à jour ses infos à la suite de la montée
            GameObject blocColonne = null; // Déclaré ici pcq j'en ai besoin en dehors de la boucle où il est instancié

            blocSousPJ = GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)];
            infosBlocSousPJ = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)].Split('/');

            // MONTE
            if (monte)
            {
                PJMonte = true;

                targetPos = new Vector3(AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.y + PJ.transform.localScale.y), AroundToInt(PJ.transform.position.z));
                blocSousPJ.transform.parent = PJ.transform; // On met en enfant du PJ le bloc physqiue sous PJ pour le bouger avec le PJ

                // Créer un nouveau bloc "Colonne" à hauteurTarget pour combler le trou sous ce bloc
                float hauteurTarget = 0;
                // Si bloc couleur a des sous-blocs, il faut créer ce bloc à hauteur du plus bas
                if (blocSousPJ.transform.childCount != 0)
                {
                    for (int i = 0; i < blocSousPJ.transform.childCount; i++)
                    {
                        if (blocSousPJ.transform.GetChild(i).transform.position.y < hauteurTarget) hauteurTarget = blocSousPJ.transform.GetChild(i).transform.position.y; // Récupérer le plus bas
                    }
                }
                // Sinon, il faut le créer à hauteur du bloc en lui-même
                else hauteurTarget = blocSousPJ.transform.position.y;
                // Instanciation
                blocColonne = (GameObject)Instantiate(GetComponent<CreateTerrain>().cubeNeutre, new Vector3(AroundToInt(blocSousPJ.transform.position.x), hauteurTarget, AroundToInt(blocSousPJ.transform.position.z)), Quaternion.identity); // TODO vérifier que le 0 marche vraiment dans tous les cas de figure
                blocColonne.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                blocColonne.name = "Colonne Supp";

                // Pour le Undo (remplacement)
                positionsBlocsCrees[positionsBlocsCrees.Count - 1].Add(blocColonne.transform.position);

                // Sortie de la boucle
                monte = false;
            }

            // DESCEND
            if (descend)
            {
                PJDescend = true;

                targetPos = new Vector3(AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.y - PJ.transform.localScale.y), AroundToInt(PJ.transform.position.z));
                blocSousPJ.transform.parent = PJ.transform; // On met en enfant du PJ le bloc physqiue sous PJ pour le bouger avec le PJ

                // S'il y au moins un bloc colonne sous le blocSousPJ alors on supprime le bloc en y le plus bas et on descend PJ et blocSouPJ
                if (blocSousPJ.transform.childCount != 0)
                {
                    // Détruire bloc "Colonne" sous ce bloc pour laisser le trou nécessaire
                    GameObject blocADetruire = blocSousPJ;
                    for (int j = 0; j < blocSousPJ.transform.childCount; j++)
                    {
                        if (blocSousPJ.transform.GetChild(j).position.y <= blocADetruire.transform.position.y) blocADetruire = blocSousPJ.transform.GetChild(j).gameObject; // Récuperer le bloc de y le plus bas
                    }

                    // Pour le Undo (remplacement)
                    positionsBlocDetruit[positionsBlocDetruit.Count - 1] = new Vector3(AroundToInt(blocADetruire.transform.position.x), AroundToInt(blocADetruire.transform.position.y), AroundToInt(blocADetruire.transform.position.z));
                    Destroy(blocADetruire);
                }

                // Sinon il faut créer une plage de bloc colonne à hauteur -1 sous tous les blocs de niveau 0 pour que PJ et blocSousPJ puissent descendre (blocSousPJ sera donc de hauteur -1)
                else
                {
                    for (int i = 0; i < GameObject.Find("Level").transform.childCount; i++) // Tous les blocs du level
                    {
                        GameObject blocI = GameObject.Find("Level").transform.GetChild(i).gameObject; // Le bloc i

                        // Pour blocs qui ont des blocs Colonnes, il faut créer un bloc sous leur bloc "Colonne 0/Supp/Comp" le plus bas
                        if (blocI.transform.childCount != 0)
                        {
                            // Bloc juste au-dessus du nouveau à mettre 
                            GameObject blocAuDessus = blocI;
                            for (int j = 0; j < blocI.transform.childCount; j++)
                            {
                                if (blocI.transform.GetChild(j).position.y <= blocAuDessus.transform.position.y) blocAuDessus = blocI.transform.GetChild(j).gameObject; // Récuperer le bloc le plus bas
                            }

                            GameObject blocSousColonne0 = (GameObject)Instantiate(GetComponent<CreateTerrain>().cubeNeutre, new Vector3(AroundToInt(blocAuDessus.transform.position.x), AroundToInt(blocAuDessus.transform.position.y) - 1, blocAuDessus.transform.position.z), Quaternion.identity); // TODO vérifier que le -1 marche vraiment dans tous les cas de figure
                            blocSousColonne0.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                            blocSousColonne0.name = "Colonne Compense";
                            blocSousColonne0.transform.parent = blocI.transform;
                            blocSousColonne0.transform.position = new Vector3(AroundToInt(blocSousColonne0.transform.position.x), AroundToInt(blocSousColonne0.transform.position.y), AroundToInt(blocSousColonne0.transform.position.z));

                            // Pour le Undo (remplacement)
                            positionsBlocsCrees[positionsBlocsCrees.Count - 1].Add(blocSousColonne0.transform.position);
                        }
                        else if (blocI.transform.position.y == 0) // Pour blocs à hauteur 0
                        {
                            GameObject blocColonneComp = (GameObject)Instantiate(GetComponent<CreateTerrain>().cubeNeutre, new Vector3(AroundToInt(blocI.transform.position.x), -1, blocI.transform.position.z), Quaternion.identity); // TODO vérifier que le -1 marche vraiment dans tous les cas de figure
                            blocColonneComp.transform.rotation *= Quaternion.AngleAxis(-90, Vector3.right);  // Pour cacher la transparence de la face du prefab dû au matériel
                            blocColonneComp.name = "Colonne Compense";
                            blocColonneComp.transform.parent = blocI.transform;
                            blocColonneComp.transform.position = new Vector3(AroundToInt(blocColonneComp.transform.position.x), AroundToInt(blocColonneComp.transform.position.y), AroundToInt(blocColonneComp.transform.position.z));

                            // Pour le Undo (remplacement)
                            positionsBlocsCrees[positionsBlocsCrees.Count - 1].Add(blocColonneComp.transform.position);
                        }
                    }
                }

                // Sortie de la boucle
                descend = false;
            }


            // MOUVEMENT
            PJ.transform.position = Vector3.Lerp(PJ.transform.position, targetPos, speedPJ * Time.deltaTime);

            // Condition d'arrêt du mouvement
            yield return new WaitUntil(() => PJ.transform.position == targetPos);

            if (PJMonte)
            {
                // Remettre bloc dans "Level" avec bonne hierarchie (bloc colonne enfant des blocs du level)
                if (blocSousPJ != null)
                {
                    // Mettre à jour la matrice des infos sur les blocs du terrain
                    GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)] = infosBlocSousPJ[0] + "/" + infosBlocSousPJ[1] + "/" + infosBlocSousPJ[2] + "/" + infosBlocSousPJ[3] + "/" + (int.Parse(infosBlocSousPJ[4]) + 1).ToString();
                    // Hierarchie
                    if (blocColonne != null)
                    {
                        blocColonne.transform.parent = blocSousPJ.transform;
                        blocColonne.transform.position = new Vector3(AroundToInt(blocColonne.transform.position.x), AroundToInt(blocColonne.transform.position.y), AroundToInt(blocColonne.transform.position.z));
                    }
                    blocSousPJ.transform.parent = GameObject.Find("Level").transform;
                    // Ajustement
                    blocSousPJ.transform.position = new Vector3(AroundToInt(blocSousPJ.transform.position.x), AroundToInt(blocSousPJ.transform.position.y), AroundToInt(blocSousPJ.transform.position.z));
                }

                PJMonte = false;
            }

            else if (PJDescend)
            {
                // Remettre bloc dans "Level" avec bonne hierarchie (bloc colonne enfant des blocs du level)
                if (blocSousPJ != null)
                {
                    // Mettre à jour la matrice des infos sur les blocs du terrain
                    GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)] = infosBlocSousPJ[0] + "/" + infosBlocSousPJ[1] + "/" + infosBlocSousPJ[2] + "/" + infosBlocSousPJ[3] + "/" + (int.Parse(infosBlocSousPJ[4]) - 1).ToString();
                    // Hierarchie
                    blocSousPJ.transform.parent = GameObject.Find("Level").transform;
                    // Ajustement
                    blocSousPJ.transform.position = new Vector3(AroundToInt(blocSousPJ.transform.position.x), AroundToInt(blocSousPJ.transform.position.y), AroundToInt(blocSousPJ.transform.position.z));
                }

                PJDescend = false;
            }
            
            // Pour vérifier le contenu de positionsBlocsCrees
            //for (int k = 0; k < positionsBlocsCrees.Count; k++)
            //{
            //    for (int l = 0; l < positionsBlocsCrees[k].Count; l++)
            //    {
            //        print(k + ", " + l + " : " + positionsBlocsCrees[k][l]);
            //    }
            //}

            // Remise à zero
            PJUnderTerrainEffect = false;
        }

        if (unlock)
        {
            // Bloc verrou
            GameObject blocVerrou = GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)];

            // Changement de matériel
            blocVerrou.GetComponent<Renderer>().material = blocDeverouilleMat;
            blocVerrou.name = " Deverouille"; // Pour ne plus le prendre en compte comme un bloc End verrouillé
            GetComponent<Victory>().IsVictory();

            // Sortie
            unlock = false;
        }

        if (paint)
        {
            // Bloc PaintPJ
            string[] infoBlocPaint = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)].Split('/');

            PJ.GetComponent<Renderer>().material = GetComponent<CreateTerrain>().StringToMaterial(infoBlocPaint[1]);

            // Indiquer que PJ est de color celle du material du paint pour les 10 prochains coups à venir 
            PJColorIfPainted = GetComponent<CreateTerrain>().StringToColor(infoBlocPaint[1]);

            // Nbr de coup limité avec le paint
            coupsAvecPaint = 10;
            // Début Affichage de l'indicateur
            GetComponent<IndicateurPaint>().UpdateIndicateurCoupsPaint();
            
            // Sortie
            paint = false;
        }

        if (mirror)
        {
            // On met PJ en enfant du bloc mirror pour qu'il suive la transformation du niveau
            GameObject blocMirror = GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)];
            PJ.transform.parent = blocMirror.transform;

            // On récupère le bloc qui est le plus éloigné de l'axe Y
            GameObject blocMostFar = blocMirror; // Arbitrairement le bloc mirror

            for (int i = 0; i < GetComponent<CreateTerrain>().nbrColonnesLignes; i++)
            {
                for (int j = 0; j < GetComponent<CreateTerrain>().nbrColonnesLignes; j++)
                {
                    GameObject bloc = GetComponent<CreateTerrain>().blocsMatrix[i, j];

                    if (bloc != null)
                    {
                        // Déterminer quel est le plus loin de Y
                        if (Mathf.Abs(AroundToInt(bloc.transform.position.y)) > Mathf.Abs(AroundToInt(blocMostFar.transform.position.y))) blocMostFar = bloc;
                    }
                }
            }

            // Mouvements des blocs SAUF le plus loin de Y
            for (int i = 0; i < GetComponent<CreateTerrain>().nbrColonnesLignes; i++)
            {
                for (int j = 0; j < GetComponent<CreateTerrain>().nbrColonnesLignes; j++)
                {
                    GameObject bloc = GetComponent<CreateTerrain>().blocsMatrix[i, j];

                    if (bloc != null)
                    {
                        if (bloc != blocMostFar) StartCoroutine(Shockwave(bloc));
                    }
                }
            }

            // Pour que tous les blocs est le temps de bouger, on fait bouger le plus loin à part en dernier
            string[] infosBlocMostFar = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(blocMostFar.transform.position.x), AroundToInt(blocMostFar.transform.position.z)].Split('/');
            Vector3 targetMostFarPos = new Vector3(AroundToInt(blocMostFar.transform.position.x), -int.Parse(infosBlocMostFar[4]), AroundToInt(blocMostFar.transform.position.z));
            if (blocMostFar.transform.position != targetMostFarPos) blocMostFar.transform.position = Vector3.Lerp(blocMostFar.transform.position, targetMostFarPos, Time.deltaTime * maxSpeedBlocs);
            yield return new WaitUntil(() => blocMostFar.transform.position == targetMostFarPos);

            // Sortie de boucle pour le faire qu'une fois
            mirror = false;

            if (blocMostFar.transform.position == targetMostFarPos && calledOnce)
            {
                // On arrête les coroutines pour empêcher tout autre mvt non voulu
                for (int i = 0; i < GetComponent<CreateTerrain>().nbrColonnesLignes; i++)
                {
                    for (int j = 0; j < GetComponent<CreateTerrain>().nbrColonnesLignes; j++)
                    {
                        GameObject bloc = GetComponent<CreateTerrain>().blocsMatrix[i, j];
                        string[] infosBloc = GetComponent<CreateTerrain>().levelMatrix[i, j].Split('/');

                        if (bloc != null)
                        {
                            //StopCoroutine(Shockwave(bloc));
                            // Ajustements de la position de chaque bloc 
                            GetComponent<CreateTerrain>().blocsMatrix[i, j].transform.position = new Vector3(AroundToInt(bloc.transform.position.x), -int.Parse(infosBloc[4])/*AroundToInt(bloc.transform.position.y)*/, AroundToInt(bloc.transform.position.z));
                            // Mettre à jour la matrice des infos sur les blocs du terrain
                            GetComponent<CreateTerrain>().levelMatrix[i, j] = infosBloc[0] + "/" + infosBloc[1] + "/" + infosBloc[2] + "/" + infosBloc[3] + "/" + (-int.Parse(infosBloc[4])).ToString();
                        }
                    }
                }

                calledOnce = false;

                // On rétablit la hiérarchie et ajuste la position du PJ
                PJ.transform.parent = null;
                PJ.transform.position = new Vector3(AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.y), AroundToInt(PJ.transform.position.z)); // TODO Dans le cas où bloc Mirror sous PJ n'est pas à y == 0, ajuster le mouvement du PJ pcq'il y a comme un accrochage          
            }            
        }
    }

    // Faire disparaître le bloc piège au moment où PJ sort du bloc
    // A appeler au moment où PJ débute son mouvement et s'il est sur un bloc piège avant de démarrer son mvt
    public void EffacerBlocPiege(Vector3 posPJAvant)
    {
        string blocAvant = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(posPJAvant.x), AroundToInt(posPJAvant.z)];
        string[] blocInfos = blocAvant.Split('/'); // Infos sur ce bloc

        if (blocInfos[0] == "O")
        {
            // Attendre que PJ quitte le bloc avant de détruire bloc piège
            if (!GetComponent<UndoMove>().PJUndoing)
            {
                // Bloc piege
                GameObject blocPiege = GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)];
                // MàJ des matrices du niveau pour effacer le bloc piege du level
                GetComponent<CreateTerrain>().levelMatrix[AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.z)] = "N/N/N/N/0";
                Destroy(blocPiege);
            }
        }
    }

    // Reattribuer le matériel du PJ normal dans le cas où PJ a été painté et qu'il a fait plus de 10 coups sans repasser par un bloc paint
    // A appeler qd PJ arrive sur le bloc après son 10ème coup après le paint
    public void GiveBackPJMaterial(Vector3 posPJ)
    {
        string blocSousPJ = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(posPJ.x), AroundToInt(posPJ.z)]; // Bloc sous le PJ
        string[] blocInfos = blocSousPJ.Split('/'); // Infos sur ce bloc
        
        if (PJColorIfPainted != Color.white)
        {
            if (blocInfos[0] != "K")
            {
                if (coupsAvecPaint == 0)
                {
                    PJ.GetComponent<Renderer>().material = GetComponent<MovePlayer>().originalPjMaterial;
                    PJColorIfPainted = Color.white;
                }
            }
        }
    }

    // Mouvement de shockwave à déclencher si PJ passe sur bloc mirror
    // Inverse le signe de la hauteur de tous les blocs du level un à un
    public IEnumerator Shockwave(GameObject bloc)
    {
        string[] infosBloc = GetComponent<CreateTerrain>().levelMatrix[AroundToInt(bloc.transform.position.x), AroundToInt(bloc.transform.position.z)].Split('/');
        Vector3 targetPos = new Vector3(AroundToInt(bloc.transform.position.x), -int.Parse(infosBloc[4]), AroundToInt(bloc.transform.position.z));

        bloc.transform.position = Vector3.Lerp(bloc.transform.position, targetPos, Time.deltaTime * Random.Range(minSpeedBlocs, maxSpeedBlocs));
        yield return new WaitUntil(() => bloc.transform.position == targetPos);

        // Ajustements
        bloc.transform.position = new Vector3(AroundToInt(bloc.transform.position.x), AroundToInt(bloc.transform.position.y), AroundToInt(bloc.transform.position.z));
        // Màj dans matrice level
        GetComponent<CreateTerrain>().blocsMatrix[AroundToInt(bloc.transform.position.x), AroundToInt(bloc.transform.position.z)] = bloc;
        GetComponent<CreateTerrain>().levelMatrix[AroundToInt(bloc.transform.position.x), AroundToInt(bloc.transform.position.z)] = infosBloc[0] + "/" + infosBloc[1] + "/" + infosBloc[2] + "/" + infosBloc[3] + "/" + (-int.Parse(infosBloc[4])).ToString();
    }
}
