﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Restart : MonoBehaviour {

    private GameObject PJ;
    private GameObject pJPivot;
    private GameObject camPivot;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Use this for initialization
    void Start () {
        PJ = GameObject.Find("PJ");
        pJPivot = GameObject.Find("PJPivot");
        camPivot = GameObject.Find("CamPivot");
    }
	
	// Update is called once per frame
	void Update () {

	}

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Arrondissement de valeur à taux près
    private int AroundToInt(float valeur)
    {
        return (int)(Mathf.Round(valeur));
    }

    // A appeler sur click sur le bouton Restart pour recommencer le niveau
    public void OnClicRestart()
    {
        if (!GetComponent<MovePlayer>().PJMoving && !GetComponent<CameraController>().camIsSwiping && !GetComponent<CameraController>().camIsZooming && !GetComponent<BlocEvents>().PJUnderTerrainEffect && !GetComponent<UndoMove>().PJUndoing)
        {
            StopAllCoroutines();

            // Détuire et Recharger le Level (cela rechargera aussi la position initiale du PJ)
            Destroy(GameObject.Find("Level"));
            GetComponent<CreateTerrain>().level = new GameObject("Level");

            GetComponent<CreateTerrain>().GetBlocsFromLevelText();

            GetComponent<CreateTerrain>().nbrColonnesLignes = (int)Mathf.Sqrt(GetComponent<CreateTerrain>().blocsLevelIndex.Length);
            GetComponent<CreateTerrain>().levelMatrix = new string[GetComponent<CreateTerrain>().nbrColonnesLignes, GetComponent<CreateTerrain>().nbrColonnesLignes];
            GetComponent<CreateTerrain>().blocsMatrix = new GameObject[GetComponent<CreateTerrain>().nbrColonnesLignes, GetComponent<CreateTerrain>().nbrColonnesLignes];

            GetComponent<CreateTerrain>().LoadLevelFromLevelText();

            // Remettre qui s'écoule à zéro
            GetComponent<Temps>().tempsEcoule = 0;
            GetComponent<Temps>().shouldTimeFlow = true;

            // Remise à zero position PJ (et son pivot) et caméra
            PJ.transform.position = GetComponent<MovePlayer>().initialPJPosition;
            PJ.transform.rotation = Quaternion.identity;
            pJPivot.transform.position = GetComponent<MovePlayer>().initialPosition;
            pJPivot.transform.rotation = GetComponent<MovePlayer>().initialRotation;
            camPivot.transform.rotation = Quaternion.identity;
            GetComponent<CameraController>().initialRotation = camPivot.transform.rotation;
            // Remise à zero du material du PJ
            GetComponent<BlocEvents>().coupsAvecPaint = 0;
            GetComponent<IndicateurPaint>().UpdateIndicateurCoupsPaint();
            GetComponent<BlocEvents>().GiveBackPJMaterial(PJ.transform.position);

            // Vider la mémoire de Positions/Rotations etc...
            GetComponent<MovePlayer>().nbPJMoves = 0;
            GetComponent<UndoMove>().countUndos = 0;
            GetComponent<MovePlayer>().PJPositions = new List<Vector3>();
            GetComponent<MovePlayer>().PJRotations = new List<Quaternion>();

            // Vider mémoire blocs
            GetComponent<BlocEvents>().positionsBlocAvantEffet = new List<Vector3>();
            GetComponent<BlocEvents>().positionsBlocsCrees = new List<List<Vector3>>();
            GetComponent<BlocEvents>().positionsBlocDetruit = new List<Vector3>();
            GetComponent<BlocEvents>().typesBloc = new List<string>();

            // Pour enregistrer les données du bloc Start pour le Undo
            GetComponent<BlocEvents>().positionsBlocAvantEffet.Add(new Vector3(AroundToInt(PJ.transform.position.x), AroundToInt(PJ.transform.position.y - PJ.transform.localScale.y), AroundToInt(PJ.transform.position.z)));
            GetComponent<BlocEvents>().positionsBlocsCrees.Add(new List<Vector3>());
            GetComponent<BlocEvents>().positionsBlocDetruit.Add(new Vector3(-1, -1, -1));
            GetComponent<BlocEvents>().typesBloc.Add("A"); // Ca sera toujours bloc Start en premier donc A

            // Remise à zero des contraintes
            // nbPjMoves déjà remis à zero
            GetComponent<Chrono>().timeInCentiSec = GetComponent<Chrono>().centiemes;
            GetComponent<Chrono>().shouldChronoFlow = true;

            // Réactiver l'UI éventuellement désactivée en cas de Victoire + désactiver l'UI de victoire
            for(int i = 0; i < GetComponent<Victory>().UiADesactiver.Count; i++)
            {
                GetComponent<Victory>().UiADesactiver[i].SetActive(true);
            }
            GetComponent<Victory>().scoreWindow.SetActive(false);
            GetComponent<Defeat>().gameOverUI.SetActive(false);
        }
    }

    // VERSION IPHONE
    // J'ai mis un EventTrigger avec OnPointerDown qui appelle OnClicRestart()
    //public void TapRetry()
    //{
    //    int nbTouches = Input.touchCount;

    //    if (nbTouches > 0) // Si au moins un touch
    //    {
    //        for (int i = 0; i < nbTouches; i++)
    //        {
    //            Touch touch = Input.GetTouch(i); // Pour le touch i

    //            if (GameObject.Find("UIHandler").GetComponent<UIHandler>().IsPointerOverUIObject() /*EventSystem.current.IsPointerOverGameObject(touch.fingerId)*/) // Si touch i est sur de l'UI
    //            {
    //                if (touch.phase == TouchPhase.Began) // Doigt posé sur écran
    //                {
    //                    Ray ray = Camera.main.ScreenPointToRay(touch.position);
    //                    RaycastHit sphereHitInfo;
    //                    if (Physics.Raycast(ray, out sphereHitInfo))
    //                    {
    //                        // Pour les lettres(zones)
    //                        if (sphereHitInfo.collider.gameObject.name.Contains("Retry"))
    //                        {
    //                            OnClicRestart();
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}
}
