// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: commented out 'float4 unity_DynamicLightmapST', a built-in variable
// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable

// Shader created with Shader Forge v1.03 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.03;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,dith:2,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:1,fgcg:1,fgcb:1,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33607,y:32745,varname:node_1,prsc:2|emission-7-OUT;n:type:ShaderForge.SFN_ScreenPos,id:3,x:32368,y:32800,varname:node_3,prsc:2,sctp:0;n:type:ShaderForge.SFN_Slider,id:5,x:32315,y:32977,ptovrint:False,ptlb:limitY,ptin:_limitY,varname:node_633,prsc:2,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Clamp01,id:6,x:32892,y:32840,varname:node_6,prsc:2|IN-1991-OUT;n:type:ShaderForge.SFN_Lerp,id:7,x:33115,y:32848,varname:node_7,prsc:2|A-8-RGB,B-9-RGB,T-6-OUT;n:type:ShaderForge.SFN_Color,id:8,x:33017,y:32564,ptovrint:False,ptlb:colorUp,ptin:_Color1,varname:node_8220,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:9,x:32793,y:32624,ptovrint:False,ptlb:colorDown,ptin:_Color2,varname:node_4864,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:10,x:32542,y:32800,varname:node_10,prsc:2|A-3-V,B-5-OUT;n:type:ShaderForge.SFN_ViewVector,id:558,x:32526,y:33205,varname:node_558,prsc:2;n:type:ShaderForge.SFN_ComponentMask,id:5404,x:32699,y:33195,varname:node_5404,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-558-OUT;n:type:ShaderForge.SFN_Clamp01,id:6917,x:33096,y:33217,varname:node_6917,prsc:2|IN-2843-OUT;n:type:ShaderForge.SFN_Add,id:2843,x:32913,y:33195,varname:node_2843,prsc:2|A-5404-OUT,B-5-OUT;n:type:ShaderForge.SFN_Slider,id:1303,x:32739,y:33054,ptovrint:False,ptlb:Blend,ptin:_Blend,varname:node_1303,prsc:2,min:1,cur:1,max:3;n:type:ShaderForge.SFN_Divide,id:1991,x:32734,y:32808,varname:node_1991,prsc:2|A-10-OUT,B-1303-OUT;proporder:5-8-9-1303;pass:END;sub:END;*/

Shader "Cubikolor/skyboxGradientVertical_shader" {
    Properties {
        _limitY ("limitY", Range(-1, 1)) = 0
        _Color1 ("colorUp", Color) = (0.5,0.5,0.5,1)
        _Color2 ("colorDown", Color) = (0.5,0.5,0.5,1)
        _Blend ("Blend", Range(1, 3)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 flash d3d11_9x 
            #pragma target 3.0
            // Dithering function, to use with scene UVs (screen pixel coords)
            // 3x3 Bayer matrix, based on https://en.wikipedia.org/wiki/Ordered_dithering
            float BinaryDither3x3( float value, float2 sceneUVs ) {
                float3x3 mtx = float3x3(
                    float3( 3,  7,  4 )/10.0,
                    float3( 6,  1,  9 )/10.0,
                    float3( 2,  8,  5 )/10.0
                );
                float2 px = floor(_ScreenParams.xy * sceneUVs);
                int xSmp = fmod(px.x,3);
                int ySmp = fmod(px.y,3);
                float3 xVec = 1-saturate(abs(float3(0,1,2) - xSmp));
                float3 yVec = 1-saturate(abs(float3(0,1,2) - ySmp));
                float3 pxMult = float3( dot(mtx[0],yVec), dot(mtx[1],yVec), dot(mtx[2],yVec) );
                return round(value + dot(pxMult, xVec));
            }
            // float4 unity_LightmapST;
            #ifdef DYNAMICLIGHTMAP_ON
                // float4 unity_DynamicLightmapST;
            #endif
            uniform float _limitY;
            uniform float4 _Color1;
            uniform float4 _Color2;
            uniform float _Blend;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 screenPos : TEXCOORD0;
                #ifndef LIGHTMAP_OFF
                    float4 uvLM : TEXCOORD1;
                #else
                    float3 shLight : TEXCOORD1;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.screenPos = o.pos;
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
/////// Vectors:
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_Color1.rgb,_Color2.rgb,saturate(((i.screenPos.g+_limitY)/_Blend)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
