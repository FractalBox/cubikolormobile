// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: commented out 'float4 unity_DynamicLightmapST', a built-in variable
// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable

// Shader created with Shader Forge v1.03 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.03;sub:START;pass:START;ps:flbk:Mobile/Diffuse,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:False,lmpd:False,lprd:False,rprd:False,enco:True,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,dith:2,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:34710,y:32988,varname:node_1,prsc:2|diff-5734-OUT,spec-5800-OUT,gloss-7-G,emission-5982-OUT,amdfl-4521-OUT,amspl-4522-OUT;n:type:ShaderForge.SFN_Tex2d,id:5,x:32962,y:32552,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_9754,prsc:2,tex:5ba6e8a7a673c83438cf8d7ee0826fd2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:7,x:32962,y:33107,ptovrint:False,ptlb:SpecGlossGlow,ptin:_SpecGlossGlow,varname:node_4690,prsc:2,tex:7c7c347c5f1e2f94fab981309ca61105,ntxv:1,isnm:False;n:type:ShaderForge.SFN_Multiply,id:9,x:33407,y:32673,cmnt:Diffuse,varname:node_9,prsc:2|A-5-RGB,B-70-OUT,C-5757-OUT;n:type:ShaderForge.SFN_Multiply,id:11,x:33428,y:32831,cmnt:Specular,varname:node_11,prsc:2|A-7-R,B-84-OUT;n:type:ShaderForge.SFN_Multiply,id:31,x:33286,y:33127,cmnt:Emissive,varname:node_31,prsc:2|A-5-RGB,B-7-B,C-5757-OUT;n:type:ShaderForge.SFN_Cubemap,id:43,x:33484,y:33283,ptovrint:False,ptlb:Diff_IBL,ptin:_Diff_IBL,varname:node_2146,prsc:2|DIR-44-OUT,MIP-52-OUT;n:type:ShaderForge.SFN_NormalVector,id:44,x:33123,y:33284,prsc:2,pt:False;n:type:ShaderForge.SFN_Cubemap,id:51,x:33484,y:33517,ptovrint:False,ptlb:Spec_IBL,ptin:_Spec_IBL,varname:node_4470,prsc:2|DIR-54-OUT,MIP-7-B;n:type:ShaderForge.SFN_Vector1,id:52,x:33123,y:33425,varname:node_52,prsc:2,v1:8;n:type:ShaderForge.SFN_ViewReflectionVector,id:54,x:33294,y:33517,varname:node_54,prsc:2;n:type:ShaderForge.SFN_Multiply,id:61,x:33682,y:33283,varname:node_61,prsc:2|A-43-RGB,B-43-A;n:type:ShaderForge.SFN_Multiply,id:62,x:33682,y:33517,varname:node_62,prsc:2|A-51-RGB,B-51-A;n:type:ShaderForge.SFN_Vector1,id:70,x:32962,y:32721,varname:node_70,prsc:2,v1:0.55;n:type:ShaderForge.SFN_Multiply,id:84,x:33171,y:32870,varname:node_84,prsc:2|A-5-RGB,B-86-OUT;n:type:ShaderForge.SFN_Vector1,id:86,x:32962,y:32957,varname:node_86,prsc:2,v1:2;n:type:ShaderForge.SFN_Tex2d,id:95,x:33510,y:33820,ptovrint:False,ptlb:reflectionTexture,ptin:_reflectionTexture,varname:node_2995,prsc:2,ntxv:2,isnm:False|UVIN-104-UVOUT,MIP-7-B;n:type:ShaderForge.SFN_ScreenPos,id:104,x:33276,y:33892,varname:node_104,prsc:2,sctp:2;n:type:ShaderForge.SFN_ToggleProperty,id:4517,x:33510,y:34003,ptovrint:False,ptlb:activeReflection,ptin:_activeReflection,varname:node_1083,prsc:2,on:False;n:type:ShaderForge.SFN_Multiply,id:4518,x:33727,y:33820,varname:node_4518,prsc:2|A-95-RGB,B-4517-OUT,C-4786-OUT;n:type:ShaderForge.SFN_Multiply,id:4519,x:33886,y:33701,cmnt:Reflection,varname:node_4519,prsc:2|A-7-R,B-4518-OUT;n:type:ShaderForge.SFN_Add,id:4521,x:33873,y:33171,cmnt:Diffuse_IBL,varname:node_4521,prsc:2|A-61-OUT,B-4519-OUT;n:type:ShaderForge.SFN_Add,id:4522,x:33943,y:33379,cmnt:Specular_IBL,varname:node_4522,prsc:2|A-62-OUT,B-4519-OUT;n:type:ShaderForge.SFN_Vector1,id:4786,x:33504,y:34102,varname:node_4786,prsc:2,v1:0.9;n:type:ShaderForge.SFN_FragmentPosition,id:4837,x:31963,y:33569,varname:node_4837,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4948,x:32963,y:33658,varname:node_4948,prsc:2|A-9304-OUT,B-5819-RGB,C-7-B;n:type:ShaderForge.SFN_Add,id:4949,x:33744,y:33084,varname:node_4949,prsc:2|A-31-OUT,B-4948-OUT;n:type:ShaderForge.SFN_Slider,id:5633,x:32235,y:33808,ptovrint:False,ptlb:grow,ptin:_grow,varname:node_2154,prsc:2,min:-2,cur:1,max:2;n:type:ShaderForge.SFN_Add,id:5734,x:33853,y:32864,varname:node_5734,prsc:2|A-9-OUT,B-4948-OUT;n:type:ShaderForge.SFN_Clamp01,id:5743,x:32590,y:33658,varname:node_5743,prsc:2|IN-3319-OUT;n:type:ShaderForge.SFN_Multiply,id:5757,x:32963,y:33485,varname:node_5757,prsc:2|A-5758-OUT,B-5743-OUT;n:type:ShaderForge.SFN_OneMinus,id:5758,x:32789,y:33485,varname:node_5758,prsc:2|IN-7-G;n:type:ShaderForge.SFN_Add,id:5800,x:33672,y:32952,varname:node_5800,prsc:2|A-11-OUT,B-4948-OUT;n:type:ShaderForge.SFN_Color,id:5819,x:32770,y:33809,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1452,prsc:2,glob:False,c1:0.7793102,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_ObjectPosition,id:5572,x:31963,y:33685,varname:node_5572,prsc:2;n:type:ShaderForge.SFN_Subtract,id:8866,x:32142,y:33656,varname:node_8866,prsc:2|A-4837-Y,B-5572-Y;n:type:ShaderForge.SFN_OneMinus,id:9304,x:32770,y:33658,varname:node_9304,prsc:2|IN-5743-OUT;n:type:ShaderForge.SFN_Subtract,id:3319,x:32392,y:33656,varname:node_3319,prsc:2|A-8866-OUT,B-5633-OUT;n:type:ShaderForge.SFN_Slider,id:1628,x:33663,y:32690,ptovrint:False,ptlb:energyMultiply,ptin:_energyMultiply,varname:node_9031,prsc:2,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:8393,x:33224,y:33026,ptovrint:False,ptlb:energyAdd,ptin:_energyAdd,varname:node_6617,prsc:2,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:6051,x:34004,y:33081,varname:node_6051,prsc:2|A-4949-OUT,B-9882-OUT;n:type:ShaderForge.SFN_Multiply,id:9882,x:33805,y:33002,varname:node_9882,prsc:2|A-8393-OUT,B-7-B;n:type:ShaderForge.SFN_Multiply,id:8064,x:34199,y:33071,varname:node_8064,prsc:2|A-6051-OUT,B-1628-OUT;n:type:ShaderForge.SFN_Clamp01,id:5982,x:34401,y:33071,varname:node_5982,prsc:2|IN-8064-OUT;proporder:5-7-43-51-95-4517-5633-5819-8393-1628;pass:END;sub:END;*/

Shader "Cubikolor/ShaderCubikolor_PJKpaint_mobile_Optimized" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _SpecGlossGlow ("SpecGlossGlow", 2D) = "gray" {}
        _Diff_IBL ("Diff_IBL", Cube) = "_Skybox" {}
        _Spec_IBL ("Spec_IBL", Cube) = "_Skybox" {}
        _reflectionTexture ("reflectionTexture", 2D) = "black" {}
        [MaterialToggle] _activeReflection ("activeReflection", Float ) = 0
        _grow ("grow", Range(-2, 2)) = 1
        _Color ("Color", Color) = (0.7793102,0,1,1)
        _energyAdd ("energyAdd", Range(0, 1)) = 0
        _energyMultiply ("energyMultiply", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 150
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers d3d9 xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            // Dithering function, to use with scene UVs (screen pixel coords)
            // 3x3 Bayer matrix, based on https://en.wikipedia.org/wiki/Ordered_dithering
            float BinaryDither3x3( float value, float2 sceneUVs ) {
                float3x3 mtx = float3x3(
                    float3( 3,  7,  4 )/10.0,
                    float3( 6,  1,  9 )/10.0,
                    float3( 2,  8,  5 )/10.0
                );
                float2 px = floor(_ScreenParams.xy * sceneUVs);
                int xSmp = fmod(px.x,3);
                int ySmp = fmod(px.y,3);
                float3 xVec = 1-saturate(abs(float3(0,1,2) - xSmp));
                float3 yVec = 1-saturate(abs(float3(0,1,2) - ySmp));
                float3 pxMult = float3( dot(mtx[0],yVec), dot(mtx[1],yVec), dot(mtx[2],yVec) );
                return round(value + dot(pxMult, xVec));
            }
            uniform float4 _LightColor0;
            // float4 unity_LightmapST;
            #ifdef DYNAMICLIGHTMAP_ON
                // float4 unity_DynamicLightmapST;
            #endif
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _SpecGlossGlow; uniform float4 _SpecGlossGlow_ST;
            uniform samplerCUBE _Diff_IBL;
            uniform samplerCUBE _Spec_IBL;
            uniform sampler2D _reflectionTexture; uniform float4 _reflectionTexture_ST;
            uniform fixed _activeReflection;
            uniform float _grow;
            uniform float4 _Color;
            uniform float _energyMultiply;
            uniform float _energyAdd;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                LIGHTING_COORDS(4,5)
                #ifndef LIGHTMAP_OFF
                    float4 uvLM : TEXCOORD6;
                #else
                    float3 shLight : TEXCOORD6;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(unity_ObjectToWorld, float4(v.normal,0)).xyz;
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float4 _SpecGlossGlow_var = tex2D(_SpecGlossGlow,TRANSFORM_TEX(i.uv0, _SpecGlossGlow));
                float gloss = _SpecGlossGlow_var.g;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float4 _Spec_IBL_var = texCUBElod(_Spec_IBL,float4(viewReflectDirection,_SpecGlossGlow_var.b));
                float4 _reflectionTexture_var = tex2Dlod(_reflectionTexture,float4(TRANSFORM_TEX(sceneUVs.rg, _reflectionTexture),0.0,_SpecGlossGlow_var.b));
                float3 node_4519 = (_SpecGlossGlow_var.r*(_reflectionTexture_var.rgb*_activeReflection*0.9)); // Reflection
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_5743 = saturate(((i.posWorld.g-objPos.g)-_grow));
                float3 node_4948 = ((1.0 - node_5743)*_Color.rgb*_SpecGlossGlow_var.b);
                float3 specularColor = ((_SpecGlossGlow_var.r*(_MainTex_var.rgb*2.0))+node_4948);
                float specularMonochrome = dot(specularColor,float3(0.3,0.59,0.11));
                float normTerm = (specPow + 8.0 ) / (8.0 * Pi);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*normTerm;
                float3 indirectSpecular = (0 + ((_Spec_IBL_var.rgb*_Spec_IBL_var.a)+node_4519));
                float3 specular = (directSpecular + indirectSpecular) * specularColor;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 indirectDiffuse = float3(0,0,0);
                float3 directDiffuse = max( 0.0, NdotL)*InvPi * attenColor;
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _Diff_IBL_var = texCUBElod(_Diff_IBL,float4(i.normalDir,8.0));
                indirectDiffuse += ((_Diff_IBL_var.rgb*_Diff_IBL_var.a)+node_4519); // Diffuse Ambient Light
                float node_5757 = ((1.0 - _SpecGlossGlow_var.g)*node_5743);
                float3 diffuse = (directDiffuse + indirectDiffuse) * ((_MainTex_var.rgb*0.55*node_5757)+node_4948);
                diffuse *= 1-specularMonochrome;
////// Emissive:
                float3 emissive = saturate(((((_MainTex_var.rgb*_SpecGlossGlow_var.b*node_5757)+node_4948)+(_energyAdd*_SpecGlossGlow_var.b))*_energyMultiply));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers d3d9 xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            // Dithering function, to use with scene UVs (screen pixel coords)
            // 3x3 Bayer matrix, based on https://en.wikipedia.org/wiki/Ordered_dithering
            float BinaryDither3x3( float value, float2 sceneUVs ) {
                float3x3 mtx = float3x3(
                    float3( 3,  7,  4 )/10.0,
                    float3( 6,  1,  9 )/10.0,
                    float3( 2,  8,  5 )/10.0
                );
                float2 px = floor(_ScreenParams.xy * sceneUVs);
                int xSmp = fmod(px.x,3);
                int ySmp = fmod(px.y,3);
                float3 xVec = 1-saturate(abs(float3(0,1,2) - xSmp));
                float3 yVec = 1-saturate(abs(float3(0,1,2) - ySmp));
                float3 pxMult = float3( dot(mtx[0],yVec), dot(mtx[1],yVec), dot(mtx[2],yVec) );
                return round(value + dot(pxMult, xVec));
            }
            uniform float4 _LightColor0;
            // float4 unity_LightmapST;
            #ifdef DYNAMICLIGHTMAP_ON
                // float4 unity_DynamicLightmapST;
            #endif
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _SpecGlossGlow; uniform float4 _SpecGlossGlow_ST;
            uniform float _grow;
            uniform float4 _Color;
            uniform float _energyMultiply;
            uniform float _energyAdd;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                LIGHTING_COORDS(4,5)
                #ifndef LIGHTMAP_OFF
                    float4 uvLM : TEXCOORD6;
                #else
                    float3 shLight : TEXCOORD6;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(unity_ObjectToWorld, float4(v.normal,0)).xyz;
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float4 _SpecGlossGlow_var = tex2D(_SpecGlossGlow,TRANSFORM_TEX(i.uv0, _SpecGlossGlow));
                float gloss = _SpecGlossGlow_var.g;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_5743 = saturate(((i.posWorld.g-objPos.g)-_grow));
                float3 node_4948 = ((1.0 - node_5743)*_Color.rgb*_SpecGlossGlow_var.b);
                float3 specularColor = ((_SpecGlossGlow_var.r*(_MainTex_var.rgb*2.0))+node_4948);
                float specularMonochrome = dot(specularColor,float3(0.3,0.59,0.11));
                float normTerm = (specPow + 8.0 ) / (8.0 * Pi);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*normTerm;
                float3 specular = directSpecular * specularColor;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL)*InvPi * attenColor;
                float node_5757 = ((1.0 - _SpecGlossGlow_var.g)*node_5743);
                float3 diffuse = directDiffuse * ((_MainTex_var.rgb*0.55*node_5757)+node_4948);
                diffuse *= 1-specularMonochrome;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Mobile/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
