// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: commented out 'float4 unity_DynamicLightmapST', a built-in variable
// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable

// Shader created with Shader Forge v1.03 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.03;sub:START;pass:START;ps:flbk:Mobile/Diffuse,lico:0,lgpr:1,nrmq:0,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:False,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,dith:2,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:1,fgcg:1,fgcb:1,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33753,y:32545,varname:node_1,prsc:2|diff-18-OUT,emission-18-OUT;n:type:ShaderForge.SFN_NormalVector,id:2,x:32396,y:32726,prsc:2,pt:False;n:type:ShaderForge.SFN_ChannelBlend,id:5,x:33055,y:32563,varname:node_5,prsc:2,chbt:0|M-16-OUT,R-9-RGB,G-11-RGB,B-13-RGB;n:type:ShaderForge.SFN_Color,id:9,x:32623,y:32180,ptovrint:False,ptlb:ColorOne,ptin:_ColorOne,varname:node_2699,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:11,x:32623,y:32350,ptovrint:False,ptlb:ColorTwo,ptin:_ColorTwo,varname:node_6487,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:13,x:32623,y:32522,ptovrint:False,ptlb:ColorThree,ptin:_ColorThree,varname:node_672,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Abs,id:16,x:32597,y:32726,varname:node_16,prsc:2|IN-2-OUT;n:type:ShaderForge.SFN_Color,id:17,x:33055,y:32414,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1718,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:18,x:33400,y:32545,varname:node_18,prsc:2|A-17-RGB,B-5-OUT,C-19-OUT,D-45-OUT;n:type:ShaderForge.SFN_Lerp,id:19,x:33376,y:32873,varname:node_19,prsc:2|A-21-OUT,B-40-OUT,T-20-V;n:type:ShaderForge.SFN_ScreenPos,id:20,x:33186,y:33046,varname:node_20,prsc:2,sctp:1;n:type:ShaderForge.SFN_Vector1,id:21,x:32949,y:32744,varname:node_21,prsc:2,v1:1;n:type:ShaderForge.SFN_OneMinus,id:36,x:32830,y:32896,varname:node_36,prsc:2|IN-5-OUT;n:type:ShaderForge.SFN_Vector1,id:38,x:32830,y:33034,varname:node_38,prsc:2,v1:0.3;n:type:ShaderForge.SFN_Clamp01,id:40,x:33186,y:32896,varname:node_40,prsc:2|IN-41-OUT;n:type:ShaderForge.SFN_Add,id:41,x:33003,y:32896,varname:node_41,prsc:2|A-36-OUT,B-38-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:42,x:33150,y:32251,ptovrint:False,ptlb:unlock,ptin:_unlock,varname:node_5366,prsc:2,on:False;n:type:ShaderForge.SFN_Add,id:43,x:33405,y:32197,varname:node_43,prsc:2|A-44-OUT,B-42-OUT;n:type:ShaderForge.SFN_Vector1,id:44,x:33150,y:32171,varname:node_44,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Clamp01,id:45,x:33588,y:32197,varname:node_45,prsc:2|IN-43-OUT;proporder:9-11-13-17-42;pass:END;sub:END;*/

Shader "Cubikolor/ShaderCubikolor_MiniCube_mobile_Optimized" {
    Properties {
        _ColorOne ("ColorOne", Color) = (0.5,0.5,0.5,1)
        _ColorTwo ("ColorTwo", Color) = (0.5,0.5,0.5,1)
        _ColorThree ("ColorThree", Color) = (0.5,0.5,0.5,1)
        _Color ("Color", Color) = (1,1,1,1)
        [MaterialToggle] _unlock ("unlock", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 150
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 flash d3d11_9x 
            #pragma target 3.0
            // Dithering function, to use with scene UVs (screen pixel coords)
            // 3x3 Bayer matrix, based on https://en.wikipedia.org/wiki/Ordered_dithering
            float BinaryDither3x3( float value, float2 sceneUVs ) {
                float3x3 mtx = float3x3(
                    float3( 3,  7,  4 )/10.0,
                    float3( 6,  1,  9 )/10.0,
                    float3( 2,  8,  5 )/10.0
                );
                float2 px = floor(_ScreenParams.xy * sceneUVs);
                int xSmp = fmod(px.x,3);
                int ySmp = fmod(px.y,3);
                float3 xVec = 1-saturate(abs(float3(0,1,2) - xSmp));
                float3 yVec = 1-saturate(abs(float3(0,1,2) - ySmp));
                float3 pxMult = float3( dot(mtx[0],yVec), dot(mtx[1],yVec), dot(mtx[2],yVec) );
                return round(value + dot(pxMult, xVec));
            }
            // float4 unity_LightmapST;
            #ifdef DYNAMICLIGHTMAP_ON
                // float4 unity_DynamicLightmapST;
            #endif
            uniform float4 _ColorOne;
            uniform float4 _ColorTwo;
            uniform float4 _ColorThree;
            uniform float4 _Color;
            uniform fixed _unlock;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float3 normalDir : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
                #ifndef LIGHTMAP_OFF
                    float4 uvLM : TEXCOORD2;
                #else
                    float3 shLight : TEXCOORD2;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = mul(unity_ObjectToWorld, float4(v.normal,0)).xyz;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.screenPos = float4( o.pos.xy / o.pos.w, 0, 0 );
                o.screenPos.y *= _ProjectionParams.x;
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 node_16 = abs(i.normalDir);
                float3 node_5 = (node_16.r*_ColorOne.rgb + node_16.g*_ColorTwo.rgb + node_16.b*_ColorThree.rgb);
                float node_21 = 1.0;
                float3 node_18 = (_Color.rgb*node_5*lerp(float3(node_21,node_21,node_21),saturate(((1.0 - node_5)+0.3)),float2(i.screenPos.x*(_ScreenParams.r/_ScreenParams.g), i.screenPos.y).g)*saturate((0.1+_unlock)));
                float3 emissive = node_18;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Mobile/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
