// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: commented out 'float4 unity_DynamicLightmapST', a built-in variable
// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable

// Shader created with Shader Forge v1.03 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.03;sub:START;pass:START;ps:flbk:Mobile/Diffuse,lico:0,lgpr:1,nrmq:0,limd:3,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:True,frtr:True,vitr:True,dbil:True,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,dith:2,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33776,y:32741,varname:node_1,prsc:2|diff-4857-OUT,spec-11-OUT,gloss-31-OUT,normal-8091-RGB,emission-5018-OUT,amdfl-4521-OUT,amspl-4522-OUT;n:type:ShaderForge.SFN_Tex2d,id:5,x:32485,y:32774,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2004,prsc:2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:7,x:32599,y:33035,ptovrint:False,ptlb:SpecGlossGlow,ptin:_SpecGlossGlow,varname:node_7702,prsc:2,ntxv:1,isnm:False;n:type:ShaderForge.SFN_Multiply,id:11,x:33024,y:32831,varname:node_11,prsc:2|A-7-R,B-5-B,C-5947-OUT;n:type:ShaderForge.SFN_Multiply,id:31,x:33105,y:33060,varname:node_31,prsc:2|A-5-B,B-7-G;n:type:ShaderForge.SFN_Cubemap,id:43,x:33080,y:33283,ptovrint:False,ptlb:Diff_IBL,ptin:_Diff_IBL,varname:node_6196,prsc:2|DIR-44-OUT,MIP-52-OUT;n:type:ShaderForge.SFN_NormalVector,id:44,x:32719,y:33284,prsc:2,pt:False;n:type:ShaderForge.SFN_Cubemap,id:51,x:33080,y:33517,ptovrint:False,ptlb:Spec_IBL,ptin:_Spec_IBL,varname:node_7811,prsc:2|DIR-54-OUT,MIP-7-R;n:type:ShaderForge.SFN_Vector1,id:52,x:32719,y:33425,varname:node_52,prsc:2,v1:8;n:type:ShaderForge.SFN_ViewReflectionVector,id:54,x:32833,y:33514,varname:node_54,prsc:2;n:type:ShaderForge.SFN_Multiply,id:61,x:33278,y:33283,varname:node_61,prsc:2|A-43-RGB,B-43-A,C-1123-OUT,D-1253-OUT;n:type:ShaderForge.SFN_Multiply,id:62,x:33278,y:33517,varname:node_62,prsc:2|A-51-RGB,B-51-A,C-3845-OUT,D-1253-OUT;n:type:ShaderForge.SFN_Tex2d,id:95,x:33106,y:33820,ptovrint:False,ptlb:reflectionTexture,ptin:_reflectionTexture,varname:node_4921,prsc:2,ntxv:2,isnm:False|UVIN-104-UVOUT,MIP-7-R;n:type:ShaderForge.SFN_ScreenPos,id:104,x:32872,y:33892,varname:node_104,prsc:2,sctp:2;n:type:ShaderForge.SFN_ToggleProperty,id:4517,x:33106,y:34003,ptovrint:False,ptlb:activeReflection,ptin:_activeReflection,varname:node_7309,prsc:2,on:False;n:type:ShaderForge.SFN_Multiply,id:4518,x:33323,y:33820,varname:node_4518,prsc:2|A-95-RGB,B-4517-OUT,C-4829-OUT;n:type:ShaderForge.SFN_Multiply,id:4519,x:33482,y:33701,varname:node_4519,prsc:2|A-7-G,B-4518-OUT;n:type:ShaderForge.SFN_Add,id:4521,x:33407,y:33141,varname:node_4521,prsc:2|A-61-OUT,B-4519-OUT;n:type:ShaderForge.SFN_Add,id:4522,x:33539,y:33379,varname:node_4522,prsc:2|A-62-OUT,B-4519-OUT;n:type:ShaderForge.SFN_Vector1,id:4829,x:33114,y:34084,varname:node_4829,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:4857,x:32739,y:32651,varname:node_4857,prsc:2|A-5-R,B-4868-OUT;n:type:ShaderForge.SFN_Vector1,id:4868,x:32560,y:32641,varname:node_4868,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Tex2d,id:4976,x:32340,y:31922,ptovrint:False,ptlb:Mask,ptin:_Mask,varname:node_6927,prsc:2,tex:1775decaa7739284d8f49d17245e2168,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Color,id:4979,x:32340,y:32110,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_8323,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:4981,x:32340,y:32275,ptovrint:False,ptlb:ColorTwo,ptin:_ColorTwo,varname:node_2057,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:4983,x:32340,y:32445,ptovrint:False,ptlb:ColorThree,ptin:_ColorThree,varname:node_3987,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_ChannelBlend,id:5006,x:33042,y:32286,varname:node_5006,prsc:2,chbt:0|M-5045-OUT,R-4979-RGB,G-4981-RGB,B-4983-RGB,A-5008-RGB;n:type:ShaderForge.SFN_Color,id:5008,x:32340,y:32612,ptovrint:False,ptlb:ColorFour,ptin:_ColorFour,varname:node_331,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5018,x:33478,y:32460,cmnt:Emissive,varname:node_5018,prsc:2|A-5006-OUT,B-6560-OUT,C-5-G;n:type:ShaderForge.SFN_Append,id:5045,x:32699,y:31971,varname:node_5045,prsc:2|A-4976-RGB,B-4976-A;n:type:ShaderForge.SFN_Vector1,id:5947,x:32776,y:32854,varname:node_5947,prsc:2,v1:5;n:type:ShaderForge.SFN_Vector1,id:1123,x:33274,y:33448,varname:node_1123,prsc:2,v1:0.6;n:type:ShaderForge.SFN_Vector1,id:3845,x:33274,y:33643,varname:node_3845,prsc:2,v1:0.4;n:type:ShaderForge.SFN_OneMinus,id:1253,x:32809,y:33168,varname:node_1253,prsc:2|IN-5-G;n:type:ShaderForge.SFN_ValueProperty,id:6560,x:33439,y:32689,ptovrint:False,ptlb:highlight,ptin:_highlight,varname:node_6560,prsc:2,glob:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:8091,x:33270,y:32864,ptovrint:False,ptlb:BumpMap,ptin:_BumpMap,varname:node_736,prsc:2,ntxv:3,isnm:True;proporder:5-8091-7-43-51-95-4517-4976-4979-4981-4983-5008-6560;pass:END;sub:END;*/

Shader "Cubikolor/ShaderCubikolor_Color_mobile_Optimized" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _BumpMap ("BumpMap", 2D) = "bump" {}
        _SpecGlossGlow ("SpecGlossGlow", 2D) = "gray" {}
        _Diff_IBL ("Diff_IBL", Cube) = "_Skybox" {}
        _Spec_IBL ("Spec_IBL", Cube) = "_Skybox" {}
        _reflectionTexture ("reflectionTexture", 2D) = "black" {}
        [MaterialToggle] _activeReflection ("activeReflection", Float ) = 0
        _Mask ("Mask", 2D) = "black" {}
        _Color ("Color", Color) = (1,1,1,1)
        _ColorTwo ("ColorTwo", Color) = (1,1,1,1)
        _ColorThree ("ColorThree", Color) = (1,1,1,1)
        _ColorFour ("ColorFour", Color) = (1,1,1,1)
        _highlight ("highlight", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 150
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 flash d3d11_9x d3d9
            #pragma target 3.0
            #pragma glsl
            // Dithering function, to use with scene UVs (screen pixel coords)
            // 3x3 Bayer matrix, based on https://en.wikipedia.org/wiki/Ordered_dithering
            float BinaryDither3x3( float value, float2 sceneUVs ) {
                float3x3 mtx = float3x3(
                    float3( 3,  7,  4 )/10.0,
                    float3( 6,  1,  9 )/10.0,
                    float3( 2,  8,  5 )/10.0
                );
                float2 px = floor(_ScreenParams.xy * sceneUVs);
                int xSmp = fmod(px.x,3);
                int ySmp = fmod(px.y,3);
                float3 xVec = 1-saturate(abs(float3(0,1,2) - xSmp));
                float3 yVec = 1-saturate(abs(float3(0,1,2) - ySmp));
                float3 pxMult = float3( dot(mtx[0],yVec), dot(mtx[1],yVec), dot(mtx[2],yVec) );
                return round(value + dot(pxMult, xVec));
            }
            uniform float4 _LightColor0;
            // float4 unity_LightmapST;
            #ifdef DYNAMICLIGHTMAP_ON
                // float4 unity_DynamicLightmapST;
            #endif
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _SpecGlossGlow; uniform float4 _SpecGlossGlow_ST;
            uniform samplerCUBE _Diff_IBL;
            uniform samplerCUBE _Spec_IBL;
            uniform sampler2D _reflectionTexture; uniform float4 _reflectionTexture_ST;
            uniform fixed _activeReflection;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform float4 _Color;
            uniform float4 _ColorTwo;
            uniform float4 _ColorThree;
            uniform float4 _ColorFour;
            uniform float _highlight;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                #ifndef LIGHTMAP_OFF
                    float4 uvLM : TEXCOORD8;
                #else
                    float3 shLight : TEXCOORD8;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(unity_ObjectToWorld, float4(v.normal,0)).xyz;
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = _BumpMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i)*2;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 _SpecGlossGlow_var = tex2D(_SpecGlossGlow,TRANSFORM_TEX(i.uv0, _SpecGlossGlow));
                float gloss = (_MainTex_var.b*_SpecGlossGlow_var.g);
                float specPow = exp2( gloss * 10.0+1.0);
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #ifndef LIGHTMAP_OFF
                    d.ambientOrLightmapUV = i.uvLM;
                #else
                    d.ambientOrLightmapUV.xyz = i.shLight;
                #endif
                UnityGI gi = UnityStandardGlobalIllumination (d, 1, gloss, normalDirection);
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float4 _Spec_IBL_var = texCUBElod(_Spec_IBL,float4(viewReflectDirection,_SpecGlossGlow_var.r));
                float node_1253 = (1.0 - _MainTex_var.g);
                float4 _reflectionTexture_var = tex2Dlod(_reflectionTexture,float4(TRANSFORM_TEX(sceneUVs.rg, _reflectionTexture),0.0,_SpecGlossGlow_var.r));
                float3 node_4519 = (_SpecGlossGlow_var.g*(_reflectionTexture_var.rgb*_activeReflection*1.0));
                float node_11 = (_SpecGlossGlow_var.r*_MainTex_var.b*5.0);
                float3 specularColor = float3(node_11,node_11,node_11);
                float specularMonochrome = dot(specularColor,float3(0.3,0.59,0.11));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float fresnelTerm = FresnelTerm(specularMonochrome, VdotH);
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float visTerm = SmithGGXVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, BlinnPhongNormalizedTerm (NdotH, RoughnessToSpecPower (1.0-gloss)));
                float specularPBL = max(0, (fresnelTerm*visTerm*normTerm) / (4 * NdotV + 1e-5f) );
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL;
                float3 indirectSpecular = (0 + ((_Spec_IBL_var.rgb*_Spec_IBL_var.a*0.4*node_1253)+node_4519));
                float3 specular = (directSpecular + indirectSpecular) * specularColor;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 indirectDiffuse = float3(0,0,0);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb*2; // Ambient Light
                float4 _Diff_IBL_var = texCUBElod(_Diff_IBL,float4(i.normalDir,8.0));
                indirectDiffuse += ((_Diff_IBL_var.rgb*_Diff_IBL_var.a*0.6*node_1253)+node_4519); // Diffuse Ambient Light
                float node_4857 = (_MainTex_var.r*0.2);
                float3 diffuse = (directDiffuse + indirectDiffuse) * float3(node_4857,node_4857,node_4857);
                diffuse *= 1-specularMonochrome;
////// Emissive:
                float4 _Mask_var = tex2D(_Mask,TRANSFORM_TEX(i.uv0, _Mask));
                float4 node_5045 = float4(_Mask_var.rgb,_Mask_var.a);
                float3 emissive = ((node_5045.r*_Color.rgb + node_5045.g*_ColorTwo.rgb + node_5045.b*_ColorThree.rgb + node_5045.a*_ColorFour.rgb)*_highlight*_MainTex_var.g);
/// Final Color:
                float3 indirectFresnelPBL = indirectSpecular*(1-specularMonochrome)*gloss*FresnelTerm(0,NdotV);
                float3 finalColor = diffuse + specular + emissive + indirectFresnelPBL;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Mobile/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
