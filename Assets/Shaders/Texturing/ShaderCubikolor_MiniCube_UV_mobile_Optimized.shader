// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Shader created with Shader Forge v1.03 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.03;sub:START;pass:START;ps:flbk:Mobile/Diffuse,lico:0,lgpr:1,nrmq:0,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:False,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,dith:2,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:1,fgcg:1,fgcb:1,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:True;n:type:ShaderForge.SFN_Final,id:1,x:34075,y:32421,varname:node_1,prsc:2|emission-5937-OUT;n:type:ShaderForge.SFN_NormalVector,id:9200,x:32618,y:32513,prsc:2,pt:False;n:type:ShaderForge.SFN_Transform,id:8836,x:32799,y:32513,varname:node_8836,prsc:2,tffrom:0,tfto:1|IN-9200-OUT;n:type:ShaderForge.SFN_If,id:9651,x:33247,y:32166,varname:node_9651,prsc:2|A-8836-XYZ,B-6090-OUT,GT-187-OUT,EQ-6090-OUT,LT-187-OUT;n:type:ShaderForge.SFN_Vector3,id:6090,x:33011,y:32184,varname:node_6090,prsc:2,v1:1,v2:0,v3:0;n:type:ShaderForge.SFN_Vector1,id:187,x:33015,y:32089,varname:node_187,prsc:2,v1:0;n:type:ShaderForge.SFN_If,id:1530,x:33247,y:32305,varname:node_1530,prsc:2|A-8836-XYZ,B-1599-OUT,GT-187-OUT,EQ-1599-OUT,LT-187-OUT;n:type:ShaderForge.SFN_Add,id:5937,x:33582,y:32343,varname:node_5937,prsc:2|A-9651-OUT,B-1530-OUT,C-7670-OUT;n:type:ShaderForge.SFN_Vector3,id:1599,x:33007,y:32362,varname:node_1599,prsc:2,v1:0,v2:1,v3:0;n:type:ShaderForge.SFN_If,id:7670,x:33247,y:32472,varname:node_7670,prsc:2|A-8836-XYZ,B-4307-OUT,GT-187-OUT,EQ-4307-OUT,LT-187-OUT;n:type:ShaderForge.SFN_Vector3,id:4307,x:33007,y:32513,varname:node_4307,prsc:2,v1:0,v2:0,v3:1;pass:END;sub:END;*/

Shader "Cubikolor/ShaderCubikolor_MiniCube_UV_mobile_Optimized" {
    Properties {
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 150
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 2.0
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float3 normalDir : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = mul(unity_ObjectToWorld, float4(v.normal,0)).xyz;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 node_8836 = mul( unity_WorldToObject, float4(i.normalDir,0) ).xyz;
                float3 node_6090 = float3(1,0,0);
                float node_9651_if_leA = step(node_8836.rgb,node_6090);
                float node_9651_if_leB = step(node_6090,node_8836.rgb);
                float node_187 = 0.0;
                float3 node_1599 = float3(0,1,0);
                float node_1530_if_leA = step(node_8836.rgb,node_1599);
                float node_1530_if_leB = step(node_1599,node_8836.rgb);
                float3 node_4307 = float3(0,0,1);
                float node_7670_if_leA = step(node_8836.rgb,node_4307);
                float node_7670_if_leB = step(node_4307,node_8836.rgb);
                float3 emissive = (lerp((node_9651_if_leA*node_187)+(node_9651_if_leB*node_187),node_6090,node_9651_if_leA*node_9651_if_leB)+lerp((node_1530_if_leA*node_187)+(node_1530_if_leB*node_187),node_1599,node_1530_if_leA*node_1530_if_leB)+lerp((node_7670_if_leA*node_187)+(node_7670_if_leB*node_187),node_4307,node_7670_if_leA*node_7670_if_leB));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Mobile/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
