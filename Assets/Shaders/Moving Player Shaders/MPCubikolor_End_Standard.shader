﻿    Shader "MP/Cubikolor_End_Standard"
    {
        Properties
        {
            _Albedo ("Map, ColorIntensity(R), Specular(G), Gloss(B), Emission(A)", 2D) = "white" {}
            _AlbedoScale ("Albedo scale", Range(0.1, 2)) = 1.0
            _MainColor ("Main Color", Color) = (1.0, 1.0, 1.0, 1.0)
            _Smoothness ("Smoothness", Range(0, 1)) = 0.5
            _SpecScale ("Spec scale", Range(0, 5)) = 0.5
            _EmissionScale ("EmissionScale", Range(0, 2)) = 0.5                  
        }
     
        SubShader
        {
            Tags
            {
                "Queue" = "Geometry"
                "RenderType" = "Opaque"
            }
         
            CGINCLUDE
            #define _GLOSSYENV 1
            ENDCG
         
            CGPROGRAM
            #pragma target 3.0
            #include "UnityPBSLighting.cginc"
            #pragma surface surf Standard
            #pragma exclude_renderers gles
     
            struct Input
            {
                float2 uv_Albedo;
            };
     
            float _AlbedoScale;
            float _SpecScale;
            float _EmissionScale;
            float _Smoothness;
            fixed4 _MainColor;
            sampler2D _Albedo;
     
            void surf (Input IN, inout SurfaceOutputStandard o)
            {
                fixed4 albedo = tex2D(_Albedo, IN.uv_Albedo);
                float spec = pow(albedo.b, 4.0);                       
                o.Albedo = spec * _AlbedoScale * _MainColor;
                o.Smoothness = spec * _Smoothness;//albedo.b * _Smoothness;
                o.Emission = albedo.a * _EmissionScale * _MainColor;                
                o.Metallic = 1.0;//spec * _SpecScale;
            }
            ENDCG
        }
     
        FallBack "Diffuse"
    }
