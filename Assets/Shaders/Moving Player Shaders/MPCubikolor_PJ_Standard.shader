﻿    Shader "MP/Cubikolo_PJ_Standard"
    {
        Properties
        {
            _Albedo ("Albedo (RGB), Alpha (A)", 2D) = "white" {}
            _AlbedoPower ("Albedo power", Range(0.5, 2)) = 1.0
            _Alpha ("alpha", Range(0, 1)) = 1.0
            [NoScaleOffset]
            _Metallic ("Specular (R), Gloss (G), Emission (B)", 2D) = "black" {}
            _Smoothness ("Smoothness", Range(0, 2)) = 0.5
            _SpecScale ("Spec scale", Range(0, 2)) = 0.5
            _EmissionScale ("EmissionScale", Range(0, 5)) = 0.5
            [NoScaleOffset]
            _Normal ("Normal (RGB)", 2D) = "bump" {}
            _NormalScale ("Normal Scale", Range(-3, 3)) = 1.0
            [NoScaleOffset]
            _Occlusion ("Occulusion", 2D) = "white" {}
            _OcclusionScale ("Occulusion Scale", Range(0, 3)) = 1.0
                  
        }
     
        SubShader
        {
            Tags
            {
                "Queue" = "Geometry"
                "RenderType" = "Opaque"
            }
         
            CGINCLUDE
            #define _GLOSSYENV 1
            ENDCG
         
            CGPROGRAM
            #pragma target 3.0
            #include "UnityPBSLighting.cginc"
            #pragma surface surf Standard
            #pragma exclude_renderers gles
     
            struct Input
            {
                float2 uv_Albedo;
            };
     
            float _AlbedoPower;
            float _Alpha;
            float _OcclusionScale;
            float _SpecScale;
            float _EmissionScale;
            float _Smoothness;
            float _NormalScale;
            sampler2D _Albedo;
            sampler2D _Normal;
            sampler2D _Metallic;
            sampler2D _Occlusion;
     
            void surf (Input IN, inout SurfaceOutputStandard o)
            {
                fixed4 albedo = pow(tex2D(_Albedo, IN.uv_Albedo), _AlbedoPower);
                fixed4 metallic = tex2D(_Metallic, IN.uv_Albedo);
                fixed3 normal = UnpackScaleNormal(tex2D(_Normal, IN.uv_Albedo), _NormalScale);
                fixed4 occlusion = tex2D(_Occlusion, IN.uv_Albedo);
                
                float spec = clamp(metallic.r + metallic.g, 0 ,1);
                
                o.Albedo = albedo;
                o.Alpha = albedo.a * _Alpha;
                o.Normal = normal;
                o.Smoothness = _Smoothness * metallic.r;
                o.Occlusion = occlusion * _OcclusionScale;
                o.Emission = clamp(metallic.b, 0, 1) * _EmissionScale *albedo;
                o.Metallic = 1.0;//spec * _SpecScale;
                //fixed4 spec = metallic.r * _SpecScale * albedo;
                //spec.a = metallic.g * _GlossScale;
                //o.Specular = spec;
            }
            ENDCG
        }
     
        FallBack "Diffuse"
    }
