﻿    Shader "MP/Cubikolor_Neutre_Standard"
    {
        Properties
        {
            _Albedo ("Albedo (RGB), Alpha (A)", 2D) = "white" {}
            _AlbedoScale ("Albedo scale", Range(0.5, 2)) = 1.0
            [NoScaleOffset]
            _Metallic ("Specular (R), Gloss (G), Emission (B)", 2D) = "black" {}
            _Smoothness ("Smoothness", Range(0, 1)) = 0.5
            _SpecScale ("Spec scale", Range(0, 5)) = 1
            //_GlossScale ("Gloss sclale", Range(0, 1)) = 0.5
            _EmissionScale ("EmissionScale", Range(0, 10)) = 1
            [NoScaleOffset]
            _Normal ("Normal (RGB)", 2D) = "bump" {}
            _NormalScale ("Normal Scale", Range(-3, 3)) = 1.0    
        }
     
        SubShader
        {
            Tags
            {
                "Queue" = "Geometry"
                "RenderType" = "Opaque"
            }
         
            CGINCLUDE
            #define _GLOSSYENV 1
            ENDCG
         
            CGPROGRAM
            #pragma target 3.0
            #include "UnityPBSLighting.cginc"
            #pragma surface surf Standard
            #pragma exclude_renderers gles
     
            struct Input
            {
                float2 uv_Albedo;
            };
     
            float _AlbedoScale;
            float _OcclusionScale;
            float _SpecScale;
            //float _GlossScale;
            float _EmissionScale;
            float _Smoothness;
            float _NormalScale;
            sampler2D _Albedo;
            sampler2D _Normal;
            sampler2D _Metallic;
            sampler2D _Occlusion;
     
            void surf (Input IN, inout SurfaceOutputStandard o)
            {
                fixed4 albedo = tex2D(_Albedo, IN.uv_Albedo);
                fixed4 metallic = tex2D(_Metallic, IN.uv_Albedo);
                fixed3 normal = UnpackScaleNormal(tex2D(_Normal, IN.uv_Albedo), _NormalScale);
                fixed4 occlusion = tex2D(_Occlusion, IN.uv_Albedo);
              //  float3 refl = WorldReflectionVector (IN, o.Normal) * metallic.r;
                float spec = clamp(pow(metallic.g, 2.0) * 4.0, 0, 1) + 0.2 * metallic.g;
                o.Albedo = albedo.b * fixed4(1.0, 1.0, 1.0, 1.0) * _AlbedoScale;
                o.Alpha = albedo.a;
                o.Normal = normal;
                o.Smoothness = spec * _Smoothness;
                o.Emission = metallic.b * _EmissionScale;
                o.Metallic = spec * _SpecScale;
                //o.Specular = metallic.r * _SpecScale * albedo.b ;//* 1.0/metallic.g ;
            }
            ENDCG
        }
     
        FallBack "Diffuse"
    }
