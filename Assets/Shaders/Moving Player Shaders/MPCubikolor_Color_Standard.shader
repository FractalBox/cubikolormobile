﻿    Shader "MP/Cubikolor_Color_Standard"
    {
        Properties
        {
            //main
            _Albedo ("Tex, Color intensity(R), Glow(G), Specular(B)", 2D) = "white" {}
            _AlbedoScale ("Albedo scale", Range(0.5, 2)) = 1.0
            _MainColor ("Main Color", Color) = (0.2, 0.2, 0.2, 0.2)
            _GlossScale ("Gloss sclale", Range(0, 1)) = 0.5
            //SpecGlossGlow
            [NoScaleOffset]
            _SpecGloss ("Specular (R), Gloss (G)", 2D) = "black" {}
            _Smoothness ("Smoothness", Range(0, 1)) = 0.5
            _SpecScale ("Spec scale", Range(0, 1)) = 0.5            
            _EmissionScale ("Glow Scale", Range(0, 10)) = 0.5
            //normal
            [NoScaleOffset]
            _Normal ("Normal (RGB)", 2D) = "bump" {}
            _NormalScale ("Normal Scale", Range(-3, 3)) = 1.0
            //mask
            [NoScaleOffset]
            _GlowMask ("Mask", 2D) = "black" {} 
            _Color ("Color 1", Color) = (1.0, 1.0, 1.0, 1.0)       
            _ColorTwo ("Color 3", Color) = (1.0, 1.0, 1.0, 1.0)  
            _ColorThree ("Color 2", Color) = (1.0, 1.0, 1.0, 1.0)  
            _ColorFour ("Color 4", Color) = (1.0, 1.0, 1.0, 1.0)  
        }
     
        SubShader
        {
            Tags
            {
                "Queue" = "Geometry"
                "RenderType" = "Opaque"
            }
         
            CGINCLUDE
            #define _GLOSSYENV 1
            ENDCG
         
            CGPROGRAM
            #pragma target 3.0
            #include "UnityPBSLighting.cginc"
            #pragma surface surf StandardSpecular
            #pragma exclude_renderers gles
     
            struct Input
            {
                float2 uv_Albedo;
            };
     
            float _AlbedoScale;
            float _SpecScale;
            float _GlossScale;
            float _EmissionScale;
            float _Smoothness;
            float _NormalScale;

            fixed4 _MainColor;
            fixed4 _Color;
            fixed4 _ColorTwo;
            fixed4 _ColorThree;
            fixed4 _ColorFour;
            
            sampler2D _Albedo;
            sampler2D _Normal;
            sampler2D _SpecGloss;
            sampler2D _GlowMask;
     
            fixed4 blendWithMask(fixed4 mask, fixed4 color1, fixed4 color2, fixed4 color3, fixed4 color4){
              return mask.r * color1 + mask.g * color2 + mask.b * color3 + mask.a * color4;
            }
     
            void surf (Input IN, inout SurfaceOutputStandardSpecular o)
            {
                fixed4 albedo = tex2D(_Albedo, IN.uv_Albedo);
                fixed4 specGloss = tex2D(_SpecGloss, IN.uv_Albedo);
                fixed3 normal = UnpackScaleNormal(tex2D(_Normal, IN.uv_Albedo), _NormalScale);
                fixed4 maskedGlow = tex2D(_GlowMask, IN.uv_Albedo);
                       
                o.Albedo = albedo.r * _AlbedoScale * _MainColor * pow(blendWithMask(maskedGlow, _Color, _ColorTwo, _ColorThree, _ColorFour),_AlbedoScale);
                o.Normal = normal;
                o.Smoothness = _Smoothness;
                o.Emission = albedo.g * _EmissionScale * pow(blendWithMask(maskedGlow, _Color, _ColorTwo, _ColorThree, _ColorFour),_EmissionScale);
                fixed4 spec = albedo.b * _SpecScale * specGloss.r * fixed4(1.0, 1.0, 1.0, 1.0);
                //Specular alpha channel is Glow in Standard output
                spec.a = albedo.b * specGloss.g * _GlossScale;
                o.Specular = spec;
            }
            ENDCG
        }
     
        FallBack "Diffuse"
    }
