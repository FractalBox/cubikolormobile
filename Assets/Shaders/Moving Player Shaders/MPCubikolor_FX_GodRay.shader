﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "MP/Cubikolor_FX_GodRay" {
	Properties {
    _MainColor("Main Color", Color) = (1.0, 1.0, 1.0, 1.0)
    _RampOffset("Ramp offset", Range(0 ,2)) = 2.0
	}
	SubShader {
		Tags {
      "Queue" = "Transparent"
      "IgnoreProjector" = "True"
      "RenderType" = "Transparent"
    }
		LOD 200
		
    Pass {
    
      Cull Off
      Lighting Off
      ZWrite Off
      Fog { Mode Off }
      Offset -1, -1
      ColorMask RGB
      AlphaTest Greater .01
      Blend SrcAlpha OneMinusSrcAlpha
      ColorMaterial AmbientAndDiffuse
      
      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag

      #include "UnityCG.cginc"
  
      float     _RampOffset;
      fixed4    _MainColor;

      struct appdata_t
      {
        float4 vertex : POSITION;
        half4 color : COLOR;
        float2 texcoord : TEXCOORD0;
      };

      struct v2f
      {
        #ifdef SHADER_API_PSSL
        float4 vertex : SV_POSITION;
        #else
        float4 vertex : POSITION;
        #endif
        half4 color : COLOR;
        float2 texcoord : TEXCOORD0;
      };

      v2f vert (appdata_t v)
      { 
        v2f o;
        o.vertex = UnityObjectToClipPos(v.vertex);
        o.color = v.color;
        o.texcoord = v.texcoord;
        return o;
      }

      #ifdef SHADER_API_PSSL
      half4 frag( v2f IN ) : SV_Target
      #else
      half4 frag( v2f IN ) : COLOR
      #endif  
      {
        // Sample the texture
        float gradient = 1.0 - clamp(IN.texcoord.y + _RampOffset * 0.5, 0.0, 1.0);
        half4 col = gradient * IN.color * _MainColor;
        col.a = col.a * gradient;
        return col;
      }
      ENDCG
      
      }
	} 
	FallBack "Diffuse"
}


  